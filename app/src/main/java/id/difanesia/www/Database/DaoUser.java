package id.difanesia.www.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import id.difanesia.www.Model.User;
@Dao
public interface DaoUser {
    @Query("SELECT * from tb_user")
    LiveData<User> getAllSavedData();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(User users);

    @Update
    void update(User users);

    @Delete
    void delete(User users);

    @Query("DELETE from tb_user")
    void delete();

    @Query("UPDATE tb_user SET name=:name, jenisKelamin=:jenis_kelamin,tanggalLahir=:tanggal_lahir,noTlpn=:no_hp,image=:image WHERE idUser=:id_user")
    void update(String name,String jenis_kelamin, String tanggal_lahir, String no_hp,String image, Integer id_user);

    @Query("UPDATE tb_user SET name=:name, jenisKelamin=:jenis_kelamin,tanggalLahir=:tanggal_lahir,noTlpn=:no_hp WHERE idUser=:id_user")
    void update(String name,String jenis_kelamin, String tanggal_lahir, String no_hp, Integer id_user);
}
