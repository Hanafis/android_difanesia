package id.difanesia.www.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.difanesia.www.Model.Search;

@Dao
public interface DaoSearch {
    @Query("SELECT * from tb_search")
    LiveData<List<Search>> getAllSavedData();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Search search);

    @Update
    void update(Search search);

    @Delete
    void delete(Search search);

    @Query("DELETE from tb_search")
    void delete();

    @Query("UPDATE tb_search SET keyword=:keyword WHERE _id=:id")
    void update(String keyword, Integer id);

}
