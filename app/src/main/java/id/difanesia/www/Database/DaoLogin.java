package id.difanesia.www.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import id.difanesia.www.Model.Login;

@Dao
public interface DaoLogin {

    @Query("SELECT * from tb_login")
    LiveData<Login> getAllSavedData();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Login login);

    @Update
    void update(Login login);

    @Delete
    void delete(Login login);

    @Query("DELETE from tb_login")
    void delete();


}
