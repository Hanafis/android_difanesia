package id.difanesia.www.Database;





import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import id.difanesia.www.Model.Login;
import id.difanesia.www.Model.Search;
import id.difanesia.www.Model.User;


/**
 * Created by amal on 02/01/19.
 */
@Database(entities = {Login.class, User.class, Search.class}, version = 4, exportSchema = false)
public abstract class DatabaseRoom extends RoomDatabase {
    private static DatabaseRoom INSTANCE;
    @TypeConverters({TypeConverters.class})
    public static DatabaseRoom getInstance() {
        return INSTANCE;
    }
    public static DatabaseRoom getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (DatabaseRoom.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DatabaseRoom.class, "difanesia_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract DaoLogin dataDaoUser();
    public abstract DaoUser dataDaoDetailUser();
    public abstract DaoSearch dataDaoSearch();

    @Override
    public void clearAllTables() {

    }
}

