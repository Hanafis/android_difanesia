package id.difanesia.www.Repository;



import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import id.difanesia.www.Database.DaoLogin;
import id.difanesia.www.Database.DatabaseRoom;
import id.difanesia.www.Model.Login;


public class LoginRepo {
    private static LoginRepo Instance = null;
    private final LiveData<Login> savedData;
    private final MutableLiveData<Boolean> loadingState;
    private final DaoLogin daoLogin;
    Context mContext;

    public LoginRepo(Application application) {
        DatabaseRoom db = DatabaseRoom.getDatabase(application);
        daoLogin = db.dataDaoUser();
        savedData = daoLogin.getAllSavedData();
        mContext = application.getApplicationContext();
        loadingState = new MutableLiveData<>();
    }


    public static LoginRepo getInstance(Application application) {
        if (Instance == null)
            Instance = new LoginRepo(application);
        return Instance;
    }

    public LiveData<Login> getSavedData() {
        return savedData;
    }


    public void insertData(Login locationDataList) {

        new insertAsyncTask(daoLogin).execute(locationDataList);
    }
    public void  deleteData(Login locationData){
        new deleteAsyncTask(daoLogin).execute(locationData);
    }



    public MutableLiveData<Boolean> getLoadingState() {
        return loadingState;
    }


    private static class insertAsyncTask extends AsyncTask<Login, Void, Void> {

        private final DaoLogin mAsyncTaskDao;

        insertAsyncTask(DaoLogin dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Login[] lists) {
            mAsyncTaskDao.insert(lists[0]);
            return null;
        }
    }



    private  static class deleteAsyncTask extends AsyncTask<Login, Void,Void>{
        private final DaoLogin mAsyncTaskDao;
        deleteAsyncTask(DaoLogin dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Login... login) {
            mAsyncTaskDao.delete(login[0]);
            return null;
        }
    }
    public void  deleteAll(){
        new deleteAllAsyncTask(daoLogin).execute();
    }
    private  static class deleteAllAsyncTask extends AsyncTask<Login, Void,Void>{
        private final DaoLogin mAsyncTaskDao;
        deleteAllAsyncTask(DaoLogin dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Login... login) {
            mAsyncTaskDao.delete();
            return null;
        }
    }

}

