package id.difanesia.www.Repository;


import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import id.difanesia.www.Database.DaoUser;
import id.difanesia.www.Database.DatabaseRoom;
import id.difanesia.www.Model.User;


public class UserRepo {
    private static UserRepo Instance = null;
    private final LiveData<User> savedData;
    private final MutableLiveData<Boolean> loadingState;
    private final DaoUser daoUser;
    Context mContext;

    public UserRepo(Application application) {
        DatabaseRoom db = DatabaseRoom.getDatabase(application);
        daoUser = db.dataDaoDetailUser();
        savedData = daoUser.getAllSavedData();
        mContext = application.getApplicationContext();
        loadingState = new MutableLiveData<>();
    }


    public static UserRepo getInstance(Application application) {
        if (Instance == null)
            Instance = new UserRepo(application);
        return Instance;
    }

    public LiveData<User> getSavedData() {
        return savedData;
    }


    public void insertData(User locationDataList) {

        new insertAsyncTask(daoUser).execute(locationDataList);
    }

    public void updateData(String name,String jenis_kelamin, String tanggal_lahir, String no_hp,String image, Integer id_user) {

        new updateAsyncTask(daoUser).execute(name,jenis_kelamin,tanggal_lahir,no_hp,image,id_user);
    }
    public void updateNoImageData(String name,String jenis_kelamin, String tanggal_lahir, String no_hp, Integer id_user) {

        new updateNoImageAsyncTask(daoUser).execute(name,jenis_kelamin,tanggal_lahir,no_hp,id_user);
    }

    public void  deleteData(User locationData){
        new deleteAsyncTask(daoUser).execute(locationData);
    }

    public MutableLiveData<Boolean> getLoadingState() {
        return loadingState;
    }


    private static class insertAsyncTask extends AsyncTask<User, Void, Void> {

        private final DaoUser mAsyncTaskDao;

        insertAsyncTask(DaoUser dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(User[] lists) {
            mAsyncTaskDao.insert(lists[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Object, Void, Void> {

        private final DaoUser mAsyncTaskDao;

        updateAsyncTask(DaoUser dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Object... login) {
            mAsyncTaskDao.update((String) login[0],(String) login[1],(String) login[2],(String) login[3],(String) login[4],(Integer) login[5] );
            return null;
        }
    }
    private static class updateNoImageAsyncTask extends AsyncTask<Object, Void, Void> {

        private final DaoUser mAsyncTaskDao;

        updateNoImageAsyncTask(DaoUser dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Object... login) {
            mAsyncTaskDao.update((String) login[0],(String) login[1],(String) login[2],(String) login[3],(Integer) login[4] );
            return null;
        }
    }
    private  static class deleteAsyncTask extends AsyncTask<User, Void,Void>{
        private final DaoUser mAsyncTaskDao;
        deleteAsyncTask(DaoUser dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(User... login) {
            mAsyncTaskDao.delete(login[0]);
            return null;
        }
    }
    public void  deleteAll(){
        new UserRepo.deleteAllAsyncTask(daoUser).execute();
    }
    private  static class deleteAllAsyncTask extends AsyncTask<User, Void,Void>{
        private final DaoUser mAsyncTaskDao;
        deleteAllAsyncTask(DaoUser dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(User... login) {
            mAsyncTaskDao.delete();
            return null;
        }
    }

}

