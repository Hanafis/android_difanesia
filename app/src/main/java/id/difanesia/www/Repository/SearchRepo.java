package id.difanesia.www.Repository;


import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import id.difanesia.www.Database.DaoSearch;
import id.difanesia.www.Database.DatabaseRoom;
import id.difanesia.www.Model.Search;

public class SearchRepo {
    private static SearchRepo Instance = null;
    private final LiveData<List<Search>> savedData;
    private final MutableLiveData<Boolean> loadingState;
    private final DaoSearch daoSearch;
    Context mContext;

    public SearchRepo(Application application) {
        DatabaseRoom db = DatabaseRoom.getDatabase(application);
        daoSearch = db.dataDaoSearch();
        savedData = daoSearch.getAllSavedData();
        mContext = application.getApplicationContext();
        loadingState = new MutableLiveData<>();
    }


    public static SearchRepo getInstance(Application application) {
        if (Instance == null)
            Instance = new SearchRepo(application);
        return Instance;
    }

    public LiveData<List<Search>> getSavedData() {
        return savedData;
    }


    public void insertData(Search locationDataList) {

        new insertAsyncTask(daoSearch).execute(locationDataList);
    }

    public void updateData(String name,Integer id) {

        new updateAsyncTask(daoSearch).execute(name,id);
    }

    public void  deleteData(Search locationData){
        new deleteAsyncTask(daoSearch).execute(locationData);
    }



    public MutableLiveData<Boolean> getLoadingState() {
        return loadingState;
    }


    private static class insertAsyncTask extends AsyncTask<Search, Void, Void> {

        private final DaoSearch mAsyncTaskDao;

        insertAsyncTask(DaoSearch dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Search[] lists) {
            mAsyncTaskDao.insert(lists[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Object, Void, Void> {

        private final DaoSearch mAsyncTaskDao;

        updateAsyncTask(DaoSearch dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Object... login) {

            mAsyncTaskDao.update((String) login[0],(Integer)login[1]);
            return null;
        }

    }
    private  static class deleteAsyncTask extends AsyncTask<Search, Void,Void>{
        private final DaoSearch mAsyncTaskDao;
        deleteAsyncTask(DaoSearch dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Search... login) {
            mAsyncTaskDao.delete(login[0]);
            return null;
        }
    }
    public void  deleteAll(){
        new deleteAllAsyncTask(daoSearch).execute();
    }
    private  static class deleteAllAsyncTask extends AsyncTask<Search, Void,Void>{
        private final DaoSearch mAsyncTaskDao;
        deleteAllAsyncTask(DaoSearch dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Search... login) {
            mAsyncTaskDao.delete();
            return null;
        }
    }

}
