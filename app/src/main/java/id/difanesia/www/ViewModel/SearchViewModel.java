package id.difanesia.www.ViewModel;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import id.difanesia.www.Model.Search;
import id.difanesia.www.Repository.SearchRepo;

public class SearchViewModel extends AndroidViewModel {

    LiveData<List<Search>> apiResponseLiveData;
    SearchRepo searchRepo;
    MutableLiveData<Boolean> loadingState;

    public SearchViewModel(@NonNull Application application) {
        super(application);
        searchRepo = new SearchRepo(application);
        apiResponseLiveData = searchRepo.getSavedData();
//        loadingState = detailSearchRepo.getLoadingState();
    }

    public LiveData<List<Search>> getApiResponseLiveData() {
        return apiResponseLiveData;
    }

    //    public void fetchFromWeb() {
//        detailSearchRepo.getAllData();
//    }
    public  void insertData(Search user){
        searchRepo.insertData(user);
    }
    public  void deleteData(Search user){
        searchRepo.deleteData(user);
    }

    public void updateData(String keyword,Integer id) {
        searchRepo.updateData(keyword,id);
    }
    public void DeleteALL() {
        searchRepo.deleteAll();
    }

    public MutableLiveData<Boolean> getLoadingState() {
        return loadingState;
    }
}



