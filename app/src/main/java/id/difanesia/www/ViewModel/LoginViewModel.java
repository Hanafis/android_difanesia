package id.difanesia.www.ViewModel;



import android.app.Application;


import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import id.difanesia.www.Model.Login;
import id.difanesia.www.Repository.LoginRepo;


public class LoginViewModel extends AndroidViewModel {

    LiveData<Login> apiResponseLiveData;
    LoginRepo loginRepo;
    MutableLiveData<Boolean> loadingState;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        loginRepo = new LoginRepo(application);
        apiResponseLiveData = loginRepo.getSavedData();

//        loadingState = userRepo.getLoadingState();
    }

    public LiveData<Login> getApiResponseLiveData() {
        return apiResponseLiveData;
    }

    //    public void fetchFromWeb() {
//        userRepo.getAllData();
//    }
    public  void insertData(Login login){
        loginRepo.insertData(login);
    }
    public  void deleteData(Login login){
        loginRepo.deleteData(login);
    }
    public void DeleteALL() {
        loginRepo.deleteAll();
    }

    public MutableLiveData<Boolean> getLoadingState() {
        return loadingState;
    }
}

