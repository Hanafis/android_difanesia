package id.difanesia.www.ViewModel;




import android.app.Application;


import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import id.difanesia.www.Model.User;
import id.difanesia.www.Repository.UserRepo;


public class UserViewModel extends AndroidViewModel {

    LiveData<User> apiResponseLiveData;
    UserRepo userRepo;
    MutableLiveData<Boolean> loadingState;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepo = new UserRepo(application);
        apiResponseLiveData = userRepo.getSavedData();
//        loadingState = detailUserRepo.getLoadingState();
    }

    public LiveData<User> getApiResponseLiveData() {
        return apiResponseLiveData;
    }

    //    public void fetchFromWeb() {
//        detailUserRepo.getAllData();
//    }
    public  void insertData(User user){
        userRepo.insertData(user);
    }
    public  void deleteData(User user){
        userRepo.deleteData(user);
    }

    public void updateData(String name,String jenis_kelamin, String tanggal_lahir, String no_hp,String image, Integer id_user) {
        userRepo.updateData(name,jenis_kelamin,tanggal_lahir,no_hp,image,id_user);
    }
    public void updateNoImageData(String name,String jenis_kelamin, String tanggal_lahir, String no_hp, Integer id_user) {
        userRepo.updateNoImageData(name,jenis_kelamin,tanggal_lahir,no_hp,id_user);
    }
    public void DeleteALL() {
        userRepo.deleteAll();
    }

    public MutableLiveData<Boolean> getLoadingState() {
        return loadingState;
    }
}


