package id.difanesia.www.Helper;

import java.text.NumberFormat;
import java.util.Locale;

public class FormatCurrency {
    public String Rupiah(Double number){
        if (number==null) {
            number= (double) 0;
        }

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(number);

    }
}
