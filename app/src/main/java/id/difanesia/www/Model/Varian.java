package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Varian {

    @SerializedName("id_varian")
    @Expose
    private Integer idVarian;
    @SerializedName("id_product")
    @Expose
    private Integer idProduct;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("pathimage")
    @Expose
    private Object pathimage;
    @SerializedName("jenis")
    @Expose
    private String jenis;
    @SerializedName("varian")
    @Expose
    private String varian;
    @SerializedName("varian2")
    @Expose
    private String varian2;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("stock")
    @Expose
    private String stock;

    public Integer getIdVarian() {
        return idVarian;
    }

    public void setIdVarian(Integer idVarian) {
        this.idVarian = idVarian;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Object getPathimage() {
        return pathimage;
    }

    public void setPathimage(Object pathimage) {
        this.pathimage = pathimage;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getVarian() {
        return varian;
    }

    public void setVarian(String varian) {
        this.varian = varian;
    }

    public String getVarian2() {
        return varian2;
    }

    public void setVarian2(String varian2) {
        this.varian2 = varian2;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

}
