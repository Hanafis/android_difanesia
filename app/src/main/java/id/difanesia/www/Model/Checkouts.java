package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Checkouts {
    @SerializedName("id_merchant")
    @Expose
    private Integer idMerchant;
    @SerializedName("id_voucher")
    @Expose
    private Integer idVoucher;
    @SerializedName("total_potongan")
    @Expose
    private Double totalPotongan;
    @SerializedName("id_alamat")
    @Expose
    private Integer idAlamat;
    @SerializedName("products")
    @Expose
    private List<Products> products = null;
    @SerializedName("keterangan")
    @Expose
    private String keterangan;
    @SerializedName("nama_ekspedisi")
    @Expose
    private String tipe_ongkir;
    @SerializedName("tipe_ongkir")

    @Expose
    private String namaEkspedisi;
    @SerializedName("biaya_ongkir")
    @Expose
    private Double biayaOngkir;
    @SerializedName("donasi")
    @Expose
    private Double donasi;

    public Integer getIdMerchant() {
        return idMerchant;
    }

    public void setIdMerchant(Integer idMerchant) {
        this.idMerchant = idMerchant;
    }

    public Integer getIdVoucher() {
        return idVoucher;
    }

    public void setIdVoucher(Integer idVoucher) {
        this.idVoucher = idVoucher;
    }

    public Double getTotalPotongan() {
        return totalPotongan;
    }

    public void setTotalPotongan(Double  totalPotongan) {
        this.totalPotongan = totalPotongan;
    }

    public Integer getIdAlamat() {
        return idAlamat;
    }

    public void setIdAlamat(Integer idAlamat) {
        this.idAlamat = idAlamat;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNamaEkspedisi() {
        return namaEkspedisi;
    }

    public void setNamaEkspedisi(String namaEkspedisi) {
        this.namaEkspedisi = namaEkspedisi;
    }
    public String getTipe_ongkir() {
        return tipe_ongkir;
    }

    public void setTipe_ongkir(String tipe_ongkir) {
        this.tipe_ongkir = tipe_ongkir;
    }

    public Double getBiayaOngkir() {
        return biayaOngkir;
    }

    public void setBiayaOngkir(Double biayaOngkir) {
        this.biayaOngkir = biayaOngkir;
    }

    public Double getDonasi() {
        return donasi;
    }

    public void setDonasi(Double donasi) {
        this.donasi = donasi;
    }


    public Checkouts(Integer idMerchant, Integer idVoucher, Integer idAlamat, Double totalPotongan, List<Products> products, String keterangan, String namaEkspedisi, Double biayaOngkir, Double donasi,String tipe_ongkir){
       this.idMerchant=idMerchant;
       this.idVoucher=idVoucher;
       this.idAlamat=idAlamat;
       this.totalPotongan=totalPotongan;
       this.products=products;
       this.keterangan=keterangan;
       this.namaEkspedisi=namaEkspedisi;
       this.biayaOngkir=biayaOngkir;
       this.donasi=donasi;
       this.tipe_ongkir=tipe_ongkir;

    }
}
