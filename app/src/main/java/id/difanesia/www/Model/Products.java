package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class Products {
    @SerializedName("id_product")
    @Expose
    private Integer idProduct;
    @SerializedName("stock")
    @Expose
    private Integer stock;

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
    public Products(Integer id,Integer jml){
        this.idProduct=id;
        this.stock=jml;
    }

}
