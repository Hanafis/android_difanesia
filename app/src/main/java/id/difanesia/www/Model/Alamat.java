package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Alamat implements Serializable {
    @SerializedName("id_alamat")
    @Expose
    private Integer idAlamat;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("nama_lengkap")
    @Expose
    private String namaLengkap;
    @SerializedName("no_tlpn")
    @Expose
    private String noTlpn;
    @SerializedName("id_provinsi")
    @Expose
    private Integer idProvinsi;
    @SerializedName("provinsi")
    @Expose
    private String provinsi;
    @SerializedName("id_kabupaten")
    @Expose
    private Integer idKabupaten;
    @SerializedName("kabupaten")
    @Expose
    private String kabupaten;
    @SerializedName("id_kecamatan")
    @Expose
    private Integer idKecamatan;
    @SerializedName("kecamatan")
    @Expose
    private String kecamatan;
    @SerializedName("id_kelurahan")
    @Expose
    private Integer idKelurahan;
    @SerializedName("kelurahan")
    @Expose
    private String kelurahan;
    @SerializedName("kodepos")
    @Expose
    private String kodepos;
    @SerializedName("jalan")
    @Expose
    private String jalan;
    @SerializedName("lainya")
    @Expose
    private String lainya;
    @SerializedName("gedung")
    @Expose
    private String gedung;
    @SerializedName("utama")
    @Expose
    private String utama;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getIdAlamat() {
        return idAlamat;
    }

    public void setIdAlamat(Integer idAlamat) {
        this.idAlamat = idAlamat;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getNoTlpn() {
        return noTlpn;
    }

    public void setNoTlpn(String noTlpn) {
        this.noTlpn = noTlpn;
    }

    public Integer getIdProvinsi() {
        return idProvinsi;
    }

    public void setIdProvinsi(Integer idProvinsi) {
        this.idProvinsi = idProvinsi;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public Integer getIdKabupaten() {
        return idKabupaten;
    }

    public void setIdKabupaten(Integer idKabupaten) {
        this.idKabupaten = idKabupaten;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public Integer getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(Integer idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public Integer getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(Integer idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKodepos() {
        return kodepos;
    }

    public void setKodepos(String kodepos) {
        this.kodepos = kodepos;
    }

    public String getJalan() {
        return jalan;
    }

    public void setJalan(String jalan) {
        this.jalan = jalan;
    }

    public String getLainya() {
        return lainya;
    }

    public void setLainya(String lainya) {
        this.lainya = lainya;
    }

    public String getGedung() {
        return gedung;
    }

    public void setGedung(String gedung) {
        this.gedung = gedung;
    }

    public String getUtama() {
        return utama;
    }

    public void setUtama(String utama) {
        this.utama = utama;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Alamat(Integer idProvinsi,String provinsi,Integer idKabupaten,String kabupaten, Integer idKecamatan,String kecamatan,Integer idKelurahan,String kodepos){
        this.idProvinsi=idProvinsi;
        this.idKabupaten=idKabupaten;
        this.idKecamatan=idKecamatan;
        this.idKelurahan=idKelurahan;
        this.provinsi=provinsi;
        this.kabupaten=kabupaten;
        this.kecamatan=kecamatan;
        this.kodepos=kodepos;
    }
}
