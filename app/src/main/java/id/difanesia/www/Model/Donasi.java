package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Donasi implements Serializable {
    @SerializedName("id_donasi")
    @Expose
    private Integer idDonasi;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("judul_donasi")
    @Expose
    private String judulDonasi;
    @SerializedName("terkumpul")
    @Expose
    private String terkumpul;
    @SerializedName("target")
    @Expose
    private String target;
    @SerializedName("tanggalselesai")
    @Expose
    private String tanggalselesai;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("gambar")
    @Expose
    private String gambar;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("donatur")
    @Expose
    private Integer totalDonatur;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profiluser")
    @Expose
    private String profiluser;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfiluser() {
        return profiluser;
    }

    public void setProfiluser(String profiluser) {
        this.profiluser = profiluser;
    }

    public Integer getTotalDonatur() {
        return totalDonatur;
    }
    public void setTotalDonatur(Integer totalDonatur) {
        this.totalDonatur = totalDonatur;
    }

    public Integer getIdDonasi() {
        return idDonasi;
    }

    public void setIdDonasi(Integer idDonasi) {
        this.idDonasi = idDonasi;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getJudulDonasi() {
        return judulDonasi;
    }

    public void setJudulDonasi(String judulDonasi) {
        this.judulDonasi = judulDonasi;
    }

    public String getTerkumpul() {
        return terkumpul;
    }

    public void setTerkumpul(String terkumpul) {
        this.terkumpul = terkumpul;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTanggalselesai() {
        return tanggalselesai;
    }

    public void setTanggalselesai(String tanggalselesai) {
        this.tanggalselesai = tanggalselesai;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
