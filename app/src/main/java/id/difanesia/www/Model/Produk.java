package id.difanesia.www.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Produk implements Serializable {

    @SerializedName("id_product")
    @Expose
    private Integer idProduct;
    @SerializedName("id_transaksi_list")
    @Expose
    private Integer idTransaksiList;
    @SerializedName("id_toko")
    @Expose
    private Integer idToko;
    @SerializedName("id_wish")
    @Expose
    private Integer idWish;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("stock")
    @Expose
    private String stock;
    @SerializedName("varian_stock")
    @Expose
    private String varianStock;
    @SerializedName("kondisi")
    @Expose
    private String kondisi;
    @SerializedName("berbahaya")
    @Expose
    private String berbahaya;
    @SerializedName("terjual")
    @Expose
    private Integer terjual;
    @SerializedName("kota")
    @Expose
    private String kota;
    @SerializedName("rating")
    @Expose
    private Float rating;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("imagetoko")
    @Expose
    private String imageToko;

    @SerializedName("toko")
    @Expose
    private String toko;

    @SerializedName("jml")
    @Expose
    private Integer jml;

    @SerializedName("id_keranjang")
    @Expose
    private Integer idKeranjang;

    @SerializedName("id_varian")
    @Expose
    private Integer idVarian;

    @SerializedName("varian")
    @Expose
    private String varian;

    @SerializedName("is_rating")
    @Expose
    private Boolean isRating;

    @SerializedName("varian_harga")
    @Expose
    private String varianHarga;

    @SerializedName("varian_image")
    @Expose
    private String varianImage;

    @SerializedName("jumlah_barang")
    @Expose
    private Integer jumlah;

    @SerializedName("check")
    @Expose
    private boolean check=false;

    @SerializedName("status_rating")
    @Expose
    private String status_rating;

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Boolean getIsRating() {
        return isRating;
    }

    public void setIsRating(Boolean isRating) {
        this.isRating = isRating;
    }


    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdTransaksiList() {
        return idTransaksiList;
    }

    public void setIdTransaksiList(Integer idTransaksiList) {
        this.idTransaksiList = idTransaksiList;
    }

    public Integer getIdToko() {
        return idToko;
    }

    public void setIdToko(Integer idToko) {
        this.idToko = idToko;
    }

    public Integer getIdWish() {
        return idWish;
    }

    public void setIdWish(Integer idWish) {
        this.idWish = idWish;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public String getBerbahaya() {
        return berbahaya;
    }

    public void setBerbahaya(String berbahaya) {
        this.berbahaya = berbahaya;
    }

    public Integer getTerjual() {
        return terjual;
    }

    public void setTerjual(Integer terjual) {
        this.terjual = terjual;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }
    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageToko() {
        return imageToko;
    }

    public void setImageToko(String imageToko) {
        this.imageToko = imageToko;
    }

    public String getToko() { return toko; }
    public void setToko(String toko) {
        this.toko = toko;
    }

    public Integer getJml() { return jml; }
    public void setJml(Integer jml) {
        this.jml = jml;
    }

    public Integer getIdKeranjang() { return idKeranjang; }

    public void setIdKeranjang(Integer idKeranjang) {
        this.idKeranjang = idKeranjang;
    }

    public Integer getIdVarian() { return idVarian; }

    public void setIdVarian(Integer idVarian) {
        this.idVarian = idVarian;
    }

    public String getVarian() { return varian; }
    public void setVarian(String varian) {
        this.varian = varian;
    }

    public String getVarianHarga() { return varianHarga; }
    public void setVarianHarga(String varianHarga) {
        this.varianHarga = varianHarga;
    }

    public String getVarianImage() { return varianImage; }
    public void setVarianImage(String varianImage) {
        this.varianImage = varianImage;
    }

    public String getVarianStock() { return varianStock; }
    public void setVarianStock(String varianStock) {
        this.varianStock = varianStock;
    }

    public boolean getCheck() { return check; }
    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getStatusRating() {
        return status_rating;
    }

    public void setStatusRating(String status_rating) {
        this.status_rating = status_rating;
    }

}