package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingBintang {

    @SerializedName("lima")
    @Expose
    private Integer lima;
    @SerializedName("empat")
    @Expose
    private Integer empat;
    @SerializedName("tiga")
    @Expose
    private Integer tiga;
    @SerializedName("dua")
    @Expose
    private Integer dua;
    @SerializedName("satu")
    @Expose
    private Integer satu;

    public Integer getLima() {
        return lima;
    }

    public void setLima(Integer lima) {
        this.lima = lima;
    }

    public Integer getEmpat() {
        return empat;
    }

    public void setEmpat(Integer empat) {
        this.empat = empat;
    }

    public Integer getTiga() {
        return tiga;
    }

    public void setTiga(Integer tiga) {
        this.tiga = tiga;
    }

    public Integer getDua() {
        return dua;
    }

    public void setDua(Integer dua) {
        this.dua = dua;
    }

    public Integer getSatu() {
        return satu;
    }

    public void setSatu(Integer satu) {
        this.satu = satu;
    }

}
