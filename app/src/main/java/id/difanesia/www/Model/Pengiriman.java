package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pengiriman {
    @SerializedName("id_ongkir")
    @Expose
    private Integer idOngkir;
    @SerializedName("id_alamat")
    @Expose
    private Integer idAlamat;
    @SerializedName("id_transaksi")
    @Expose
    private Integer idTransaksi;
    @SerializedName("resi")
    @Expose
    private String resi;
    @SerializedName("nama_ekspedisi")
    @Expose
    private String namaEkspedisi;
    @SerializedName("nominal_ongkir")
    @Expose
    private Integer nominalOngkir;
    @SerializedName("berat")
    @Expose
    private String berat;
    @SerializedName("lebar")
    @Expose
    private String lebar;
    @SerializedName("panjang")
    @Expose
    private String panjang;
    @SerializedName("tinggi")
    @Expose
    private String tinggi;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getIdOngkir() {
        return idOngkir;
    }

    public void setIdOngkir(Integer idOngkir) {
        this.idOngkir = idOngkir;
    }

    public Integer getIdAlamat() {
        return idAlamat;
    }

    public void setIdAlamat(Integer idAlamat) {
        this.idAlamat = idAlamat;
    }

    public Integer getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(Integer idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public String getResi() {
        return resi;
    }

    public void setResi(String resi) {
        this.resi = resi;
    }

    public String getNamaEkspedisi() {
        return namaEkspedisi;
    }

    public void setNamaEkspedisi(String namaEkspedisi) {
        this.namaEkspedisi = namaEkspedisi;
    }

    public Integer getNominalOngkir() {
        return nominalOngkir;
    }

    public void setNominalOngkir(Integer nominalOngkir) {
        this.nominalOngkir = nominalOngkir;
    }

    public String getBerat() {
        return berat;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }

    public String getLebar() {
        return lebar;
    }

    public void setLebar(String lebar) {
        this.lebar = lebar;
    }

    public String getPanjang() {
        return panjang;
    }

    public void setPanjang(String panjang) {
        this.panjang = panjang;
    }

    public String getTinggi() {
        return tinggi;
    }

    public void setTinggi(String tinggi) {
        this.tinggi = tinggi;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
