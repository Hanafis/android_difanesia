package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Donatur {
    @SerializedName("id_penerima")
    @Expose
    private Integer idPenerima;
    @SerializedName("id_donatur")
    @Expose
    private Integer idDonatur;
    @SerializedName("id_donasi")
    @Expose
    private Integer idDonasi;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profiluser")
    @Expose
    private String profiluser;
    @SerializedName("donasi")
    @Expose
    private String donasi;

    public Integer getIdPenerima() {
        return idPenerima;
    }

    public void setIdPenerima(Integer idPenerima) {
        this.idPenerima = idPenerima;
    }
    public Integer getIdDonatur() {
        return idDonatur;
    }

    public void setIdDonatur(Integer idDonatur) {
        this.idDonatur = idDonatur;
    }

    public Integer getIdDonasi() {
        return idDonasi;
    }

    public void setIdDonasi(Integer idDonasi) {
        this.idDonasi = idDonasi;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfiluser() {
        return profiluser;
    }

    public void setProfiluser(String profiluser) {
        this.profiluser = profiluser;

    }public String getDonasi() {
        return donasi;
    }

    public void setDonasi(String donasi) {
        this.donasi = donasi;
    }


}
