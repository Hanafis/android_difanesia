package id.difanesia.www.Model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tb_search")
public class Search {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int _id;

    
    @SerializedName("keyword")
    @Expose
    private String keyword;

    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int get_id() { return _id; }

    public void set_id(int _id) { this._id = _id;}

    public Search(String keyword){
        this.keyword=keyword;
    }
}
