package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Payment implements Serializable {
    private String metode;
    private String bank;
    private String kode;

    @SerializedName("response_description")
    @Expose
    private String responseDescription;
    @SerializedName("payment_page_url")
    @Expose
    private Object paymentPageUrl;
    @SerializedName("response_code")
    @Expose
    private String responseCode;
    @SerializedName("transaction_status")
    @Expose
    private String transactionStatus;
    @SerializedName("va_number_list")
    @Expose
    private String vaNumberList;
    @SerializedName("debitin_session_token")
    @Expose
    private Object debitinSessionToken;
    @SerializedName("creditcard_form_url")
    @Expose
    private Object creditcardFormUrl;
    @SerializedName("qris_data")
    @Expose
    private Object qrisData;
    @SerializedName("plink_ref_no")
    @Expose
    private String plinkRefNo;
    @SerializedName("creditcard_session_token")
    @Expose
    private Object creditcardSessionToken;
    @SerializedName("debitin_form_url")
    @Expose
    private Object debitinFormUrl;
    @SerializedName("response_message")
    @Expose
    private String responseMessage;
    @SerializedName("initial_data_customer")
    @Expose
    private Object initialDataCustomer;
    @SerializedName("validity")
    @Expose
    private String validity;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }
    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }
    public String getMetode() {
        return metode;
    }

    public void setMetode(String metode) {
        this.metode = metode;
    }

    public Payment(String metode,String bank, String kode){
        this.metode=metode;
        this.bank=bank;
        this.kode=kode;
    }



    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public Object getPaymentPageUrl() {
        return paymentPageUrl;
    }

    public void setPaymentPageUrl(Object paymentPageUrl) {
        this.paymentPageUrl = paymentPageUrl;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getVaNumberList() {
        return vaNumberList;
    }

    public void setVaNumberList(String vaNumberList) {
        this.vaNumberList = vaNumberList;
    }

    public Object getDebitinSessionToken() {
        return debitinSessionToken;
    }

    public void setDebitinSessionToken(Object debitinSessionToken) {
        this.debitinSessionToken = debitinSessionToken;
    }

    public Object getCreditcardFormUrl() {
        return creditcardFormUrl;
    }

    public void setCreditcardFormUrl(Object creditcardFormUrl) {
        this.creditcardFormUrl = creditcardFormUrl;
    }

    public Object getQrisData() {
        return qrisData;
    }

    public void setQrisData(Object qrisData) {
        this.qrisData = qrisData;
    }

    public String getPlinkRefNo() {
        return plinkRefNo;
    }

    public void setPlinkRefNo(String plinkRefNo) {
        this.plinkRefNo = plinkRefNo;
    }

    public Object getCreditcardSessionToken() {
        return creditcardSessionToken;
    }

    public void setCreditcardSessionToken(Object creditcardSessionToken) {
        this.creditcardSessionToken = creditcardSessionToken;
    }

    public Object getDebitinFormUrl() {
        return debitinFormUrl;
    }

    public void setDebitinFormUrl(Object debitinFormUrl) {
        this.debitinFormUrl = debitinFormUrl;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Object getInitialDataCustomer() {
        return initialDataCustomer;
    }

    public void setInitialDataCustomer(Object initialDataCustomer) {
        this.initialDataCustomer = initialDataCustomer;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

}
