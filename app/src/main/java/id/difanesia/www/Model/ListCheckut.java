package id.difanesia.www.Model;

import java.util.List;

public class ListCheckut {
    private List<Checkouts> merchants;
    private String bank_id;
    private String methode;
    private List<Integer> carts;
    public List<Checkouts> getCheckouts() {
        return merchants;
    }

    public void setIdMerchant(List<Checkouts> merchants) {
        this.merchants = merchants;
    }

    public List<Integer> getId_keranjang() {
        return carts;
    }

    public void setId_keranjang(List<Integer> carts) {
        this.carts = carts;
    }
    public String getBank_id() {
        return bank_id;
    }

    public void setBank_id(String bank_id) {
        this.bank_id = bank_id;
    }

    public List<Checkouts> getMerchants() {
        return merchants;
    }

    public List<Integer> getCarts() {
        return carts;
    }

    public String getMethode() {
        return methode;
    }

    public void setCarts(List<Integer> carts) {
        this.carts = carts;
    }

    public void setMerchants(List<Checkouts> merchants) {
        this.merchants = merchants;
    }

    public void setMethode(String methode) {
        this.methode = methode;
    }

    public ListCheckut(List<Checkouts> list, String bank_id, List<Integer> carts,String methode){
        this.merchants=list;
        this.bank_id=bank_id;
        this.carts=carts;
        this.methode=methode;
    }
}
