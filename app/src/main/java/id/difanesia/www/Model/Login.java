package id.difanesia.www.Model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Entity(tableName = "tb_login")
public final class Login {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int _id;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("id_user")
    @Expose
    private Integer id_user;


    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }



    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getId_user() {
        return id_user;
    }
    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public int get_id() { return _id; }

    public void set_id(int _id) { this._id = _id;}


}

