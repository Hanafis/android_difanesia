package id.difanesia.www.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Notifikasi {
    @SerializedName("id_notifikasi")
    @Expose
    private Integer idNotifikasi;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("id_product")
    @Expose
    private Integer idProduct;
    @SerializedName("id_kupon")
    @Expose
    private Integer idKupon;
    @SerializedName("jenis")
    @Expose
    private String jenis;
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("pesan")
    @Expose
    private String pesan;
    @SerializedName("dibaca")
    @Expose
    private String dibaca;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("gambar")
    @Expose
    private String gambar;

    public Integer getIdNotifikasi() {
        return idNotifikasi;
    }

    public void setIdNotifikasi(Integer idNotifikasi) {
        this.idNotifikasi = idNotifikasi;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdKupon() {
        return idKupon;
    }

    public void setIdKupon(Integer idKupon) {
        this.idKupon = idKupon;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getDibaca() {
        return dibaca;
    }

    public void setDibaca(String dibaca) {
        this.dibaca = dibaca;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

}
