package id.difanesia.www.Model;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Welcome {



    public Welcome(String judul, String isi, Integer gambar){
        this.judul=judul;
        this.isi=isi;
        this.gambar=gambar;
    }
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("isi")
    @Expose
    private String isi;
    @SerializedName("gambar")
    @Expose
    private Integer gambar;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public Integer getGambar() {
        return gambar;
    }

    public void setGambar(Integer gambar) {
        this.gambar = gambar;
    }
}
