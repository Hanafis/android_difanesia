package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailTransaksi {
    @SerializedName("id_transaksi")
    @Expose
    private Integer idTransaksi;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("id_merchant")
    @Expose
    private Integer idMerchant;
    @SerializedName("id_kupon")
    @Expose
    private Object idKupon;
    @SerializedName("kode_transaksi")
    @Expose
    private String kodeTransaksi;
    @SerializedName("total_potongan")
    @Expose
    private Integer totalPotongan;
    @SerializedName("sub_total_transaksi")
    @Expose
    private String subTotalTransaksi;
    @SerializedName("sub_total_barang")
    @Expose
    private String subTotalBarang;
    @SerializedName("sub_total_donasi")
    @Expose
    private String subTotalDonasi;
    @SerializedName("biaya_presmalink")
    @Expose
    private String biayaPresmalink;
    @SerializedName("ongkir")
    @Expose
    private String ongkir;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("keterangan")
    @Expose
    private String keterangan;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("Produk")
    @Expose
    private List<Produk> produk = null;
    @SerializedName("alamat")
    @Expose
    private Alamat alamat;
    @SerializedName("pengiriman")
    @Expose
    private Pengiriman pengiriman;
    @SerializedName("payment_information")
    @Expose
    private PaymentInfromation paymentInformation = null;
    public Integer getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(Integer idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdMerchant() {
        return idMerchant;
    }

    public void setIdMerchant(Integer idMerchant) {
        this.idMerchant = idMerchant;
    }

    public Object getIdKupon() {
        return idKupon;
    }

    public void setIdKupon(Object idKupon) {
        this.idKupon = idKupon;
    }

    public String getKodeTransaksi() {
        return kodeTransaksi;
    }

    public void setKodeTransaksi(String kodeTransaksi) {
        this.kodeTransaksi = kodeTransaksi;
    }

    public Integer getTotalPotongan() {
        return totalPotongan;
    }

    public void setTotalPotongan(Integer totalPotongan) {
        this.totalPotongan = totalPotongan;
    }

    public String getSubTotalTransaksi() {
        return subTotalTransaksi;
    }

    public void setSubTotalTransaksi(String subTotalTransaksi) {
        this.subTotalTransaksi = subTotalTransaksi;
    }

    public String getSubTotalBarang() {
        return subTotalBarang;
    }

    public void setSubTotalBarang(String subTotalBarang) {
        this.subTotalBarang = subTotalBarang;
    }

    public String getSubTotalDonasi() {
        return subTotalDonasi;
    }

    public void setSubTotalDonasi(String subTotalDonasi) {
        this.subTotalDonasi = subTotalDonasi;
    }

    public String getBiayaPresmalink() {
        return biayaPresmalink;
    }

    public void setBiayaPresmalink(String biayaPresmalink) {
        this.biayaPresmalink = biayaPresmalink;
    }

    public String getOngkir() {
        return ongkir;
    }

    public void setOngkir(String ongkir) {
        this.ongkir = ongkir;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Produk> getProduk() {
        return produk;
    }

    public void setProduk(List<Produk> produk) {
        this.produk = produk;
    }

    public Alamat getAlamat() {
        return alamat;
    }

    public void setAlamat(Alamat alamat) {
        this.alamat = alamat;
    }

    public Pengiriman getPengiriman() {
        return pengiriman;
    }

    public void setPengiriman(Pengiriman pengiriman) {
        this.pengiriman = pengiriman;
    }
    public PaymentInfromation getPaymentInformation() {
        return paymentInformation;
    }

    public void setPaymentInformation(PaymentInfromation paymentInformation) {
        this.paymentInformation = paymentInformation;
    }
}
