package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Voucher implements Serializable {

        @SerializedName("id_kupon_list")
        @Expose
        private Integer idKuponList;
        @SerializedName("id_kupon")
        @Expose
        private Integer idKupon;
        @SerializedName("id_product")
        @Expose
        private Integer idProduct;
        @SerializedName("id_user")
        @Expose
        private Integer idUser;
        @SerializedName("id_transaksi")
        @Expose
        private Integer idTransaksi;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("expired_date")
        @Expose
        private String expiredDate;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("syarat")
        @Expose
        private String syarat;
        @SerializedName("nama_kupon")
        @Expose
        private String namaKupon;
        @SerializedName("kode")
        @Expose
        private String kode;
        @SerializedName("minimum")
        @Expose
        private String minimum;
        @SerializedName("deskripsi")
        @Expose
        private String deskripsi;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("persen")
        @Expose
        private Object persen;
        @SerializedName("gambar")
        @Expose
        private String gambar;

        public Integer getIdKuponList() {
            return idKuponList;
        }

        public void setIdKuponList(Integer idKuponList) {
            this.idKuponList = idKuponList;
        }

        public Integer getIdKupon() {
            return idKupon;
        }

        public void setIdKupon(Integer idKupon) {
            this.idKupon = idKupon;
        }

        public Integer getIdProduct() {
            return idProduct;
        }

        public void setIdProduct(Integer idProduct) {
            this.idProduct = idProduct;
        }

        public Integer getIdUser() {
            return idUser;
        }

        public void setIdUser(Integer idUser) {
            this.idUser = idUser;
        }

        public Integer getIdTransaksi() {
            return idTransaksi;
        }

        public void setIdTransaksi(Integer idTransaksi) {
            this.idTransaksi = idTransaksi;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getExpiredDate() {
            return expiredDate;
        }

        public void setExpiredDate(String expiredDate) {
            this.expiredDate = expiredDate;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getSyarat() {
            return syarat;
        }

        public void setSyarat(String syarat) {
            this.syarat = syarat;
        }

        public String getNamaKupon() {
            return namaKupon;
        }

        public void setNamaKupon(String namaKupon) {
            this.namaKupon = namaKupon;
        }

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

        public String getMinimum() {
            return minimum;
        }

        public void setMinimum(String minimum) {
            this.minimum = minimum;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Object getPersen() {
            return persen;
        }

        public void setPersen(Object persen) {
            this.persen = persen;
        }

        public String getGambar() {
            return gambar;
        }

        public void setGambar(String gambar) {
            this.gambar = gambar;
        }
}
