package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Provinsi {

    @SerializedName("kota")
    @Expose
    private String kota;

    @SerializedName("pilih")
    @Expose
    private Integer pilih;

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public Integer getPilih() {
        return pilih;
    }

    public void setPilih(Integer kota) {
        this.pilih = pilih;
    }

    public Provinsi(String kota,Integer value){
        this.kota=kota;
        this.pilih=value;

    }

}
