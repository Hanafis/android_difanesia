package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListAlamat {
    @SerializedName("provinsi")
    @Expose
    private String provinsi;
    @SerializedName("id_provinsi")
    @Expose
    private String idProvinsi;

    @SerializedName("kabupaten")
    @Expose
    private String kabupaten;
    @SerializedName("id_kabupaten")
    @Expose
    private String idKabupaten;

    @SerializedName("kecamatan")
    @Expose
    private String kecamatan;
    @SerializedName("id_kecamatan")
    @Expose
    private String idKecamatan;

    @SerializedName("kelurahan")
    @Expose
    private String kelurahan;
    @SerializedName("id_kelurahan")
    @Expose
    private String idKelurahan;

    @SerializedName("KodePos")
    @Expose
    private Integer kodePos;
    @SerializedName("id_kodepos")
    @Expose
    private Integer idKodepos;

    public Integer getKodePos() {
        return kodePos;
    }

    public void setKodePos(Integer kodePos) {
        this.kodePos = kodePos;
    }

    public Integer getIdKodepos() {
        return idKodepos;
    }

    public void setIdKodepos(Integer idKodepos) {
        this.idKodepos = idKodepos;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(String idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(String idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getIdKabupaten() {
        return idKabupaten;
    }

    public void setIdKabupaten(String idKabupaten) {
        this.idKabupaten = idKabupaten;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getIdProvinsi() {
        return idProvinsi;
    }

    public void setIdProvinsi(String idProvinsi) {
        this.idProvinsi = idProvinsi;
    }
}
