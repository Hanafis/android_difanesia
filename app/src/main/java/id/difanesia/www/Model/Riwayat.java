package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Riwayat implements Serializable {
    @SerializedName("id_transaksi")
    @Expose
    private Integer idTransaksi;
    @SerializedName("rounded")
    @Expose
    private Boolean rounded=false;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("id_voucher")
    @Expose
    private Integer idVoucher;
    @SerializedName("id_alamat")
    @Expose
    private Integer idAlamat;
    @SerializedName("id_ongkir")
    @Expose
    private Integer idOngkir;
    @SerializedName("kode_transaksi")
    @Expose
    private String kodeTransaksi;
    @SerializedName("jumlah_bayar")
    @Expose
    private String jumlahBayar;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("produk")
    @Expose
    private Produk produk;

    @SerializedName("pesan")
    @Expose
    private String pesan;
    @SerializedName("lacakPaket")
    @Expose
    private String lacakPaket;



    public Integer getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(Integer idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdVoucher() {
        return idVoucher;
    }

    public void setIdVoucher(Integer idVoucher) {
        this.idVoucher = idVoucher;
    }

    public Integer getIdAlamat() {
        return idAlamat;
    }

    public void setIdAlamat(Integer idAlamat) {
        this.idAlamat = idAlamat;
    }

    public Integer getIdOngkir() {
        return idOngkir;
    }

    public void setIdOngkir(Integer idOngkir) {
        this.idOngkir = idOngkir;
    }

    public String getKodeTransaksi() {
        return kodeTransaksi;
    }

    public void setKodeTransaksi(String kodeTransaksi) {
        this.kodeTransaksi = kodeTransaksi;
    }

    public String getJumlahBayar() {
        return jumlahBayar;
    }

    public void setJumlahBayar(String jumlahBayar) {
        this.jumlahBayar = jumlahBayar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Produk getProduk() {
        return produk;
    }

    public void setProduk(Produk produk) {
        this.produk = produk;
    }


    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getLacakPaket() {
        return lacakPaket;
    }

    public void setLacakPaket(String lacakPaket) {
        this.lacakPaket = lacakPaket;
    }

    public Boolean getRounded() {
        return rounded;
    }

    public void setRounded(Boolean rounded) {
        this.rounded = rounded;
    }

}
