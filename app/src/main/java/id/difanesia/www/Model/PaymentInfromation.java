package id.difanesia.www.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentInfromation implements Serializable {
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("bank_id")
    @Expose
    private String bankId;
    @SerializedName("va")
    @Expose
    private String va;
    @SerializedName("validity")
    @Expose
    private String validity;

    @SerializedName("id_transaksi_wrap")
    @Expose
    private Integer idTransaksi;

    @SerializedName("manual")
    @Expose
    private Boolean manual;

    public Integer getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(Integer idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public Boolean getManual() {
        return manual;
    }

    public void setManual(Boolean manual) {
        this.manual = manual;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getVa() {
        return va;
    }

    public void setVa(String va) {
        this.va = va;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }
}
