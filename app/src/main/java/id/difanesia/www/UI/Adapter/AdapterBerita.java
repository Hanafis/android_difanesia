package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import id.difanesia.www.Model.Berita;
import id.difanesia.www.R;

public class AdapterBerita extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Berita> beritas;
    OnclickButton onclickButton;
    public AdapterBerita(Context context,List<Berita> beritas,OnclickButton onclickButton){
            this.context=context;
            this.beritas=beritas;
            this.onclickButton=onclickButton;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_berita, parent, false);
        return new BeritaViewHolder(view, onclickButton);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof AdapterBerita.BeritaViewHolder){
                final AdapterBerita.BeritaViewHolder beritaViewHolder= (BeritaViewHolder) holder;
                beritaViewHolder.juduBerita.setText(beritas.get(position).getJudulBerita());
                beritaViewHolder.deskripsiBerita.setText(beritas.get(position).getDeskripsi());
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                try {
                    date = simpleDateFormat.parse(beritas.get(position).getCreatedAt());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat convetDateFormat = new SimpleDateFormat("MMMM d,yyyy");
                assert date != null;
                String Htanggal=convetDateFormat.format(date);
                beritaViewHolder.tanggalBerita.setText(Htanggal);
                if (beritas.get(position).getGambar()!=null){
                    if (!beritas.get(position).getGambar().equals("")){
                        Picasso.get().load(context.getString(R.string.path)  +"/uploads/products/"+ beritas.get(position).getGambar()).into(beritaViewHolder.gambarBerita);
                    }
                }
                beritaViewHolder.layoutBerita.setOnClickListener(view -> onclickButton.OnclickBerita(beritaViewHolder.getAdapterPosition()));

            }
    }

    @Override
    public int getItemCount() {
        return this.beritas==null?0:beritas.size();
    }

    private static class BeritaViewHolder extends RecyclerView.ViewHolder implements OnclickButton{
        TextView juduBerita,deskripsiBerita,tanggalBerita;
        ImageView gambarBerita;
        LinearLayout layoutBerita;
        public BeritaViewHolder(View view, OnclickButton onclickButton) {
            super(view);
            juduBerita=view.findViewById(R.id.JudulBerita);
            deskripsiBerita=view.findViewById(R.id.DeskripsiBerita);
            tanggalBerita=view.findViewById(R.id.TanggalBerita);
            gambarBerita=view.findViewById(R.id.GambarBerita);
            layoutBerita=view.findViewById(R.id.LayoutBerita);
        }

        @Override
        public void OnclickBerita(int posisi) {

        }
    }
    public interface OnclickButton{
        public void OnclickBerita(int posisi);
    }
}
