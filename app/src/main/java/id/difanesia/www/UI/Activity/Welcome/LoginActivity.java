package id.difanesia.www.UI.Activity.Welcome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.MainActivity;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.ResetPassword.ForgetPasswordActivity;
import id.difanesia.www.ViewModel.LoginViewModel;
import id.difanesia.www.ViewModel.UserViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    Button BtnMasuk,BtnDaftar;
    TextView Lupapassword;
    ApiRequest apiinterface;
    UserViewModel userViewModel;
    LoginViewModel loginViewModel;
    private EditText editTextEmail, editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        BtnDaftar=findViewById(R.id.BtnDaftar);
        BtnMasuk=findViewById(R.id.BtnMasuk);
        Lupapassword=findViewById(R.id.BtnLupa);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        editTextEmail = (EditText) findViewById(R.id.EmailTextEdit);
        editTextPassword = (EditText) findViewById(R.id.PasswordBaru);
        BtnMasuk.setOnClickListener(view -> Validate());
        BtnDaftar.setOnClickListener(view -> {
            Intent daftar = new Intent(LoginActivity.this, RegistrasiActivity.class);
            startActivity(daftar);
        });
        Lupapassword.setOnClickListener(view -> {
            Intent lupa = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
            startActivity(lupa);
        });
        editTextPassword.setOnKeyListener((view, i, keyEvent) -> {
            if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                Validate();
                return true;
            }
            return false;
        });
    }
    public void Validate(){
        if (editTextEmail.getText().toString().matches("")) {
            editTextEmail.setError("Kolom Email Tidak Boleh Kosong");
            editTextEmail.requestFocus();
            return;

        }
        if (editTextPassword.getText().toString().matches("")) {
            editTextPassword.setError("Kolom Password Tidak Boleh Kosong");
            editTextPassword.requestFocus();
            return;

        }
        userSignIn();
    }
    private void userSignIn() {

        //defining a progress dialog to show while signing up
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loggin Up...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        //getting the user values

        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        //building retrofit object

        //Save Data
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);

        //defining the call
        Call<ApiResponse> call = apiinterface.login(email,password);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                progressDialog.dismiss();
                if(response.body()!=null) {
                    if (response.body().getLogin()!=null) {
                        loginViewModel.insertData(response.body().getLogin());
                        ApiDetailUser(response.body().getLogin().getToken(),response.body().getLogin().getId_user());
                    } else {
                        Toast.makeText(getApplicationContext(),"Username Atau Password Yang Anda Masukan Salah",Toast.LENGTH_LONG).show();

                    }
                }
                else{
                    if (response.errorBody()!=null){
                        Toast.makeText(getApplicationContext(),"Username Atau Password Yang Anda Masukan Salah",Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Gagal Terhubung Ke Server", Toast.LENGTH_LONG).show();
                    }
                }



            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.toString(),Toast.LENGTH_LONG);
            }
        });

    }
    public void ApiDetailUser(String Token,Integer id_user){
        Call<ApiResponse> call = apiinterface.userdetail("Bearer "+Token,id_user);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if(response.body()!=null){
                    if(response.body().getUser()!=null){
                        userViewModel.insertData(response.body().getUser());

                    }
                }
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}