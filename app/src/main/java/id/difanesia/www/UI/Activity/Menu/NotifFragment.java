package id.difanesia.www.UI.Activity.Menu;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Riwayat.RiwayatActivity;
import id.difanesia.www.UI.Adapter.AdapterNotifikasi;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotifFragment extends Fragment {
    View view;
    LinearLayout btnBayar,btnKemas,btnKirim,btnRiwayat,btnBelumDibaca,layoutNotNotif;
    TextView tandaiBelumDibaca;
    RecyclerView listNotifikasi;
    Button btnPesanan,btnpromo;
    String Token;
    Integer id_users,Limit=12,Start=0;
    ApiRequest apiinterface;
    LoginViewModel loginViewModel;
    AdapterNotifikasi adapterNotifikasi;
    CardView badageBayar,badageDikemas,badageDikirim,badageRiwayat;
    TextView badageBayarText,badageDikemasText,badageDikirimText,badageRiwayatText;
    String Jenis="pesanan";
    ScrollView scroll;
    ShimmerFrameLayout loading;
    public NotifFragment() {
        // Required empty public constructor
    }




    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_notif, container, false);
        apiinterface = ApiConfig.getClient(getContext()).create(ApiRequest.class);
        btnBayar=view.findViewById(R.id.BtnBayar);
        btnKemas=view.findViewById(R.id.BtnKemas);
        btnKirim=view.findViewById(R.id.BtnKirim);
        btnRiwayat=view.findViewById(R.id.BtnRiwayat);
        btnPesanan=view.findViewById(R.id.BtnPesanan);
        btnpromo=view.findViewById(R.id.BtnPromo);
        btnBelumDibaca=view.findViewById(R.id.BtnBelumDibaca);
        tandaiBelumDibaca=view.findViewById(R.id.TandaiDibaca);
        badageDikemas=view.findViewById(R.id.BadageDiKemas);
        badageDikirim=view.findViewById(R.id.BadageDiKirim);
        badageRiwayat=view.findViewById(R.id.BadageRiwayat);
        badageBayar=view.findViewById(R.id.BadageDibayar);
        badageDikemasText=view.findViewById(R.id.BadageDiKemasText);
        badageDikirimText=view.findViewById(R.id.BadageDiKirimText);
        badageRiwayatText=view.findViewById(R.id.BadageRiwayatText);
        badageBayarText=view.findViewById(R.id.BadageDibayarText);
        scroll=view.findViewById(R.id.Scroll);
        loading=view.findViewById(R.id.Loading);
        loading.startShimmer();
        listNotifikasi=view.findViewById(R.id.ListNotifikasi);
        layoutNotNotif=view.findViewById(R.id.LayoutNot);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        listNotifikasi.setLayoutManager(layoutManager);
        listNotifikasi.setHasFixedSize(true);
        listNotifikasi.setNestedScrollingEnabled(false);
        btnKirim.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), RiwayatActivity.class);
            Aksi(intent,"Kirim");
        });
        btnBayar.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), RiwayatActivity.class);
            Aksi(intent,"Bayar");
        });
        btnKemas.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), RiwayatActivity.class);
            Aksi(intent,"Kemas");
        });
        btnRiwayat.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), RiwayatActivity.class);
            Aksi(intent,"Riwayat");
        });
        Token();
        btnPesanan.setOnClickListener(view1 -> {
            ApiNotifikasi("pesanan");
            Jenis="pesanan";
            btnPesanan.setBackground(getContext().getResources().getDrawable(R.drawable.button_rounded_hijau));
            btnPesanan.setTextColor(getContext().getResources().getColor(R.color.white));
            btnpromo.setBackgroundColor(getContext().getResources().getColor(R.color.transparant));
            btnpromo.setTextColor(getContext().getResources().getColor(R.color.black));
        });
        btnpromo.setOnClickListener(view1 -> {
            ApiNotifikasi("promo");
            Jenis="promo";
            btnpromo.setBackground(getContext().getResources().getDrawable(R.drawable.button_rounded_hijau));
            btnpromo.setTextColor(getContext().getResources().getColor(R.color.white));
            btnPesanan.setBackgroundColor(getContext().getResources().getColor(R.color.transparant));
            btnPesanan.setTextColor(getContext().getResources().getColor(R.color.black));
        });
        btnBelumDibaca.setOnClickListener(view1 -> ApiAllDibaca());

        return view;
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(getViewLifecycleOwner(), login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiNotifikasi("pesanan");
                    ApiRiwayat();

                }
                else {
                    Toast.makeText(getContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void Aksi(Intent intent,String jenis){
        intent.putExtra("RIWAYAT",jenis);
        startActivity(intent);

    }
    public void ApiNotifikasi(String jenis){
        Call<ApiResponse> call = apiinterface.Notifikasi(Token,id_users,jenis);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getNotifikasi().size()>0){
                        layoutNotNotif.setVisibility(View.GONE);
                        adapterNotifikasi=new AdapterNotifikasi(getContext(), response.body().getNotifikasi(), (posisi, dibaca) -> {
                            if (dibaca.equals("1")) {
                                ApiDibaca(posisi,response.body().getNotifikasi().get(posisi).getIdNotifikasi());
                            }
                        });
                        listNotifikasi.setAdapter(adapterNotifikasi);
                        adapterNotifikasi.notifyDataSetChanged();
                    }
                    if (response.body().getDibaca()!=null){
                        tandaiBelumDibaca.setText("( "+response.body().getDibaca()+" )");
                    }
                    scroll.setVisibility(View.GONE);
                    loading.setVisibility(View.GONE);
                    loading.stopShimmer();
                }else{
                    scroll.setVisibility(View.GONE);
                    loading.setVisibility(View.GONE);
                    loading.stopShimmer();
                    layoutNotNotif.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void ApiDibaca(int Posisi,Integer id_notifikasi){
        Call<ApiResponse> call = apiinterface.NotifikasiDibaca(Token,id_users,id_notifikasi);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()){
                        adapterNotifikasi.Dibaca(Posisi);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void ApiAllDibaca(){
        Call<ApiResponse> call = apiinterface.NotifikasiSemuaDibaca(Token,id_users,Jenis);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()){
                        ApiNotifikasi(Jenis);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void ApiRiwayat(){
        Call<ApiResponse> call = apiinterface.RiwayatJumlah(Token,id_users);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getBayar()>0){
                        badageBayar.setVisibility(View.VISIBLE);
                        badageBayarText.setText(response.body().getBayar().toString().trim());
                    }
                    if (response.body().getKemas()>0){
                        badageDikemas.setVisibility(View.VISIBLE);
                        badageDikemasText.setText(response.body().getKemas().toString().trim());
                    }
                    if (response.body().getKirim()>0){
                        badageDikirim.setVisibility(View.VISIBLE);
                        badageDikirimText.setText(response.body().getKirim().toString().trim());
                    }
                    if (response.body().getSelesai()>0){
                        badageRiwayat.setVisibility(View.VISIBLE);
                        badageRiwayatText.setText(response.body().getSelesai().toString().trim());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}