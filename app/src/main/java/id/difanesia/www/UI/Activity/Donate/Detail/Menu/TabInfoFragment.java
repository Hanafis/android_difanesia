package id.difanesia.www.UI.Activity.Donate.Detail.Menu;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.Donasi;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterDonatur;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TabInfoFragment extends Fragment {
        View view;
        Donasi donasi;
        TextView namaPenggalang;
        CircleImageView profilPenggalang;
        RecyclerView listPenerimaDana;
        ApiRequest apiinterface;
        LoginViewModel loginViewModel;
        String Token;
        Integer id_users,Limit=12,Start=0;
        AdapterDonatur adapterPenerima;


    public TabInfoFragment(Donasi donasi) {
        this.donasi=donasi;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_tab_info, container, false);
        apiinterface= ApiConfig.getClient(getContext()).create(ApiRequest.class);
        namaPenggalang=view.findViewById(R.id.NamaPenggalang);
        profilPenggalang=view.findViewById(R.id.GambarDonasi);
        listPenerimaDana=view.findViewById(R.id.ListPenerimaDana);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        listPenerimaDana.setLayoutManager(layoutManager);
        listPenerimaDana.setHasFixedSize(true);
        listPenerimaDana.setNestedScrollingEnabled(false);
        if (donasi!=null){
             namaPenggalang.setText(donasi.getName());
             Picasso.get().load(getResources().getString(R.string.path)+"/uploads/products/"+donasi.getProfiluser()).into(profilPenggalang);
            Token();
        }


        return view;
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(getViewLifecycleOwner(), login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiDonasi();

                }
                else {
                    Toast.makeText(getContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void ApiDonasi(){
        Call<ApiResponse> call = apiinterface.PenerimaDonasi(Token,donasi.getIdDonasi(),Start,Limit);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getPenerima().size()>0){
                        adapterPenerima=new AdapterDonatur(getActivity(),response.body().getPenerima());
                        listPenerimaDana.setAdapter(adapterPenerima);
                        adapterPenerima.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}