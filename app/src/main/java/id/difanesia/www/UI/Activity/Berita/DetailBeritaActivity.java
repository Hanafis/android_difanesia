package id.difanesia.www.UI.Activity.Berita;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.Berita;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Donate.Detail.DetailDonasiActivity;
import id.difanesia.www.UI.Adapter.AdapterBerita;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailBeritaActivity extends AppCompatActivity {
    ImageButton btnBack;
    RecyclerView listBerita;
    LoginViewModel loginViewModel;
    String Token;
    Integer id_users,Start=0,Limit=2;
    ApiRequest apiinterface;
    AdapterBerita adapterBerita;
    Berita berita;
    TextView judulBerita,tanggalBerita,isiBerita;
    ImageView gambarBertita;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        btnBack=findViewById(R.id.BtnBack);
        judulBerita=findViewById(R.id.JudulBerita);
        tanggalBerita=findViewById(R.id.TanggalBerita);
        isiBerita=findViewById(R.id.IsiBerita);
        gambarBertita=findViewById(R.id.GambarBerita);
        listBerita =findViewById(R.id.ListBerita);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        listBerita.setLayoutManager(layoutManager);
        listBerita.setHasFixedSize(true);
        listBerita.setNestedScrollingEnabled(false);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                berita= null;
            } else {
                berita= (Berita) extras.getSerializable("DETAILBERITA");
            }
        }
        else {
            berita= (Berita) savedInstanceState.getSerializable("DETAILBERITA");
        }
        if (berita!=null){
            judulBerita.setText(berita.getJudulBerita());
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = simpleDateFormat.parse(berita.getCreatedAt());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat convetDateFormat = new SimpleDateFormat("MMMM d,yyyy");
            assert date != null;
            String Htanggal=convetDateFormat.format(date);
            tanggalBerita.setText(Htanggal);
            if (!berita.getGambar().equals("")){
                Picasso.get().load(getString(R.string.path)  +"/uploads/products/"+ berita.getGambar()).into(gambarBertita);
            }
            isiBerita.setText(berita.getDeskripsi());
        }
        btnBack.setOnClickListener(view -> onBackPressed());
        Token();
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiBerita();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void ApiBerita(){
        Call<ApiResponse> call = apiinterface.BeritaList(Token, Start,Limit);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getBerita().size()>0){
                        adapterBerita = new AdapterBerita(getApplicationContext(), response.body().getBerita(), posisi -> {
                            Intent intent = new Intent(getApplicationContext(), DetailDonasiActivity.class);
                            startActivity(intent);
                        });
                        listBerita.setAdapter(adapterBerita);
                        adapterBerita.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}