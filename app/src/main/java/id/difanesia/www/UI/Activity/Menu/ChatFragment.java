package id.difanesia.www.UI.Activity.Menu;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterTabChat;


public class ChatFragment extends Fragment {
    View view;
    AdapterTabChat adapter;
    ViewPager2 viewPager2;
    TabLayout tabLayout;


    public ChatFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       view=inflater.inflate(R.layout.fragment_chat, container, false);
        viewPager2=view.findViewById(R.id.pager);
        tabLayout=view.findViewById(R.id.LayoutTabs);
        adapter=new AdapterTabChat(getActivity());
        viewPager2.setAdapter(adapter);
        tabLayout.addTab(tabLayout.newTab().setText("Semua Chat"));
        tabLayout.addTab(tabLayout.newTab().setText("Belum Dibaca"));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });
       return view;
    }
}