package id.difanesia.www.UI.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.List;

import id.difanesia.www.Model.Riwayat;
import id.difanesia.www.UI.Activity.Riwayat.RiwayatFragment;

public class AdapterTabRiwayat extends FragmentStateAdapter {
        List<Riwayat> dibayar;
        List<Riwayat> dikemas;
        List<Riwayat> dikirim;
        List<Riwayat> selesai;
        List<Riwayat> dibatalkan;
        List<Riwayat> pengembalian;
    public AdapterTabRiwayat(@NonNull FragmentActivity fragmentActivity){
        super(fragmentActivity);

    }
    public void addData(List<Riwayat> dibayar,
                        List<Riwayat> dikemas,
                        List<Riwayat> dikirim,
                        List<Riwayat> selesai,
                        List<Riwayat> dibatalkan,
                        List<Riwayat> pengembalian){
        this.dibayar=dibayar;
        this.dikemas=dikemas;
        this.dikirim=dikirim;
        this.selesai=selesai;
        this.dibatalkan=dibatalkan;
        this.pengembalian=pengembalian;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position==1){
            return new RiwayatFragment(dikemas);
        }
        else if (position==2){
            return new RiwayatFragment(dikirim);
        }
        else if (position==3){
            return new RiwayatFragment(selesai);
        }
        else if (position==4){
            return new RiwayatFragment(dibatalkan);
        }
        else if (position==5){
            return new RiwayatFragment(pengembalian);
        }
        return new RiwayatFragment(dibayar);
    }

    @Override
    public int getItemCount() {
        return 6;
    }
}
