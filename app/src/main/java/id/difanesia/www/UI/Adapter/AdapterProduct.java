package id.difanesia.www.UI.Adapter;






import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;

import com.squareup.picasso.Picasso;


import java.util.List;

import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;

public class AdapterProduct extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context context;
    List<Produk> itemds;
    final int VIEW_TYPE_LOADING=0;
    final int VIEW_TYPE_ITEM=1;
    OnBtnClick monBtnClick;
    FormatCurrency formatCurrency = new FormatCurrency();

    Integer id_user;
    public AdapterProduct(Context context, List<Produk> itemds,Integer id_user,OnBtnClick onBtnClick){
        super();
        this.monBtnClick=onBtnClick;
        this.context = context;
        this.itemds=itemds;
        this.id_user=id_user;


    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product, parent, false);
            return new ItemViewHolder(view, monBtnClick);
        } else
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_product, parent, false);
            return new LoadingHolder(view);
        }
    }


    @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if(holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            final float scale = context.getResources().getDisplayMetrics().density;
            ViewGroup.LayoutParams params = ((ItemViewHolder) holder).cardsize.getLayoutParams();
            float density = (float) context.getResources().getDisplayMetrics().density;
            if (density>=1.0 && density<1.5) {
                params.width = (int) (110 * scale + 0.5f);
                ((ItemViewHolder) holder).cardsize.setLayoutParams(params);
            }
            else if (density>=1.5 && density<2) {

                params.width = (int) (130 * scale + 0.5f);
                ((ItemViewHolder) holder).cardsize.setLayoutParams(params);
            }
            else if (density>=2.0 && density<3) {
                params.width = (int) (150 * scale + 0.5f);
                ((ItemViewHolder) holder).cardsize.setLayoutParams(params);
            }
            else if (density>=3.0 && density<4) {
                params.width = (int) (170 * scale + 0.5f);
                ((ItemViewHolder) holder).cardsize.setLayoutParams(params);
            }
            else if (density>=4.0) {
                params.width = (int) (190 * scale + 0.5f);
                ((ItemViewHolder) holder).cardsize.setLayoutParams(params);
            }
            if (itemds.get(position).getImage()!=null) {
                Picasso.get().load(context.getString(R.string.path) +itemds.get(position).getImage()).into(((ItemViewHolder) holder).gambarProduct);

            }
            if (itemds.get(position).getIdWish()!=null){
                ((ItemViewHolder) holder).btnWish.setVisibility(View.VISIBLE);
                ((ItemViewHolder) holder).btnWish.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_baseline_favorite_24));
            }
            ((ItemViewHolder) holder).judulProduct.setText(itemds.get(position).getNama());
            ((ItemViewHolder) holder).hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(itemds.get(position).getHarga())));
            if (itemds.get(position).getRating()!=null) {
                ((ItemViewHolder) holder).ratingProduct.setText(itemds.get(position).getRating().toString().replaceAll("\\.?0*$", ""));
            }
            if (itemds.get(position).getTerjual()!=null){
                float terjual = Float.parseFloat(itemds.get(position).getTerjual().toString());
                String ribuan ="";
                if (terjual>=1000){
                    terjual=terjual/1000;
                    ribuan=" rb+";
                }
                ((ItemViewHolder) holder).terjualProduct.setText(Float.toString(terjual).replaceAll(context.getString(R.string.TitikBelakang), "")+ribuan);
            }
            ((ItemViewHolder) holder).kotaProduct.setText(itemds.get(position).getKota());
            itemViewHolder.detailProduct.setOnClickListener(v -> monBtnClick.OnClickProduct(itemViewHolder.getAdapterPosition(),itemds.get(position)));
            ((ItemViewHolder) holder).btnWish.setOnClickListener(view -> monBtnClick.OnClickWish(holder.getAdapterPosition(),itemds.get(position)));

        }
        else if (holder instanceof LoadingHolder){
            final LoadingHolder loadingHolder= (LoadingHolder) holder;
            loadingHolder.loadingProduct.startShimmer();
        }
    }

    @Override
    public int getItemCount() {
        return itemds==null ? 0:itemds.size();
    }

    private static class LoadingHolder extends RecyclerView.ViewHolder{
        ShimmerFrameLayout loadingProduct;
        public LoadingHolder(@NonNull View itemView) {
            super(itemView);
            loadingProduct = itemView.findViewById(R.id.LoadingProduct);

        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder implements OnBtnClick{

        LinearLayout detailProduct,cardsize;
        ImageView gambarProduct;
        TextView judulProduct,hargaProduct,kotaProduct,ratingProduct,terjualProduct;
        ImageButton btnWish;
        CheckBox btnChecklist;
        public ItemViewHolder(@NonNull  View itemView,OnBtnClick onBtnClick) {
            super(itemView);
            detailProduct=itemView.findViewById(R.id.DetailProduct);
            gambarProduct=itemView.findViewById(R.id.GambarProduct);
            judulProduct=itemView.findViewById(R.id.JudulProduct);
            hargaProduct=itemView.findViewById(R.id.HargaProduct);
            kotaProduct=itemView.findViewById(R.id.KotaProduct);
            ratingProduct=itemView.findViewById(R.id.RatingProduct);
            terjualProduct=itemView.findViewById(R.id.TerjualProduct);
            btnChecklist=itemView.findViewById(R.id.BtnChecklist);
            btnWish=itemView.findViewById(R.id.BtnWishList);
            cardsize=itemView.findViewById(R.id.CardSize);


        }


        @Override
        public void OnClickProduct(int posisi, Produk produk) {

        }

        @Override
        public void OnClickWish(Integer posisi, Produk produk) {

        }


    }



    @Override
    public int getItemViewType(int position) {
        return itemds.get(position)==null ? VIEW_TYPE_LOADING:VIEW_TYPE_ITEM ;
    }

    public void clear() {
        int size = itemds.size();
        this.itemds.clear();
        notifyItemRangeRemoved(0, size);

    }
    @SuppressLint("NotifyDataSetChanged")
    public void add(List<Produk> models) {
        if (models!=null) {
            this.itemds.remove(getItemCount()-1);
            notifyItemChanged(getItemCount()-1);
            this.itemds.addAll(models);
            notifyDataSetChanged();
        }
        else{
            if (this.itemds.get(getItemCount()-1)!=null) {
                this.itemds.add(null);
                notifyDataSetChanged();
            }

        }
    }
    public void endScroll(){
        if (this.itemds.get(getItemCount()-1)==null) {
                this.itemds.remove(getItemCount() - 1);
            notifyDataSetChanged();
        }
    }
    public  interface OnBtnClick{

        void OnClickProduct(int posisi,Produk produk);
        void OnClickWish(Integer posisi,Produk produk);

    }

    public void updateDataFavorit(Integer posisis, Boolean value,int id_wish) {
        int newPosition = posisis;
        if (value) {
            this.itemds.get(newPosition).setIdWish(id_wish);
            notifyItemChanged(newPosition);
        }else{
            this.itemds.get(newPosition).setIdWish(null);
            notifyItemChanged(newPosition);
        }

    }



}


