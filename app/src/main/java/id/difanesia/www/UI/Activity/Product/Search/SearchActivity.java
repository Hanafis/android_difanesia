package id.difanesia.www.UI.Activity.Product.Search;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.SpacesItemDecoration;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.Model.Search;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Product.DetailProductActivity;
import id.difanesia.www.UI.Adapter.AdapterFilterKota;
import id.difanesia.www.UI.Adapter.AdapterProduct;
import id.difanesia.www.UI.Adapter.AdapterSearch;
import id.difanesia.www.ViewModel.LoginViewModel;
import id.difanesia.www.ViewModel.SearchViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    Button btnHapusRiwayat;
    ImageButton btnBack;
    TextInputEditText searchEditText;
    SearchViewModel searchViewModel;
    RecyclerView searchList;
    AdapterSearch adapterSearch;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        btnBack=findViewById(R.id.BtnBack);
        btnHapusRiwayat=findViewById(R.id.BtnHapusPencarian);
        searchEditText= findViewById(R.id.SearchEditText);
        searchEditText.requestFocus();
        searchList =findViewById(R.id.SearchList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        searchList.setLayoutManager(layoutManager);
        searchList.setHasFixedSize(true);
        Aksi();
        
    }
    @SuppressLint("NotifyDataSetChanged")
    private void Aksi(){
        btnBack.setOnClickListener(view -> {
            onBackPressed();
            finish();
        });
        btnHapusRiwayat.setOnClickListener(view -> {
            searchViewModel.DeleteALL();
            adapterSearch.clear();
            btnHapusRiwayat.setVisibility(View.GONE);
        });
        searchViewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        searchViewModel.getApiResponseLiveData().observe(this, searches -> {
            if (searches!=null) {
                if (searches.size() > 0) {
                    adapterSearch = new AdapterSearch(getApplicationContext(), searches, posisi -> {
                        InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(searchEditText.getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                        Intent intent = new Intent(SearchActivity.this, SearchListActivity.class);
                        intent.putExtra("KEYWORDSEARCH",searches.get(posisi).getKeyword());
                        startActivity(intent);
                    });
                    adapterSearch.notifyDataSetChanged();
                    searchList.setAdapter(adapterSearch);



                }
                else {
                    btnHapusRiwayat.setVisibility(View.GONE);
                }
            }

        });
        searchEditText.setOnKeyListener((view, i, keyEvent) -> {
            if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                Search search = new Search(Objects.requireNonNull(searchEditText.getText()).toString().trim());
                searchViewModel.insertData(search);
                Intent intent = new Intent(SearchActivity.this, SearchListActivity.class);
                intent.putExtra("KEYWORDSEARCH", Objects.requireNonNull(searchEditText.getText()).toString().trim());
                startActivity(intent);
                return true;
            }

            return false;

        });
    }



}