package id.difanesia.www.UI.Activity.ResetPassword;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;


import java.util.Objects;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CodeVerificationActivity extends AppCompatActivity {
    ApiRequest apiinterface;
    TextView noOrEmail,kirimUlang,timer;
    PinView code;
    Button btnLanjutkan;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_verification);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        noOrEmail=findViewById(R.id.NoOrEmail);
        kirimUlang=findViewById(R.id.BtnUlang);
        timer=findViewById(R.id.TimerUlang);
        code=findViewById(R.id.PinView);
        btnLanjutkan=findViewById(R.id.BtnLanjutkan);
        kirimUlang.setTextColor(this.getResources().getColor(R.color.GreyDot));
        kirimUlang.setOnClickListener(null);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                email= null;
            } else {
                email= (String) extras.getString("EMAIL");
            }
        }
        else {
            email= (String) savedInstanceState.getSerializable("EMAIL");
        }
        Timer();
        btnLanjutkan.setOnClickListener(view -> ApiCode());
    }
    public void Timer(){
        new CountDownTimer(60000, 1000) {

            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
                timer.setText("00:" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                kirimUlang.setTextColor(getResources().getColor(R.color.Aplikasi));
                kirimUlang.setOnClickListener(view -> ApiReset());
            }
        }.start();
    }
    public void ApiReset(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Kirim Ulang Kode...");
        progressDialog.show();
        Call<ApiResponse> call = apiinterface.reset(email);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()){
                        progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),"Berhasil Mengirim Ulang Kode Verifikasi",Toast.LENGTH_LONG).show();
                            Timer();
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void ApiCode(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Konfirmasi Kode...");
        progressDialog.show();
        Call<ApiResponse> call = apiinterface.kode(email,Integer.parseInt(Objects.requireNonNull(code.getText()).toString().trim()));
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()){
                        progressDialog.dismiss();
                        Intent intent = new Intent(CodeVerificationActivity.this, ChangePasswordActivity.class);
                        intent.putExtra("KODE",code.getText().toString());
                        intent.putExtra("EMAIL",email);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
            }
        });
    }

}