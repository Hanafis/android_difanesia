package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Donatur;
import id.difanesia.www.R;

public class AdapterDonatur extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Donatur> donaturs;
    Context context;
    FormatCurrency formatCurrency=new FormatCurrency();

    public AdapterDonatur(Context context, List<Donatur> donaturs) {
        this.context=context;
        this.donaturs=donaturs;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_donatur,parent,false);
        return new DonaturViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DonaturViewHolder){
            final DonaturViewHolder donaturViewHolder = (DonaturViewHolder) holder;
            Picasso.get().load(context.getResources().getString(R.string.path)+"/uploads/products/"+donaturs.get(position).getProfiluser()).into(donaturViewHolder.profil);
            donaturViewHolder.nama.setText(donaturs.get(position).getName());
            if (donaturs.get(position).getIdDonatur()!=null){
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                try {
                    date = simpleDateFormat.parse(donaturs.get(position).getCreatedAt());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat convetDateFormat = new SimpleDateFormat("MMMM d,yyyy");
                assert date != null;
                String Htanggal=convetDateFormat.format(date);
                donaturViewHolder.verifikasi.setText(Htanggal);
                donaturViewHolder.uang.setText(formatCurrency.Rupiah(Double.parseDouble(donaturs.get(position).getDonasi())));
                donaturViewHolder.uang.setVisibility(View.VISIBLE);
            }

        }

    }

    @Override
    public int getItemCount() {
        return donaturs==null?0:donaturs.size();
    }

    private class DonaturViewHolder extends RecyclerView.ViewHolder {
        CircleImageView profil;
        TextView nama,uang,verifikasi;
        public DonaturViewHolder(View view) {
            super(view);
            profil=view.findViewById(R.id.GambarDonatur);
            nama=view.findViewById(R.id.NamaDonatur);
            verifikasi=view.findViewById(R.id.Verifikasi);
            uang=view.findViewById(R.id.UangDonasi);

        }
    }
}
