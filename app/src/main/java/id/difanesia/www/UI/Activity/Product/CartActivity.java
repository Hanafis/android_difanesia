package id.difanesia.www.UI.Activity.Product;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Voucher.VoucherActivity;
import id.difanesia.www.UI.Adapter.AdapterCart;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {
    ApiRequest apiinterface;
    TextView TotalBelanja;
    Button btnCheckout;
    ImageButton btnBack;
    LoginViewModel loginViewModel;
    LinearLayout layoutNotNotifikasi;
    String Token;
    Integer id_users;
    Float TotalHarga= (float) 0;
    Float harga= (float) 0;
    Float jml= (float) 0;
    Float Barang= (float) 0;
    List<Produk> Keranjang;
    List<AdapterCart> listAdapterChart;
//    List<Produk> list;
    LinearLayout btnPromo,layoutCart;
    FormatCurrency formatCurrency = new FormatCurrency();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        TotalBelanja=findViewById(R.id.TotalTextView);
        btnCheckout=findViewById(R.id.BtnCheckout);
        btnBack=findViewById(R.id.BtnBack);
        layoutCart=findViewById(R.id.LayoutCart);
        Keranjang = new ArrayList<>();
        btnPromo=findViewById(R.id.BtnPromo);
        layoutNotNotifikasi=findViewById(R.id.LayoutNot);
        btnPromo.setOnClickListener(view -> {
            Intent in = new Intent(CartActivity.this, VoucherActivity.class);
            startActivity(in);
        });

        btnBack.setOnClickListener(view -> onBackPressed());
        btnCheckout.setOnClickListener(view -> {

            Intent intent = new Intent(CartActivity.this,CheckoutActivity.class);
            intent.putExtra("PRODUCT",(Serializable) Keranjang);
            intent.putExtra("JMLBARANG",Barang);
            intent.putExtra("JMLHARGA",TotalHarga);
            startActivity(intent);
        });
        Token();
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiCart();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void ApiCart(){

        Call<ApiResponse> call = apiinterface.CartList(Token,id_users);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){

                    if (response.body().getProduk().size()>0){
                        //memisahkan json
                        layoutNotNotifikasi.setVisibility(View.GONE);
                        List<Produk> keranjang = response.body().getProduk();
                        List<Produk> item = new ArrayList<>();
                        listAdapterChart = new ArrayList<>();
                        int posisi=0;
                            for (int i = 0; i <= keranjang.size(); i++) {
                                if (i==keranjang.size()){
                                    CartList(item, posisi);
                                    item = new ArrayList<>();
                                    Log.e("Posisi",posisi+"");
                                }
                                else if (i==0){
                                    item.add(keranjang.get(i));
                                }else {
                                    Log.e("index",i+"");
                                    if (Objects.equals(keranjang.get(i).getIdToko(), keranjang.get(i - 1).getIdToko())) {
                                        item.add(keranjang.get(i));
                                        
                                    } else {
                                        CartList(item,posisi);
                                        item = new ArrayList<>();
                                        item.add(keranjang.get(i));
                                        Log.e("Posisi",posisi+"");
                                        posisi++;
                                    }
                                }

                            }



                    }else{
                        layoutNotNotifikasi.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public Integer convertdp(Integer number){
        float data=number * getResources().getDisplayMetrics().density;
        String hasil =String.format("%.0f", data);
        Log.e("HASIL",hasil);
        return Integer.valueOf(hasil);
    }
    @SuppressLint("NotifyDataSetChanged")
    private void CartList(List<Produk> keranjang,int posisiadapter) {
        Log.e("Item",""+keranjang.size());
        RecyclerView cartList = new RecyclerView(this);
        LinearLayout.LayoutParams linLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        cartList.setPadding(10,0,10,5);
        linLayoutParams.setMargins(convertdp(10),convertdp(10),convertdp(10),convertdp(5));
        cartList.setLayoutParams(linLayoutParams);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        cartList.setLayoutManager(layoutManager);
        cartList.setHasFixedSize(true);
        cartList.setNestedScrollingEnabled(false);
        cartList.setBackground(getResources().getDrawable(R.drawable.rounded_product));
        AdapterCart adapterCart = new AdapterCart(getApplicationContext(), keranjang, new AdapterCart.OnBtnClick() {
            @Override
            public void OnClickProduct(Produk produk) {
                Intent intent = new Intent(getApplicationContext(), DetailProductActivity.class);
                intent.putExtra("DETAILPRODUK", produk);
                startActivity(intent);
            }

            @Override
            public void OnClickCheckAll(Produk produk) {

            }

            @Override
            public void OnClickCheckProduk(int posisi,Produk produk) {

                if (produk.getIdVarian()!=null) {
                    harga=Float.valueOf(produk.getVarianHarga());
                }
                else{
                    harga=Float.valueOf(produk.getHarga());
                }
                jml=Float.valueOf(produk.getJml());

                if (produk.getCheck()) {
                    if (TotalHarga> 0) {
                        TotalHarga = TotalHarga - (harga * jml);
                        Barang=Barang-jml;
                        TotalBelanja.setText(formatCurrency.Rupiah(Double.parseDouble(TotalHarga.toString())));
                    }
                    listAdapterChart.get(posisiadapter).UpdateData(posisi,false);
                    
                    Keranjang.remove(produk);
                }else{
                    TotalHarga=TotalHarga+(harga*jml);
                    Barang=Barang+jml;
                    TotalBelanja.setText(formatCurrency.Rupiah(Double.parseDouble(TotalHarga.toString())));
                    listAdapterChart.get(posisiadapter).UpdateData(posisi,true);
                    Log.e("Check ",posisiadapter+"");
                    Integer ind = null;
                    for (int i =0; i<Keranjang.size(); i++){
                        if (Keranjang.get(i).getIdToko().equals(produk.getIdToko())){
                            ind=i;
                        }
                    }
                    if (ind!=null){
                        Keranjang.add(ind,produk);
                    }else{
                        Keranjang.add(produk);
                    }




                }
            }

            @Override
            public void OnClickWish(int posisi,Produk produk) {

                ApiWish(produk.getIdProduct(),posisi, listAdapterChart.get(posisiadapter));
            }

            @Override
            public void OnClickHapus(int posisi,Produk produk) {
                ApiHapus(posisi,produk, listAdapterChart.get(posisiadapter));
            }

            @Override
            public void OnClickTambah(int posisi,Produk produk) {
                Log.e("CHECKED", String.valueOf(produk.getCheck()));
                if (produk.getCheck()) {
                    if (produk.getIdVarian() != null) {
                        harga = Float.valueOf(produk.getVarianHarga());
                    } else {
                        harga = Float.valueOf(produk.getHarga());
                    }
                    TotalHarga = TotalHarga + harga;
                    Barang=Barang+1;
                    TotalBelanja.setText(formatCurrency.Rupiah(Double.parseDouble(TotalHarga.toString())));
                    int index =Keranjang.indexOf(produk);
                    Keranjang.get(index).setJml(Keranjang.get(index).getJml());
                }
                ApiTambah(posisi,produk, listAdapterChart.get(posisiadapter));

            }

            @Override
            public void OnClickKurang(int posisi,Produk produk) {
                if(produk.getJml()>1) {
                    if (produk.getIdVarian()!=null) {
                        harga=Float.valueOf(produk.getVarianHarga());
                    }
                    else{
                        harga=Float.valueOf(produk.getHarga());
                    }
                    if (produk.getCheck()) {
                        if (TotalHarga > 0) {
                            TotalHarga = TotalHarga - harga;
                            Barang=Barang-1;
                            TotalBelanja.setText(formatCurrency.Rupiah(Double.parseDouble(TotalHarga.toString())));
                            int index =Keranjang.indexOf(produk);
                            Keranjang.get(index).setJml(Keranjang.get(index).getJml());
                        }
                    }
                    ApiKurang(posisi,produk, listAdapterChart.get(posisiadapter));

                }else{
                    ApiHapus(posisi,produk, listAdapterChart.get(posisiadapter));
                }
            }
        });
        Log.e("Posisi adap",posisiadapter+"");
        listAdapterChart.add(adapterCart);
        cartList.setAdapter(adapterCart);
        adapterCart.notifyDataSetChanged();
        Log.e("IDKU", String.valueOf(adapterCart.getItemCount()));
        layoutCart.addView(cartList);
    }

    public void ApiWish(int id_product,int posisi,AdapterCart adapterCart){
            Call<ApiResponse> call = apiinterface.WishAdd(Token, id_users, id_product);
            call.enqueue(new Callback<ApiResponse>() {
                @Override
                public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess() != null) {
                            adapterCart.updateDataFavorit(posisi, response.body().getSuccess());
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                }
            });

    }
    public void ApiHapus(int posisi,Produk produk,AdapterCart adapterCart){
        Call<ApiResponse> call = apiinterface.CartDelete(Token,produk.getIdKeranjang());
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getMessage()!=null){
                        if (produk.getIdVarian() != null) {
                            harga = Float.valueOf(produk.getVarianHarga());
                        } else {
                            harga = Float.valueOf(produk.getHarga());
                        }
                        jml=Float.valueOf(produk.getJml());
                        if (TotalHarga> 0) {
                            TotalHarga=TotalHarga-(harga*jml);
                            Barang=Barang-jml;
                            TotalBelanja.setText(formatCurrency.Rupiah(Double.parseDouble(TotalHarga.toString())));
                        }

                        Keranjang.remove(produk);
                        adapterCart.RemoveDataCart(posisi);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });

    }
    public void ApiTambah(int posisi,Produk produk,AdapterCart adapterCart){
        Call<ApiResponse> call = apiinterface.CartAdd(Token,id_users,produk.getIdProduct(),1);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getMessage()!=null){
                        adapterCart.ActionCart(posisi,true);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });

    }
    public void ApiKurang(int posisi,Produk produk,AdapterCart adapterCart){
        Call<ApiResponse> call = apiinterface.CartMinus(Token,produk.getIdKeranjang());
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getMessage()!=null){
                        adapterCart.ActionCart(posisi,false);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }

}