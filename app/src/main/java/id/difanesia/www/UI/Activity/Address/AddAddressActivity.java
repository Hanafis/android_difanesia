package id.difanesia.www.UI.Activity.Address;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.Alamat;
import id.difanesia.www.R;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends AppCompatActivity {
    ImageButton btnBack;
    EditText namaLengkap,noTlpn,jalan,lainnya;
    Button btnRumah,btnKantor,btnSimpan,btnHapus;
    TextView provinsi,kabupaten,kecamatan,kelurahan,kodePos;
    LinearLayout layoutAlamat;
    Alamat alamat=null;
    ApiRequest apiinterface;
    LoginViewModel loginViewModel;
    String Token,Gedung,Utama="tidak";
    Integer id_users;
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    Switch utama;
    Integer id_provinsi,id_kabupaten,id_kecamatan,id_kelurahan;
    int LAUNCH_SECOND_ACTIVITY = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        btnBack=findViewById(R.id.BtnBack);
        namaLengkap=findViewById(R.id.NamaLengkap);
        noTlpn=findViewById(R.id.NoTlpn);
        jalan=findViewById(R.id.Jalan);
        btnRumah=findViewById(R.id.Rumah);
        btnKantor=findViewById(R.id.Kantor);
        btnSimpan=findViewById(R.id.BtnSimpan);
        btnHapus=findViewById(R.id.BtnHapus);
        provinsi=findViewById(R.id.Provinsi);
        kabupaten=findViewById(R.id.Kabupaten);
        kecamatan=findViewById(R.id.Kecamatan);
        kodePos=findViewById(R.id.KodePos);
        layoutAlamat=findViewById(R.id.LayoutAlamat);
        utama=findViewById(R.id.Utama);
        lainnya=findViewById(R.id.Lainnya);
        kelurahan=findViewById(R.id.Kelurahan);
        Token();
        Aksi();
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    @SuppressLint("UseCompatLoadingForDrawables")
    public void Aksi(){
        btnBack.setOnClickListener(view -> onBackPressed());
        layoutAlamat.setOnClickListener(view -> {
            Intent intent = new Intent(AddAddressActivity.this, AddressListActivity.class);
            if (alamat!=null){
                intent.putExtra("ALAMATS",alamat);
            }
            startActivityForResult(intent,1);

        });
        btnSimpan.setOnClickListener(view -> {
            if (namaLengkap.getText().length()>0 &&  noTlpn.getText().length()>0) {
                if (jalan.getText().length() > 0) {
                    if (Gedung != null) {
                        ApiAlamt();
                    } else {
                        Toast.makeText(this, "Pilih Bagunan Sebagai Gedung atau Rumah", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Lengkapi Nama jalan Atau desa", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(this, "Silahkan Lengkapi Data Terlebih Dahulu", Toast.LENGTH_SHORT).show();
            }
        });
        btnRumah.setOnClickListener(view -> {
            btnRumah.setBackground(getResources().getDrawable(R.drawable.button_rounded_hijau));
            btnRumah.setTextColor(getResources().getColor(R.color.white));
            btnKantor.setBackground(getResources().getDrawable(R.drawable.edit_text));
            btnKantor.setTextColor(getResources().getColor(R.color.black));
            Gedung="rumah";
        });
        btnKantor.setOnClickListener(view -> {
            btnKantor.setBackground(getResources().getDrawable(R.drawable.button_rounded_hijau));
            btnKantor.setTextColor(getResources().getColor(R.color.white));
            btnRumah.setBackground(getResources().getDrawable(R.drawable.edit_text));
            btnRumah.setTextColor(getResources().getColor(R.color.black));
            Gedung="kantor";
        });

    }
    public void ApiAlamt(){
        if (utama.isChecked()){
            Utama="ya";
        }
        else{
            Utama="tidak";
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tambah Alamat...");
        progressDialog.show();
        Call<ApiResponse> call = apiinterface.ALamats(
                Token,
                id_users,
                namaLengkap.getText().toString().trim(),
                noTlpn.getText().toString().trim(),
                provinsi.getText().toString().trim(),
                kabupaten.getText().toString().trim(),
                kecamatan.getText().toString().trim(),
                kelurahan.getText().toString().trim(),
                kodePos.getText().toString().trim(),
                jalan.getText().toString().trim(),
                lainnya.getText().toString().trim(),
                Gedung,
                Utama
                );
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getMessage()!=null){
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                        Intent intent =new Intent(AddAddressActivity.this,AddressActivity.class);
                        startActivity(intent);
                        finish();

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    Alamat alamats = (Alamat) bundle.getSerializable("ALAMATS");
                    if (alamats != null) {
                        id_provinsi=alamats.getIdProvinsi();
                        id_kabupaten=alamats.getIdKabupaten();
                        id_kecamatan=alamats.getIdKecamatan();
                        id_kelurahan=alamats.getIdKelurahan();
                        provinsi.setText(alamats.getProvinsi());
                        kabupaten.setText(alamats.getKabupaten());
                        kecamatan.setText(alamats.getKecamatan());
                        kelurahan.setText(alamats.getKelurahan());
                        kodePos.setText(alamats.getKodepos());

                    }
                }
            }

        }
    }
}