package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.Model.UserRating;
import id.difanesia.www.R;

public class AdapterRating extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<UserRating> userRatings;
    public AdapterRating(Context context,List<UserRating> userRatings){
        this.context=context;
        this.userRatings=userRatings;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rating, parent, false);
        return new RatingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof AdapterRating.RatingViewHolder) {

            final AdapterRating.RatingViewHolder ratingViewHolder = (RatingViewHolder) holder;
            ratingViewHolder.ratingBarUser.setRating(Integer.parseInt(userRatings.get(position).getRating().toString().replaceAll("\\.?0*$", "")));
            if (userRatings.get(position).getImage()!=null) {
                ratingViewHolder.gambarRating.setVisibility(View.VISIBLE);
                Picasso.get().load(context.getString(R.string.path) + "/" + userRatings.get(position).getImage()).into(ratingViewHolder.gambarRating);
            }
            Picasso.get().load(context.getString(R.string.path)+"/"+userRatings.get(position).getImguser()).into(ratingViewHolder.gambarUser);
            ratingViewHolder.namaUser.setText(userRatings.get(position).getName());
            ratingViewHolder.deskripsiUser.setText(userRatings.get(position).getDeskripsi());



        }
    }

    @Override
    public int getItemCount()  {
        return userRatings == null ? 0 : userRatings.size();
    }

    private static class RatingViewHolder extends RecyclerView.ViewHolder {
        RatingBar ratingBarUser;
        TextView ratingUser,namaUser,deskripsiUser;
        ImageView gambarRating;
        CircleImageView gambarUser;

        public RatingViewHolder(View view) {
            super(view);
            ratingBarUser=view.findViewById(R.id.RatingBarUser);
            ratingUser=view.findViewById(R.id.RatingUser);
            namaUser=view.findViewById(R.id.NamaRatingUser);
            deskripsiUser=view.findViewById(R.id.deskripsiRatingUser);
            gambarUser=view.findViewById(R.id.GambarUser);
            gambarRating=view.findViewById(R.id.GambarRating);

        }
    }
    @SuppressLint("NotifyDataSetChanged")
    public void add(List<UserRating> models) {
        this.userRatings.addAll(models);
        notifyDataSetChanged();
    }
}
