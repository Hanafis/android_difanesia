package id.difanesia.www.UI.Activity.ResetPassword;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Locale;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {
    ImageButton btnBack;
    Button btnLanjutkan;
    EditText email;
    ApiRequest apiinterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        setContentView(R.layout.activity_lupa_password);
        btnBack=findViewById(R.id.BtnBack);
        btnLanjutkan=findViewById(R.id.BtnLanjutkan);
        email=findViewById(R.id.Email);
        btnBack.setOnClickListener(view -> onBackPressed());
        btnLanjutkan.setOnClickListener(view -> ApiReset());
    }
    public void ApiReset(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Kirim Kode...");
        progressDialog.show();
        Call<ApiResponse> call = apiinterface.reset(email.getText().toString().trim().toLowerCase(Locale.ROOT));
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()){
                        progressDialog.dismiss();
                        Intent intent = new Intent(ForgetPasswordActivity.this, CodeVerificationActivity.class);
                        intent.putExtra("EMAIL",email.getText().toString().toLowerCase(Locale.ROOT));
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}