package id.difanesia.www.UI.Activity.Berita;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterBerita;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListBerita extends Fragment {

    View view;
    ApiRequest apiinterface;
    AdapterBerita adapterBerita;
    RecyclerView beritaList;
    LoginViewModel loginViewModel;
    ScrollView scrollView;
    ShimmerFrameLayout loading;
    String Token;
    Integer id_users,Limit=12,Start=0;
    LinearLayout layoutNot;
    public ListBerita() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_tab_berita, container, false);
        apiinterface = ApiConfig.getClient(getContext()).create(ApiRequest.class);
        beritaList =view.findViewById(R.id.ListBerita);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        beritaList.setLayoutManager(layoutManager);
        beritaList.setHasFixedSize(true);
        beritaList.setNestedScrollingEnabled(false);
        scrollView=view.findViewById(R.id.ScrollBerita);
        loading=view.findViewById(R.id.LoadingBerita);
        layoutNot=view.findViewById(R.id.LayoutNot);
        loading.startShimmer();
        Token();
        return view;
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(getViewLifecycleOwner(), login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiBerita();

                }
                else {
                    Toast.makeText(getContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void ApiBerita(){
        Call<ApiResponse> call = apiinterface.BeritaList(Token, Start,Limit);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getBerita().size()>0){
                        adapterBerita = new AdapterBerita(getContext(), response.body().getBerita(), posisi -> {
                            Intent intent = new Intent(getActivity(), DetailBeritaActivity.class);
                            intent.putExtra("DETAILBERITA",response.body().getBerita().get(posisi));
                            startActivity(intent);
                        });
                        beritaList.setAdapter(adapterBerita);
                        adapterBerita.notifyDataSetChanged();
                        scrollView.setVisibility(View.GONE);
                        beritaList.setVisibility(View.VISIBLE);
                        loading.stopShimmer();
                    }else{
                        scrollView.setVisibility(View.GONE);
                        layoutNot.setVisibility(View.VISIBLE);
                        loading.stopShimmer();
                    }
                }else{
                    scrollView.setVisibility(View.GONE);
                   layoutNot.setVisibility(View.VISIBLE);
                    loading.stopShimmer();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}