package id.difanesia.www.UI.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.List;

import id.difanesia.www.Model.UserRating;
import id.difanesia.www.UI.Activity.Product.Rating.RatingFragment;


public class AdapterTabRating extends FragmentStateAdapter {
        List<UserRating> b1;
        List<UserRating> b2;
        List<UserRating> b3;
        List<UserRating> b4;
        List<UserRating> b5;
        Integer id_product;
    public AdapterTabRating(@NonNull FragmentActivity fragmentActivity,Integer id_product)
    {
        super(fragmentActivity);
        this.id_product=id_product;
    }
    public void addData(List<UserRating> b1,List<UserRating> b2,List<UserRating> b3,List<UserRating> b4,List<UserRating> b5){
        this.b1=b1;
        this.b2=b2;
        this.b3=b3;
        this.b4=b4;
        this.b5=b5;
    }
    @NonNull
    @Override
    public Fragment createFragment(int position) {

        switch (position) {
            case 1:
                return  new RatingFragment(4,b4,id_product);
            case 2:
                return  new RatingFragment(3,b3,id_product);
            case 3:
                return  new RatingFragment(2,b2,id_product);
            case 4:
                return  new RatingFragment(1,b1,id_product);
            default:
                return  new RatingFragment(5,b5,id_product);
        }
    }
    @Override
    public int getItemCount() {return 5; }
}
