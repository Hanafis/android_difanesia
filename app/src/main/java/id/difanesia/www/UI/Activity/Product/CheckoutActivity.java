package id.difanesia.www.UI.Activity.Product;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.DeviceInformation;
import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Alamat;
import id.difanesia.www.Model.Checkouts;
import id.difanesia.www.Model.ListCheckut;
import id.difanesia.www.Model.ListProducts;
import id.difanesia.www.Model.Payment;
import id.difanesia.www.Model.Products;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.Model.Voucher;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Address.AddressActivity;
import id.difanesia.www.UI.Activity.Payment.DetailPaymentActivity;
import id.difanesia.www.UI.Activity.Payment.PaymentActivity;
import id.difanesia.www.UI.Activity.Voucher.VoucherActivity;
import id.difanesia.www.UI.Adapter.AdapterCart;
import id.difanesia.www.UI.Adapter.AdapterCheckout;
import id.difanesia.www.ViewModel.LoginViewModel;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {
    ApiRequest apiinterface,apipayment;
    String Token;
    Integer id_users;
    LoginViewModel loginViewModel;
    ImageView gmbrbank;
    TextView gedung,namaUser,alamatUser,nomorUser,totalDonasi,totalBayar,totalBarang,totalHarga,totalOngkir,totalPajak,namabank,metode;
    EditText voucher;
    Button btnAlamat,btnCheckout;
    ImageButton btnBack;
    List<Produk> produks=null;
    Float TotalHarga= (float) 0;
    Float TotalBayar= (float) 0;
    Float Barang= (float) 0;
    Float totalPotongan=(float) 0;
    Float nominalDonasi= (float) 0;
    Double ongkir;
    String expedisi="POS Indonesia";
    FormatCurrency formatCurrency = new FormatCurrency();
    int LAUNCH_SECOND_ACTIVITY = 1;
    String Email;
    DeviceInformation deviceInformation;
    Alamat alamat;
    Payment payment;
    LinearLayout btnLayoutPembeyaran,layoutProduct;
    List<AdapterCart> listAdapterChart;
    List<EditText> listPesan=new ArrayList<>();
    List<EditText> listDonasi=new ArrayList<>();
    List<Integer> id_user=new ArrayList<>();
    List<ListProducts> listProducts =new ArrayList<>();


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        deviceInformation = new DeviceInformation();
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        gedung=findViewById(R.id.Gedung);
        namaUser=findViewById(R.id.NamaUser);
        alamatUser=findViewById(R.id.AlamatUser);
        nomorUser=findViewById(R.id.NomorUser);
        totalBayar=findViewById(R.id.TotalBayar);
        totalBarang=findViewById(R.id.TotalBarang);
        totalHarga=findViewById(R.id.TotalHarga);
        totalOngkir=findViewById(R.id.TotalOngkir);
        totalPajak=findViewById(R.id.TotalPajak);
        voucher=findViewById(R.id.BtnVoucher);
        btnAlamat=findViewById(R.id.BtnUbah);
        btnCheckout=findViewById(R.id.BtnCheckout);
        gmbrbank=findViewById(R.id.gmbrBank);
        namabank=findViewById(R.id.NamaBank);
        metode=findViewById(R.id.MetodePembayaran);
        btnBack=findViewById(R.id.BtnBack);
        totalDonasi=findViewById(R.id.Donasi);
        layoutProduct=findViewById(R.id.LayoutProduct);
        btnLayoutPembeyaran=findViewById(R.id.LayoutPayment);

        produks= new ArrayList<>();
        Setup(savedInstanceState);
        Token();
        Aksi();


    }

    @SuppressLint("NewApi")
    private void Setup(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                TotalHarga= null;
                produks=null;
                Barang =null;
            } else {
                TotalHarga= extras.getFloat("JMLHARGA");
                TotalBayar=TotalBayar+TotalHarga+5000+25000;
                totalBayar.setText(formatCurrency.Rupiah(Double.parseDouble(String.valueOf(TotalBayar))));
                produks= (List<Produk>) extras.getSerializable("PRODUCT");
                Barang= extras.getFloat("JMLBARANG");

            }
        }
        if (produks!=null) {
            if (produks.size() > 0) {
                Log.e("SIZE", String.valueOf(produks.size()));
                List<Produk> keranjang = produks;
                List<Produk> item = new ArrayList<>();
                listAdapterChart = new ArrayList<>();
                if (produks.size()>1) {
                    for (int i = 0; i <= keranjang.size(); i++) {
                        if (i == keranjang.size()) {
                            Productist(item);
                            item = new ArrayList<>();

                        } else if (i == 0) {
                            item.add(keranjang.get(i));

                        } else {
                            Log.e("index", i + "");
                            if (Objects.equals(keranjang.get(i).getIdToko(), keranjang.get(i - 1).getIdToko())) {
                                item.add(keranjang.get(i));

                            } else {
                                Productist(item);
                                item = new ArrayList<>();
                                item.add(keranjang.get(i));


                            }
                        }

                    }
                }else{
                    Productist(produks);
                }
                totalBarang.setText("(" + Barang.toString().replaceAll(getResources().getString(R.string.TitikBelakang), "") + ")");
                if (TotalHarga != null) {
                    totalHarga.setText(formatCurrency.Rupiah(Double.parseDouble(TotalHarga.toString())));
                }
            }
        }
    }
    @SuppressLint("UseCompatLoadingForDrawables")
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void Productist(List<Produk> keranjang){
        RecyclerView cartList = new RecyclerView(this);
        LinearLayout.LayoutParams linLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        cartList.setPadding(10,0,10,5);
        linLayoutParams.setMargins(convertdp(0),convertdp(5),convertdp(0),convertdp(5));
        cartList.setPadding(convertdp(5),convertdp(10),convertdp(5),convertdp(10));
        cartList.setLayoutParams(linLayoutParams);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        cartList.setLayoutManager(layoutManager);
        cartList.setHasFixedSize(true);
        cartList.setNestedScrollingEnabled(false);
        cartList.setBackground(getResources().getDrawable(R.drawable.rounded_product));
        AdapterCheckout adapterCheckout = new AdapterCheckout(this, keranjang);
        cartList.setAdapter(adapterCheckout);
        TextView textView = new TextView(this);
        Typeface typeface = ResourcesCompat.getFont(this, R.font.poppins_semibold);
        textView.setText("Pesan");
        textView.setPadding(0,convertdp(5),0,0);
        textView.setTypeface(typeface);

        EditText pesan = new EditText(this);
        pesan.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        pesan.setPadding(convertdp(15),convertdp(10),convertdp(15),convertdp(10));
        pesan.setHint("Pesan Ke Penjual");
        pesan.setBackground(getResources().getDrawable(R.drawable.edit_text_product));
        TextView textView1 = new TextView(this);
        textView1.setText("Reward Ke Penjual");
        textView1.setPadding(0,convertdp(5),0,0);
        textView1.setTypeface(typeface);
        EditText donasi = new EditText(this);
        donasi.setHint("Reward Ke Penjual");
        donasi.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        donasi.setPadding(convertdp(15),convertdp(10),convertdp(15),convertdp(10));
        donasi.setBackground(getResources().getDrawable(R.drawable.edit_text_product));
        listPesan.add(pesan);
        listDonasi.add(donasi);

        id_user.add(keranjang.get(0).getIdToko()!=null?keranjang.get(0).getIdToko():keranjang.get(0).getIdUser());
        layoutProduct.addView(cartList);
        layoutProduct.addView(textView);
        layoutProduct.addView(pesan);
        layoutProduct.addView(textView1);
        layoutProduct.addView(donasi);
        List<Products> barang = new ArrayList<>();
        for (int i=0; i<keranjang.size(); i++){
            barang.add(new Products(keranjang.get(i).getIdProduct(),keranjang.get(i).getJml()));
        }

        listProducts.add(new ListProducts(barang));
        donasi.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s != null && s.length() > 0) {
                    totalBayar.setText(formatCurrency.Rupiah(Double.parseDouble(String.valueOf(TotalBayar + Float.parseFloat(s.toString())))));

                }else{
                    totalBayar.setText(formatCurrency.Rupiah(Double.parseDouble(String.valueOf(TotalBayar + 0))));
                }
                float donasi=0;
                for (int i=0; i<listDonasi.size(); i++){
                    if (listDonasi.get(i).getText() != null && listDonasi.get(i).getText().length() > 0) {
                        donasi = donasi + Float.parseFloat(listDonasi.get(i).getText().toString());
                    }
                }
                totalDonasi.setText(formatCurrency.Rupiah(Double.parseDouble(String.valueOf(donasi))));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after){
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }
    public Integer convertdp(Integer number){
        float data=number * getResources().getDisplayMetrics().density;
        String hasil =String.format("%.0f", data);
        Log.e("HASIL",hasil);
        return Integer.valueOf(hasil);
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiAlamat();
                    Email=login.getEmail();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void Aksi(){
        voucher.setOnClickListener(view -> {
            Intent intent= new Intent(CheckoutActivity.this, VoucherActivity.class);
            intent.putExtra("JMLHARGA",TotalHarga);
            startActivityForResult(intent,LAUNCH_SECOND_ACTIVITY);
        });
        btnBack.setOnClickListener(view -> onBackPressed());
        btnCheckout.setOnClickListener(view -> {

            if (payment==null) {
                Intent intent = new Intent(CheckoutActivity.this, PaymentActivity.class);
                startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
            }else{
                if (alamat!=null) {
                    Payment();
                }else{
                    Toast.makeText(this, "Silahkan Pilih Alamat Terlebih Dahulu", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnAlamat.setOnClickListener(view -> {
            Intent intent = new Intent(CheckoutActivity.this, AddressActivity.class);
            startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
        });
        btnLayoutPembeyaran.setOnClickListener(v -> {
            Intent intent = new Intent(CheckoutActivity.this, PaymentActivity.class);
            startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
        });



    }
    public void ApiAlamat(){
        Call<ApiResponse> call = apiinterface.ALamatUtama(Token,id_users);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getAlamat().get(0)!=null){
                        gedung.setText(response.body().getAlamat().get(0).getGedung());
                        namaUser.setText(response.body().getAlamat().get(0).getNamaLengkap());
                        nomorUser.setText(response.body().getAlamat().get(0).getNoTlpn());
                        alamatUser.setText(response.body().getAlamat().get(0).getJalan()+", "+
                                response.body().getAlamat().get(0).getKelurahan()+", "+
                                response.body().getAlamat().get(0).getKecamatan()+", "+
                                response.body().getAlamat().get(0).getKabupaten()+", "+
                                response.body().getAlamat().get(0).getProvinsi()+" "+
                                response.body().getAlamat().get(0).getKodepos());
                        alamat=response.body().getAlamat().get(0);
                    }
                    else{
                        Intent intent = new Intent(CheckoutActivity.this, AddressActivity.class);
                        startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void Payment(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Checkout...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        List<Checkouts> checkouts = new ArrayList<>();
        List<Integer> id_keranjang=new ArrayList<>();
        for (Produk produk:produks){
            id_keranjang.add(produk.getIdKeranjang());
        }
        for (int i = 0; i< listProducts.size(); i++){
            String don ="0";
            if(!listDonasi.get(i).getText().toString().isEmpty()){
                don=listDonasi.get(i).getText().toString();
            }
            checkouts.add(new Checkouts(
                    id_user.get(i),
                    null,
                    alamat.getIdAlamat(),
                    Double.parseDouble(String.valueOf(totalPotongan)),
                    listProducts.get(i).getProduk(),
                    listPesan.get(i).getText().toString(),
                    expedisi,
                    25000.0,
                    Double.parseDouble(don),
                    "reg"));
        }
        ListCheckut c=new ListCheckut(checkouts,payment.getKode(),id_keranjang,payment.getMetode());
        Gson gson = new Gson();
        String jsonString = gson.toJson(c);
        Log.e("JSON",jsonString);
        RequestBody body = null;
        try {
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),(new JSONObject(jsonString)).toString());
        } catch (JSONException e) {
            e.printStackTrace();

        }
        Call<ApiResponse> call = apiinterface.Checkout(Token,body);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()!=null){
                        if (response.body().getSuccess()){
                            progressDialog.dismiss();
                            Intent intent = new Intent(CheckoutActivity.this, DetailPaymentActivity.class);
                            intent.putExtra("DETAILPAYMENT",response.body().getPayment());
                            intent.putExtra("TAGIHAN",TotalBayar.toString());
                            startActivity(intent);
                            finish();

                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Transaction Failed",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                Log.e("Error",t.toString());
            }
        });
    }
    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    Alamat alamats = (Alamat) bundle.getSerializable("ALAMAT");
                    if (alamats != null) {
                        gedung.setText(alamats.getGedung());
                        namaUser.setText(alamats.getNamaLengkap());
                        nomorUser.setText(alamats.getNoTlpn());
                        alamatUser.setText(alamats.getJalan() + ", " +
                                alamats.getKelurahan() + ", " +
                                alamats.getKecamatan() + ", " +
                                alamats.getKabupaten() + ", " +
                                alamats.getProvinsi() + " " +
                                alamats.getKodepos());
                        alamat=alamats;

                    }
                }
                Voucher vouc =(Voucher) bundle.getSerializable("VOUCHER");
                if (vouc!=null){
                    voucher.setText(vouc.getKode().toUpperCase(Locale.ROOT));
                }
                Payment payment =(Payment) bundle.getSerializable("PAYMENT");
                if(payment!=null){
                    this.payment = payment;
                    switch (payment.getKode()){
                        case "009":
                            gmbrbank.setImageDrawable(getResources().getDrawable(R.drawable.bni));
                            break;
                        case "002":
                            gmbrbank.setImageDrawable(getResources().getDrawable(R.drawable.bri));
                            break;
                        case "014":
                            gmbrbank.setImageDrawable(getResources().getDrawable(R.drawable.bca));
                            break;
                        case "008":
                            gmbrbank.setImageDrawable(getResources().getDrawable(R.drawable.mandiri));
                            break;
                    }
                    namabank.setText(payment.getBank());
                    metode.setText(payment.getMetode());
                    btnLayoutPembeyaran.setVisibility(View.VISIBLE);
                    btnCheckout.setText("Checkout");
                }

            }

        }
    } //onActivityResult

}