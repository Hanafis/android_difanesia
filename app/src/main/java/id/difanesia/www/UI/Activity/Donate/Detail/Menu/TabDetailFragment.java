package id.difanesia.www.UI.Activity.Donate.Detail.Menu;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.Donasi;
import id.difanesia.www.R;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;


public class TabDetailFragment extends Fragment {
    TextView detaildonasil;

    View view;
    Donasi donasi;

    public TabDetailFragment(Donasi donasi) {
        this.donasi=donasi;
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_tab_detail, container, false);
        detaildonasil=view.findViewById(R.id.DetailText);
        if (donasi!=null) {
        detaildonasil.setText(donasi.getDeskripsi());
        }


        return view;
    }


}