package id.difanesia.www.UI.Activity.Riwayat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.Riwayat;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Donate.Detail.DetailDonasiActivity;
import id.difanesia.www.UI.Adapter.AdapterTabDetailDonate;
import id.difanesia.www.UI.Adapter.AdapterTabRiwayat;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RiwayatActivity extends AppCompatActivity {

    AdapterTabRiwayat adapter;
    ImageButton btnBack;
    ViewPager2 viewPager2;
    TabLayout tabLayout;
    LoginViewModel loginViewModel;
    ApiRequest apiinterface;
    String Token;
    Integer id_users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        viewPager2=findViewById(R.id.pager);
        btnBack=findViewById(R.id.BtnBack);
        btnBack.setOnClickListener(view->onBackPressed());
        tabLayout=findViewById(R.id.LayoutTabs);

        Token();
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    Menu();
                    ApiRiwayat();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void Menu(){
        adapter=new AdapterTabRiwayat(this);

        tabLayout.addTab(tabLayout.newTab().setText("Belum Dibayar"));
        tabLayout.addTab(tabLayout.newTab().setText("Dikemas"));
        tabLayout.addTab(tabLayout.newTab().setText("Dikirim"));
        tabLayout.addTab(tabLayout.newTab().setText("Selesai"));
        tabLayout.addTab(tabLayout.newTab().setText("Batal"));
        tabLayout.addTab(tabLayout.newTab().setText("Dikembalikan"));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });

    }
    public void ApiRiwayat(){
        Call<ApiResponse> call = apiinterface.RiwayatList(Token,id_users);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                    if (response.body()!=null){
                        adapter.addData(response.body().getLBayar(),
                                        response.body().getLKemas(),
                                        response.body().getLKirim(),
                                        response.body().getLSelesai(),
                                        response.body().getLBatal(),
                                        response.body().getLPengembalian());
                        viewPager2.setAdapter(adapter);

                    }else{

                    }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}