package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.joda.time.DateMidnight;
import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.difanesia.www.Model.Voucher;
import id.difanesia.www.R;

public class AdapterVoucher extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Voucher> vouchers;
    Context context;
    OnClickButton onClickButton;

    public AdapterVoucher(Context context,List<Voucher> vouchers,OnClickButton onClickButton){
        this.context=context;
        this.vouchers=vouchers;
        this.onClickButton=onClickButton;

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_voucher,parent,false);
        return new VoucherViewHolder(view, onClickButton);
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof VoucherViewHolder){
                final VoucherViewHolder voucherViewHolder = (VoucherViewHolder) holder;
                voucherViewHolder.namaVoucher.setText(vouchers.get(position).getNamaKupon());
                if(vouchers.get(position).getGambar()!=null){
                    Picasso.get().load(vouchers.get(position).getGambar()).into(voucherViewHolder.gambarVoucher);
                }
                voucherViewHolder.expiredDate.setText(vouchers.get(position).getExpiredDate());
                voucherViewHolder.layoutVoucher.setOnClickListener(view -> onClickButton.OnClickDetail(voucherViewHolder.getAdapterPosition()));





            }
    }

    @Override
    public int getItemCount() {
        return this.vouchers==null?0:vouchers.size();
    }
    public interface OnClickButton{
        void OnclickVoucher(int posisi);
        void OnClickDetail(int posisi);
    }

    private static class VoucherViewHolder extends RecyclerView.ViewHolder implements OnClickButton {
        TextView namaVoucher,expiredDate;
        ImageView gambarVoucher;
        LinearLayout layoutVoucher;
        public VoucherViewHolder(View view, OnClickButton onClickButton) {
            super(view);
            namaVoucher=view.findViewById(R.id.JudulVoucher);
            expiredDate=view.findViewById(R.id.TanggalVoucher);
            gambarVoucher=view.findViewById(R.id.GambarVoucher);
            layoutVoucher=view.findViewById(R.id.LayoutVoucher);


        }

        @Override
        public void OnclickVoucher(int posisi) {

        }

        @Override
        public void OnClickDetail(int posisi) {

        }
    }
}
