package id.difanesia.www.UI.Activity.Product.Rating;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.util.List;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.Model.Riwayat;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Product.Search.SearchListActivity;
import id.difanesia.www.UI.Adapter.AdapterNewRating;
import id.difanesia.www.ViewModel.LoginViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewRatingProduct extends AppCompatActivity {
    LinearLayout btnBackHeader;
    ApiRequest apiinterface;
    AdapterNewRating adapterNewRating;
    RecyclerView productList;
    List<Produk> produks=null;
    LinearLayout layoutBtnImage,btnRating,btnBack;
    ImageView gambarRating;
    RatingBar ratingBarProduct;
    TextView namaProduct;
    EditText deskripsiRating;
    Button btnTempRat1,btnTempRat2,btnTempRat3,btnTempRat4,btnTempRat5,btnKirim,btnClose;
    BottomSheetDialog  filterProduct;
    Produk tempProduk;
    View filter;
    Uri imagepath;
    String Token;
    Riwayat riwayat;
    Integer Ratings=5;
    LoginViewModel loginViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_rating_product);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);

        //product
        btnBackHeader=findViewById(R.id.HeaderAtas);
        productList=findViewById(R.id.ListRatingProduct);
        productList.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        productList.setHasFixedSize(true);
        productList.setNestedScrollingEnabled(false);
        btnBack=findViewById(R.id.BtnBack);
        btnBack.setOnClickListener(view->{ Intent returnIntent = new Intent();
            setResult(RESULT_OK,returnIntent);
            finish();});
        Token();
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            produks=null;
            riwayat=null;
        } else {
            produks= (List<Produk>) extras.getSerializable("PRODUCT");
            riwayat= (Riwayat) extras.getSerializable("RIWAYAT");
        }
        if (produks!=null){
            adapterNewRating= new AdapterNewRating(this, produks, new AdapterNewRating.OnBtnClick() {
                @Override
                public void OnClickProduct(Produk produk) {
                    if (!produk.getIsRating()) {
                        tempProduk = produk;
                        namaProduct.setText(produk.getNama());
                        Picasso.get().load(produk.getImage()).into(gambarRating);

                        filterProduct.setContentView(filter);
                        filterProduct.show();
                    }else{
                        Toast.makeText(getApplicationContext(),"Kamu Sudah Menilai Produk Ini",Toast.LENGTH_LONG).show();
                    }
                }
            });
            productList.setAdapter(adapterNewRating);
            FilterLayout();
        }

    }
    private void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public void FilterLayout(){
        filterProduct = new BottomSheetDialog(NewRatingProduct.this, R.style.BottomSheetDialogTheme);
        filter= LayoutInflater.from(getApplicationContext()).inflate(R.layout.rating_popup,findViewById(R.id.FilterContainer));
        namaProduct=filter.findViewById(R.id.NamaProduct);
        layoutBtnImage=filter.findViewById(R.id.LayoutBtnImage);
        btnRating=filter.findViewById(R.id.BtnRatings);
        gambarRating=filter.findViewById(R.id.GambarRating);
        ratingBarProduct=filter.findViewById(R.id.ratingBarProduct);
        deskripsiRating=filter.findViewById(R.id.DeskripsiRating);
        btnTempRat1=filter.findViewById(R.id.Btntemrat1);
        btnTempRat2=filter.findViewById(R.id.Btntemrat2);
        btnTempRat3=filter.findViewById(R.id.Btntemrat3);
        btnTempRat4=filter.findViewById(R.id.Btntemrat4);
        btnTempRat5=filter.findViewById(R.id.Btntemrat5);
        btnClose=filter.findViewById(R.id.BtnClosePopup);
        btnKirim=filter.findViewById(R.id.BtnKirim);
        btnClose.setOnClickListener(view -> {
            filterProduct.hide();
        });
        btnKirim.setOnClickListener(view -> {
            if (riwayat!=null) {
                ApiRating();
            }
        });
        namaProduct.setText("");
        btnTempRat1.setOnClickListener(v -> deskripsiRating.setText(deskripsiRating.getText()+" "+btnTempRat1.getText()));
        btnTempRat2.setOnClickListener(v -> deskripsiRating.setText(deskripsiRating.getText()+" "+btnTempRat2.getText()));
        btnTempRat3.setOnClickListener(v -> deskripsiRating.setText(deskripsiRating.getText()+" "+btnTempRat3.getText()));
        btnTempRat4.setOnClickListener(v -> deskripsiRating.setText(deskripsiRating.getText()+" "+btnTempRat4.getText()));
        btnTempRat5.setOnClickListener(v -> deskripsiRating.setText(deskripsiRating.getText()+" "+btnTempRat5.getText()));
        btnRating.setOnClickListener(v -> ImagePicker.Companion.with(NewRatingProduct.this)
                .crop()
                .compress(1024)
                .maxResultSize(1080,1080)
                .start());
        gambarRating.setOnClickListener(v -> ImagePicker.Companion.with(NewRatingProduct.this)
                .crop()
                .compress(1024)
                .maxResultSize(1080,1080)
                .start());

    }
    private void ApiRating() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Rating Product...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        RequestBody id_transaksi = RequestBody.create(MediaType.parse("text/plain"), riwayat.getIdTransaksi().toString().trim());
        RequestBody id_transaksi_list = RequestBody.create(MediaType.parse("text/plain"), tempProduk.getIdTransaksiList().toString().trim());
        RequestBody id_product = RequestBody.create(MediaType.parse("text/plain"), tempProduk.getIdProduct().toString().trim());
        RequestBody rating = RequestBody.create(MediaType.parse("text/plain"), Ratings.toString().trim());
        RequestBody komentar = RequestBody.create(MediaType.parse("text/plain"), deskripsiRating.getText().toString().trim());
        if (imagepath!=null){
            MultipartBody.Part body=null;
            if (getRealPathFromURI(imagepath) != null) {
                File file = new File(getRealPathFromURI(imagepath)); //Get File
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
            }
                Call<ApiResponse> call = apiinterface.TambahRating(Token, id_transaksi, id_transaksi_list, id_product, rating, komentar, body);
                call.enqueue(new Callback<ApiResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                        if (response.body() != null) {
                            if (response.body().getSuccess() != null) {
                                filterProduct.hide();
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                            }
                        }else{
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();

                    }
                });
            }


    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data.getData()!=null) {
            btnRating.setVisibility(View.GONE);
            gambarRating.setVisibility(View.VISIBLE);
            imagepath = data.getData();
            gambarRating.setImageURI(imagepath);
        }

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(RESULT_OK,returnIntent);
        finish();

    }
}