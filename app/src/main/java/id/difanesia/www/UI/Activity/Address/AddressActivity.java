package id.difanesia.www.UI.Activity.Address;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.Serializable;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterAddress;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressActivity extends AppCompatActivity {
    ImageButton btnBack;
    Button btnTambah;
    RecyclerView listAddress;
    ApiRequest apiinterface;
    String Token;
    Integer id_users,Limit=12,Start=0;
    AdapterAddress adapterAddress;
    LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        btnBack=findViewById(R.id.BtnBack);
        btnTambah=findViewById(R.id.BtnTambah);
        listAddress=findViewById(R.id.AddressList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        listAddress.setLayoutManager(layoutManager);
        listAddress.setHasFixedSize(true);
        listAddress.setNestedScrollingEnabled(false);
        Token();
        btnBack.setOnClickListener(view -> onBackPressed());
        btnTambah.setOnClickListener(view -> {
            Intent intent = new Intent(AddressActivity.this,AddAddressActivity.class);
            startActivity(intent);
        });

    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiAlamat();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void ApiAlamat(){
        Call<ApiResponse> call = apiinterface.ALamatList(Token,id_users);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getAlamat().size()>0){
                        adapterAddress=new AdapterAddress(getApplicationContext(), response.body().getAlamat(), new AdapterAddress.OnBtnClick() {
                            @Override
                            public void OnClickAlamat(int posisi) {
                                Intent intent = new Intent();
                                intent.putExtra("ALAMAT", response.body().getAlamat().get(posisi));
                                intent.putExtra("GANTI",(Boolean) true);
                                setResult(Activity.RESULT_OK,intent);
                                adapterAddress.dipilih(posisi);
                                finish();

                            }

                            @Override
                            public void OnClickUpdate(int posisi) {

                                    Intent intent = new Intent(AddressActivity.this,EditAddressActivity.class);
                                    intent.putExtra("IDALAMAT", response.body().getAlamat().get(posisi));
                                    startActivity(intent);

                            }
                        });
                        listAddress.setAdapter(adapterAddress);
                        adapterAddress.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }


}