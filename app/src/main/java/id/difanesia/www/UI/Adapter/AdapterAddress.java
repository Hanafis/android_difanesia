package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.difanesia.www.Model.Alamat;
import id.difanesia.www.R;

public class AdapterAddress extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Alamat> alamats;
    OnBtnClick onBtnClick;
    public AdapterAddress(Context context,List<Alamat> alamats,OnBtnClick onBtnClick){
            this.context=context;
            this.alamats=alamats;
            this.onBtnClick=onBtnClick;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_address, parent, false);
        return new AddressViewHolder(view, onBtnClick);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof AddressViewHolder){
                final AdapterAddress.AddressViewHolder addressViewHolder = (AddressViewHolder) holder;
                addressViewHolder.gedung.setText(alamats.get(position).getGedung());
                addressViewHolder.namaUser.setText(alamats.get(position).getNamaLengkap());
                addressViewHolder.nomorUser.setText(alamats.get(position).getNoTlpn());
                addressViewHolder.alamatUser.setText(alamats.get(position).getJalan()+", "+
                        alamats.get(position).getKelurahan()+", "+
                        alamats.get(position).getKecamatan()+", "+
                        alamats.get(position).getKabupaten()+", "+
                        alamats.get(position).getProvinsi()+" "+
                        alamats.get(position).getKodepos());
                if (position==0){
                    addressViewHolder.dipilih.setVisibility(View.VISIBLE);
                }
                if (alamats.get(position).getIdAlamat().toString().equals("ya")){
                    addressViewHolder.utama.setVisibility(View.VISIBLE);
                }
                addressViewHolder.layoutAddress.setOnClickListener(view -> onBtnClick.OnClickAlamat(addressViewHolder.getAdapterPosition()));
                addressViewHolder.btnUbah.setOnClickListener(view -> onBtnClick.OnClickUpdate(addressViewHolder.getAdapterPosition()));
            }
    }

    @Override
    public int getItemCount() {
        return alamats==null?0:alamats.size();
    }
    public interface OnBtnClick {
        void OnClickAlamat(int posisi);
        void OnClickUpdate(int posisi);

    }

    private static class AddressViewHolder extends RecyclerView.ViewHolder implements OnBtnClick{
        LinearLayout layoutAddress;
        Button btnUbah;
        TextView namaUser,gedung,nomorUser,alamatUser,utama,dipilih;
        public AddressViewHolder(View view, OnBtnClick onBtnClick) {
            super(view);
            layoutAddress=view.findViewById(R.id.LayoutAlamat);
            btnUbah=view.findViewById(R.id.BtnUbah);
            namaUser=view.findViewById(R.id.NamaUser);
            nomorUser=view.findViewById(R.id.NomorUser);
            alamatUser=view.findViewById(R.id.AlamatUser);
            gedung=view.findViewById(R.id.Gedung);
            utama=view.findViewById(R.id.AlamatUtama);
            dipilih=view.findViewById(R.id.AlamatDipilih);
        }

        @Override
        public void OnClickAlamat(int posisi) {

        }

        @Override
        public void OnClickUpdate(int posisi) {

        }
    }
    @SuppressLint("NotifyDataSetChanged")
    public void dipilih(int posisi){
        Alamat p = this.alamats.get(posisi);
        this.alamats.remove(posisi);
        notifyItemRemoved(posisi);
        notifyItemRangeChanged(posisi, this.alamats.size());
        notifyItemRangeChanged(posisi, alamats.size());
        this.alamats.add(0,p);
        notifyDataSetChanged();
    }
}
