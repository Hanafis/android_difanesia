package id.difanesia.www.UI.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import id.difanesia.www.UI.Activity.Berita.ListBerita;
import id.difanesia.www.UI.Activity.Donate.ListDonasi;

public class AdapterTabDonate extends FragmentStateAdapter {

    public AdapterTabDonate(@NonNull FragmentActivity fragmentActivity)
    {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        if (position == 1) {
            return new ListBerita();
        }
        return new ListDonasi();
    }
    @Override
    public int getItemCount() {return 2; }
}
