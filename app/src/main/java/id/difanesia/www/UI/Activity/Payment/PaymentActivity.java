package id.difanesia.www.UI.Activity.Payment;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import id.difanesia.www.Model.Payment;
import id.difanesia.www.R;
public class PaymentActivity extends AppCompatActivity {
    LinearLayout bni,bri,mandiri,bca,bniManual;
    ImageButton btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        bni=findViewById(R.id.BNI);
        bniManual=findViewById(R.id.BNIManual);
        bri=findViewById(R.id.BRI);
        mandiri=findViewById(R.id.MANDIRI);
        bca=findViewById(R.id.BCA);
        btnBack=findViewById(R.id.BtnBack);
        btnBack.setOnClickListener(view -> onBackPressed());
//        bni.setOnClickListener(v -> {
//            Intent intent = new Intent();
//            Payment payment = new Payment("VA","Bank Nasional Indonesia","009");
//            intent.putExtra("PAYMENT", payment);
//            setResult(Activity.RESULT_OK,intent);
//            finish();
//        });
        bniManual.setOnClickListener(v -> {
            Intent intent = new Intent();
            Payment payment = new Payment("MANUAL","Bank Nasional Indonesia","009");
            intent.putExtra("PAYMENT", payment);
            setResult(Activity.RESULT_OK,intent);
            finish();
        });
//        bri.setOnClickListener(v -> {
//            Intent intent = new Intent();
//            Payment payment = new Payment("VA","Bank Rakyat Indonesia","002");
//            intent.putExtra("PAYMENT", payment);
//            setResult(Activity.RESULT_OK,intent);
//            finish();
//        });
//        mandiri.setOnClickListener(v -> {
//            Intent intent = new Intent();
//            Payment payment = new Payment("VA","Mandiri","008");
//            intent.putExtra("PAYMENT", payment);
//            setResult(Activity.RESULT_OK,intent);
//            finish();
//        });
//        bca.setOnClickListener(v -> {
//            Intent intent = new Intent();
//            Payment payment = new Payment("VA","Bank Central Asia","014");
//            intent.putExtra("PAYMENT", payment);
//            setResult(Activity.RESULT_OK,intent);
//            finish();
//        });
    }
}