package id.difanesia.www.UI.Activity.Product.Search;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.SpacesItemDecoration;
import id.difanesia.www.MainActivity;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Product.DetailProductActivity;
import id.difanesia.www.UI.Adapter.AdapterFilterKota;
import id.difanesia.www.UI.Adapter.AdapterProduct;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchListActivity extends AppCompatActivity {
    ApiRequest apiinterface;
    NestedScrollView infinityScroll;
    ImageButton btnBack,btnFilter,btnClose;
    LinearLayout layoutSemuaLokasi,layoutNotNotifikasi;
    EditText btnSearch,minEditText,maxEditText;
    ShimmerFrameLayout loadingProduct;
    List<String> Kota;
    RecyclerView productList,kotaList,kotaAllList;
    AdapterProduct adapterProduct;
    AdapterFilterKota adapterFilterKota;
    BottomSheetDialog  filterProduct;
    Button btnApplyFilter,btnAllKota,btnCloseKota,btnTerlaris,btnTertinggi,btnTerrendah;
    View filter;
    String Token, Urutan,Min="0",Max="0",Keyword;
    Integer id_users,Limit=12,Start=0;
    Boolean Koneksi=false,loading=false,endline=false;
    LoginViewModel loginViewModel;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_list);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        infinityScroll=findViewById(R.id.ProductInfinity);
        btnFilter=findViewById(R.id.BtnApplyFilter);
        btnBack=findViewById(R.id.BtnBack);
        btnSearch=findViewById(R.id.BtnSearch);
        loadingProduct=findViewById(R.id.LoadingProduct);
        layoutNotNotifikasi=findViewById(R.id.LayoutNot);
        Kota= new ArrayList<>();
        productList =findViewById(R.id.ProductList);
        productList.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        productList.setHasFixedSize(true);
        productList.setNestedScrollingEnabled(false);
        SpacesItemDecoration decoration = new SpacesItemDecoration(8,true);
        productList.addItemDecoration(decoration);
        loadingProduct.startShimmer();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                Keyword= null;
            } else {
                Keyword= extras.getString("KEYWORDSEARCH");
            }
        }
        else {
            Keyword= (String) savedInstanceState.getSerializable("KEYWORDSEARCH");
        }
        if (Keyword!=null){
            btnSearch.setText(Keyword);
            Aksi();
            Token();
        }
        if (infinityScroll != null) {
            infinityScroll.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {



                if (scrollY == ( v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if(!loading){
                        loading=true;
                        TambahProduct();

                    }
                }
            });
        }


    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    FilterLayout();
                    ApiSearchProduk(false);


                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void Aksi(){
        btnBack.setOnClickListener(view -> {
            Intent intent = new Intent(SearchListActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        });
        btnFilter.setOnClickListener(view -> {
            filterProduct.setContentView(filter);
            filterProduct.show();
        });
        btnSearch.setOnClickListener(view -> onBackPressed());
    }
    public void FilterLayout(){
        filterProduct = new BottomSheetDialog(SearchListActivity.this, R.style.BottomSheetDialogTheme);
        filter= LayoutInflater.from(getApplicationContext()).inflate(R.layout.filter_layout,findViewById(R.id.FilterContainer));
        btnClose=filter.findViewById(R.id.BtnCloseFilter);
        layoutSemuaLokasi=filter.findViewById(R.id.LayoutSemuaLokasi);
        btnAllKota=filter.findViewById(R.id.BtnAllKota);
        btnCloseKota=filter.findViewById(R.id.BtnCloseKota);
        btnApplyFilter=filter.findViewById(R.id.BtnApplyFilter);
        btnTerlaris=filter.findViewById(R.id.BtnTerlaris);
        btnTertinggi=filter.findViewById(R.id.BtnTertinggi);
        btnTerrendah=filter.findViewById(R.id.BtnTerrendah);
        kotaList=filter.findViewById(R.id.KotaList);
        kotaList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        kotaList.setHasFixedSize(true);
        kotaList.setNestedScrollingEnabled(false);
        minEditText=filter.findViewById(R.id.MinEditText);
        maxEditText=filter.findViewById(R.id.MaxEditText);
        btnAllKota.setOnClickListener(view -> viewVisibleAnimator(layoutSemuaLokasi));
        btnCloseKota.setOnClickListener(view -> viewGoneAnimator(layoutSemuaLokasi));
        btnApplyFilter.setOnClickListener(view -> {
            if (!maxEditText.getText().toString().equals("0")) {
                Min = minEditText.getText().toString().trim();
                Max = maxEditText.getText().toString().trim();
            }
            else
            {
                Min=null;
                Max=null;
            }
            ApiSearchProduk(true);
            filterProduct.hide();
        });
        FilterUrutkan();
        RangeHarga();
        kotaAllList =filter.findViewById(R.id.KotaAllList);
        kotaAllList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        kotaAllList.setHasFixedSize(true);




    }
    @SuppressLint("NotifyDataSetChanged")
    public void ApiSearchProduk(Boolean applyFilter){

        productList.setVisibility(View.VISIBLE);
        Call<ApiResponse> call = apiinterface.Search(Token,Keyword,Start,Limit,Kota,Urutan,Min,Max);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null) {
                    if (response.body().getProduk()!=null){
                        if (response.body().getProduk().size() > 0) {
                            Log.e("SIZE SEARCH", String.valueOf(response.body().getProduk().size()));
                            adapterProduct = new AdapterProduct(getApplicationContext(), response.body().getProduk(), id_users, new AdapterProduct.OnBtnClick() {
                                @Override
                                public void OnClickProduct(int posisi, Produk produk) {
                                    Intent intent = new Intent(SearchListActivity.this, DetailProductActivity.class);
                                    intent.putExtra("DETAILPRODUK",produk);
                                    startActivity(intent);
                                }

                                @Override
                                public void OnClickWish(Integer posisi, Produk produk) {
                                    ApiWish(produk.getIdProduct(), posisi, produk.getIdWish());
                                }
                            });
                            Koneksi = true;
                            adapterProduct.notifyDataSetChanged();
                            productList.setAdapter(adapterProduct);
                            loadingProduct.stopShimmer();
                            loadingProduct.setVisibility(View.GONE);
                            layoutNotNotifikasi.setVisibility(View.GONE);

                        } else {
                            productList.setVisibility(View.GONE);
                            layoutNotNotifikasi.setVisibility(View.VISIBLE);
                            loadingProduct.stopShimmer();
                            loadingProduct.setVisibility(View.GONE);
                        }
                    }else{
                        layoutNotNotifikasi.setVisibility(View.VISIBLE);
                        loadingProduct.stopShimmer();
                        loadingProduct.setVisibility(View.GONE);
                    }

                    if (!applyFilter) {
                        assert response.body() != null;
                        if (response.body().getProvinsi() != null) {
                            if (response.body().getProvinsi().size() > 0) {
                                adapterFilterKota = new AdapterFilterKota(getApplicationContext(), response.body().getProvinsi(), (posisi, value, kota) -> {
                                    if (value == 0) {
                                        adapterFilterKota.updateDataKota(posisi, 1);
                                        Kota.add(kota);

                                    } else {

                                        adapterFilterKota.updateDataKota(posisi, 0);
                                        Kota.remove(kota);
                                    }

                                    adapterFilterKota.notifyDataSetChanged();
                                });
                                if (response.body().getProvinsi().size() < 9) {
                                    btnAllKota.setVisibility(View.GONE);


                                }
                                Koneksi = true;

                                kotaList.setAdapter(adapterFilterKota);
                                kotaAllList.setAdapter(adapterFilterKota);
                                adapterFilterKota.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                    t.getMessage();
            }
        });
    }
    @SuppressLint("NotifyDataSetChanged")
    public void TambahProduct(){
        if (Koneksi) {
            if (!endline){
                adapterProduct.add(null);
            }
            Start = Start + Limit;
            loading = false;
            Call<ApiResponse> call = apiinterface.Search(Token, Keyword, Start, Limit, Kota, Urutan, Min, Max);
            call.enqueue(new Callback<ApiResponse>() {
                @Override
                public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getProduk().size() > 0) {
                            adapterProduct.add(response.body().getProduk());
                            adapterProduct.notifyDataSetChanged();
                            endline=false;
                        } else{
                            endline=true;
                            adapterProduct.endScroll();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                }
            });
        }
    }
    @SuppressLint("UseCompatLoadingForDrawables")
    public void FilterUrutkan(){
        btnTerlaris.setOnClickListener(view -> {
            Urutan="terlaris";
            btnTerlaris.setBackground(getResources().getDrawable(R.drawable.button_rounded_hijau));
            btnTerlaris.setTextColor(getResources().getColor(R.color.white));
            btnTertinggi.setBackground(getResources().getDrawable(R.drawable.edit_text));
            btnTertinggi.setTextColor(getResources().getColor(R.color.black));
            btnTerrendah.setBackground(getResources().getDrawable(R.drawable.edit_text));
            btnTerrendah.setTextColor(getResources().getColor(R.color.black));

        });
        btnTertinggi.setOnClickListener(view -> {
            Urutan="tertinggi";
            btnTertinggi.setBackground(getResources().getDrawable(R.drawable.button_rounded_hijau));
            btnTertinggi.setTextColor(getResources().getColor(R.color.white));
            btnTerlaris.setBackground(getResources().getDrawable(R.drawable.edit_text));
            btnTerlaris.setTextColor(getResources().getColor(R.color.black));
            btnTerrendah.setBackground(getResources().getDrawable(R.drawable.edit_text));
            btnTerrendah.setTextColor(getResources().getColor(R.color.black));
        });
        btnTerrendah.setOnClickListener(view -> {
            Urutan="terrendah";
            btnTerrendah.setBackground(getResources().getDrawable(R.drawable.button_rounded_hijau));
            btnTerrendah.setTextColor(getResources().getColor(R.color.white));
            btnTertinggi.setBackground(getResources().getDrawable(R.drawable.edit_text));
            btnTertinggi.setTextColor(getResources().getColor(R.color.black));
            btnTerlaris.setBackground(getResources().getDrawable(R.drawable.edit_text));
            btnTerlaris.setTextColor(getResources().getColor(R.color.black));
        });
    }
    public void RangeHarga(){
        minEditText.setText(Min);
        maxEditText.setText(Max);

        maxEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!minEditText.getText().toString().equals("") && !maxEditText.getText().toString().equals("")) {

                    float min = Float.parseFloat(String.valueOf(minEditText.getText()));
                    float max = Float.parseFloat(String.valueOf(maxEditText.getText()));
                    if (max < min) {
                        minEditText.setText(maxEditText.getText());
                        maxEditText.setText(Float.toString(min));
                        minEditText.requestFocus();
                        minEditText.setSelection(minEditText.getText().length());
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    private void viewGoneAnimator(final View view) {

        view.animate()
                .alpha(0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                    }
                });

    }
    private void viewVisibleAnimator(final View view) {

        view.animate()
                .alpha(1f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                    }
                });

    }
    public void ApiWish(int id_product,int posisi,int id_wish){
        Call<ApiResponse> call = apiinterface.WishAdd(Token,id_users,id_product);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()!=null){
                        adapterProduct.updateDataFavorit(posisi,response.body().getSuccess(),id_wish);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}