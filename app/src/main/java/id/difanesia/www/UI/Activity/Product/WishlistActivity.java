package id.difanesia.www.UI.Activity.Product;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.SpacesItemDecoration;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterProduct;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishlistActivity extends AppCompatActivity {
    Button btnEdit;
    ImageButton btnSearch,btnBackMain;
    RecyclerView productList;
    TextInputEditText searchEditText;
    LinearLayout btnBack,itemPencarianLayout,layoutNotNotifikasi;
    String Token;
    Integer id_users;
    Integer Limit=12;
    Integer Start=0;
    Boolean loading=false;
    Boolean Koneksi=false;
    Boolean Pencarian=false,endline=false;
    Boolean First=true;
    AdapterProduct adapterProduct;
    ShimmerFrameLayout loadingProduct;
    ApiRequest apiinterface;
    String Keyword;
    NestedScrollView infinityScroll;
    LoginViewModel loginViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        btnEdit=findViewById(R.id.BtnEdit);
        btnSearch=findViewById(R.id.BtnSearch);
        btnBack=findViewById(R.id.BtnBack);
        btnBackMain=findViewById(R.id.BtnBackMain);
        searchEditText=(TextInputEditText) findViewById(R.id.SearchEditText);
        itemPencarianLayout=findViewById(R.id.itemPencarianLayout);
        productList =findViewById(R.id.ProductList);
        loadingProduct=findViewById(R.id.LoadingProduct);
        layoutNotNotifikasi=findViewById(R.id.LayoutNot);
        loadingProduct.startShimmer();
        productList.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        productList.setHasFixedSize(true);
        productList.setNestedScrollingEnabled(false);
        SpacesItemDecoration decoration = new SpacesItemDecoration(8,true);
        productList.addItemDecoration(decoration);
        Token();
        Aksi();

        searchEditText.setOnKeyListener((view, i, keyEvent) -> {
            if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                Pencarian = true;
                if(Objects.requireNonNull(searchEditText.getText()).toString().equals("")){
                    Keyword=null;
                }
                else {
                    Keyword = searchEditText.getText().toString().trim();
                }
                Start=0;
                Limit=12;
                ApiWishProduk();
                InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                return true;
            }
            return false;
        });
        if (infinityScroll != null) {
            infinityScroll.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {



                if (scrollY == ( v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if(!loading){
                        loading=true;
                        TambahProduct();

                    }
                }
            });
        }
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiWishProduk();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void Aksi(){
        btnBackMain.setOnClickListener(view -> {
            if (Pencarian){
                viewGoneAnimator(itemPencarianLayout);
                Keyword=null;
                ApiWishProduk();
            }
            else {
                onBackPressed();
                finish();
            }
        });
        btnBack.setOnClickListener(view -> {
            onBackPressed();
            finish();
        });
        btnSearch.setOnClickListener(view -> viewVisibleAnimator(itemPencarianLayout));
    }
    public void ApiWishProduk(){
        productList.setVisibility(View.VISIBLE);

        Call<ApiResponse> call = apiinterface.WishList(Token,id_users,Keyword,Start,Limit);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getProduk().size()>0){
                        adapterProduct = new AdapterProduct(getApplicationContext(), response.body().getProduk(), id_users, new AdapterProduct.OnBtnClick() {
                            @Override
                            public void OnClickProduct(int posisi,Produk produk) {
                                Intent intent = new Intent(WishlistActivity.this, DetailProductActivity.class);
                                intent.putExtra("DETAILPRODUK",produk);
                                startActivity(intent);
                            }

                            @Override
                            public void OnClickWish(Integer posisi,Produk produk) {
                                ApiWish(produk.getIdProduct(),posisi,produk.getIdWish());
                            }
                        });
                        Koneksi=true;
                        adapterProduct.notifyDataSetChanged();
                        productList.setAdapter(adapterProduct);
                        loadingProduct.stopShimmer();
                        loadingProduct.setVisibility(View.GONE);
                        productList.setVisibility(View.VISIBLE);
                        layoutNotNotifikasi.setVisibility(View.GONE);

                    }
                    else{
                            loadingProduct.setVisibility(View.GONE);
                            productList.setVisibility(View.GONE);
                            layoutNotNotifikasi.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    public void TambahProduct(){
        if (Koneksi) {
            if (!endline) {
                adapterProduct.add(null);
            }
            Start = Start + Limit;
            loading = false;
            Call<ApiResponse> call = apiinterface.WishList(Token, id_users, Keyword, Start, Limit);
            call.enqueue(new Callback<ApiResponse>() {
                @Override
                public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getProduk().size() > 0) {
                            adapterProduct.add(response.body().getProduk());
                            adapterProduct.notifyDataSetChanged();
                            endline=false;
                        } else {
                            endline = true;
                            adapterProduct.endScroll();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                }
            });
        }
    }
    private void viewGoneAnimator(final View view) {

        view.animate()
                .alpha(0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                    }
                });

    }
    private void viewVisibleAnimator(final View view) {

        view.animate()
                .alpha(1f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                    }
                });

    }
    public void ApiWish(int id_product,int posisi,int id_wish){
        Call<ApiResponse> call = apiinterface.WishAdd(Token,id_users,id_product);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()!=null){
                        adapterProduct.updateDataFavorit(posisi,response.body().getSuccess(),id_wish);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}