package id.difanesia.www.UI.Activity.Riwayat;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.List;

import id.difanesia.www.Model.Riwayat;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterRiwayat;


public class RiwayatFragment extends Fragment {

    List<Riwayat> riwayat;
    View view;
    RecyclerView listRiwayat;
    AdapterRiwayat adapterRiwayat;
    ScrollView scrollView;
    ShimmerFrameLayout loading;
    LinearLayout layoutNot;

    public RiwayatFragment(List<Riwayat> riwayat) {
        this.riwayat=riwayat;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_riwayat, container, false);
        loading=view.findViewById(R.id.Loading);
        loading.startShimmer();
        listRiwayat=view.findViewById(R.id.ListRiwayat);
        scrollView=view.findViewById(R.id.Scroll);
        layoutNot=view.findViewById(R.id.LayoutNot);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        listRiwayat.setLayoutManager(layoutManager);
        listRiwayat.setHasFixedSize(true);
//        listRiwayat.setNestedScrollingEnabled(false);
        if (riwayat!=null){
            if (riwayat.size()>0) {
                adapterRiwayat = new AdapterRiwayat(getContext(), riwayat, new AdapterRiwayat.OnClickButton() {
                    @Override
                    public void OnClickDetail(Riwayat riwayat) {
                        Intent intent = new Intent(getActivity(), DetailRiwayatActivity.class);
                        intent.putExtra("DETAILRIWAYAT", riwayat);
                        startActivity(intent);
                    }

                    @Override
                    public void OnclickBtn(Riwayat riwayat) {
                        if (riwayat.getStatus().equals("Sedang Dikemas") || riwayat.getStatus().equals("Sedang Dikirim") || riwayat.getStatus().equals("Belum Dibayar")) {

                        }
                    }
                });
                listRiwayat.setAdapter(adapterRiwayat);
                scrollView.setVisibility(View.GONE);
            }else{
                scrollView.setVisibility(View.GONE);
                layoutNot.setVisibility(View.VISIBLE);
            }
            loading.stopShimmer();
        }else{

        }
        return view;
    }
}