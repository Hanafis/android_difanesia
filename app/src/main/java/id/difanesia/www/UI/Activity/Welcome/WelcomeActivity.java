package id.difanesia.www.UI.Activity.Welcome;



import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import id.difanesia.www.Model.Welcome;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterWelcome;
import id.difanesia.www.ViewModel.LoginViewModel;

public class WelcomeActivity extends AppCompatActivity {
    ViewPager2 viewPager;
    DotsIndicator dotsIndicator;
    AdapterWelcome adapterSlider;
    LoginViewModel loginViewModel;
    Button btnlogin,btndaftar;
    LinearLayout headerAtas,btnBack,layoutBtn;
    View view4;
    private static int currentPage = 0;
    private static final int NUM_PAGES = 3;
    Timer timer;
    final String[] judul={"Selamat Datang","Apa Itu Difanesia","Memenuhi Kebutuhan",
//            "Memiliki Bisnis Model Baru"
    };
    final String[] isi={
            "Kami siap memenuhi kebutuhan dan hak-hak difabel Indonesia",
            "Kami merupakan perusahaan sosial yang didirikan oleh para difabel,pemerhati dan didukung oleh anak muda",
            "Kami memiliki tujuan memenuhi kebutuhan dan hak-hak difabel di Indonesia",
//            "Kami memiliki misi sosial serta menerapkan bisnis model baru, dengan mengacu pada profit"
    };
    final Integer[] gambar ={R.drawable.satu,R.drawable.dua,R.drawable.tiga};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        viewPager = findViewById(R.id.SliderList);
        dotsIndicator =  findViewById(R.id.dots_indicator);
        btndaftar=findViewById(R.id.BtnDaftar);
        btnlogin=findViewById(R.id.BtnMasuk);
        headerAtas=findViewById(R.id.HeaderAtas);
        layoutBtn=findViewById(R.id.LayoutBtn);
        btnBack=findViewById(R.id.BtnBack);
        view4=findViewById(R.id.view4);
        Token();
        List<Welcome> welcome = new ArrayList<>();
        for (int i=0; i<3; i++) {
            welcome.add(new Welcome(judul[i],isi[i],gambar[i]));
        }
        adapterSlider = new AdapterWelcome(this, welcome, viewPager);
        viewPager.setAdapter(adapterSlider);
        dotsIndicator.setViewPager2(viewPager);
        getPagger();
        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = () -> {
            if (currentPage == NUM_PAGES) {
                currentPage = 0;
            }
            viewPager.setCurrentItem(currentPage++, true);
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500,5000);

    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                headerAtas.setVisibility(View.VISIBLE);
                layoutBtn.setVisibility(View.GONE);
                view4.setVisibility(View.GONE);
                btnBack.setOnClickListener(view -> onBackPressed());

            }
            else{
                btndaftar.setOnClickListener(view -> {
                    Intent masuk = new Intent(WelcomeActivity.this, RegistrasiActivity.class);
                    startActivity(masuk);
                    finish();
                });
                btnlogin.setOnClickListener(view -> {
                    Intent daftar = new Intent(WelcomeActivity.this, LoginActivity.class);
                    startActivity(daftar);
                    finish();
                });
            }
        });

    }
    private void getPagger() {
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                

            }


        });
    }
}