package id.difanesia.www.UI.Activity.Welcome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;


import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.MainActivity;
import id.difanesia.www.Model.Login;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Setting.UpdateProfileActivity;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrasiActivity extends AppCompatActivity {
    Button BtnDaftar;
    LinearLayout BtnMasuk;
    ApiRequest apiinterface;
    LoginViewModel loginViewModel;
    private EditText editTextName, editTextEmail, editTextPassword,editTextCpassword,editNoTlpn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        BtnDaftar=findViewById(R.id.BtnDaftar);
        BtnMasuk=findViewById(R.id.BtnMasuk);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        editTextName = (EditText) findViewById(R.id.NamaTextEdit);
        editNoTlpn = (EditText) findViewById(R.id.NotlpnTextEdit);
        editTextEmail = (EditText) findViewById(R.id.EmailTextEdit);
        editTextPassword = (EditText) findViewById(R.id.PasswordBaru);
        editTextCpassword = (EditText) findViewById(R.id.KonfirmasiPassword);
        BtnMasuk.setOnClickListener(view -> {
            Intent masuk = new Intent(RegistrasiActivity.this, LoginActivity.class);
            startActivity(masuk);
        });
        BtnDaftar.setOnClickListener(view -> {

            if (editTextName.getText().toString().matches("")) {
                editTextName.setError("Kolom Nama Tidak Boleh Kosong");
                editTextName.requestFocus();
                return;

            }
            if (editNoTlpn.getText().toString().matches("")) {
                editNoTlpn.setError("Kolom No Telepon Tidak Boleh Kosong");
                editNoTlpn.requestFocus();
                return;

            }
            if (editTextEmail.getText().toString().matches("")) {
                editTextEmail.setError("Kolom Email Tidak Boleh Kosong");
                editTextEmail.requestFocus();
                return;

            }
            if (editTextPassword.getText().toString().matches("")) {
                editTextPassword.setError("Kolom Password Tidak Boleh Kosong");
                editTextPassword.requestFocus();
                return;

            }
            if (editTextCpassword.getText().toString().matches("")) {
                editTextCpassword.setError("Kolom Konfirmasi Paswword Tidak Boleh Kosong");
                editTextCpassword.requestFocus();
                return;

            }


            if (validate()){
                userSignUp();
            }
        });


    }
    private boolean validate() {
        boolean temp=true;
        String pass=editTextPassword.getText().toString();
        String cpass=editTextCpassword.getText().toString();
        if(!pass.equals(cpass)){
            editTextPassword.setError("Password Tidak Sama");
            editTextPassword.requestFocus();
            temp=false;
        }
        return temp;
    }
    private void userSignUp() {
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        //defining a progress dialog to show while signing up
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Signing Up...");
        progressDialog.show();

        //getting the user values

        String name = editTextName.getText().toString().trim();
        String telepon = editNoTlpn.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String c_password = editTextCpassword.getText().toString().trim();

        //building retrofit object

        //defining the call
        Call<ApiResponse> call = apiinterface.register(name,telepon,email,password,c_password);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                progressDialog.dismiss();
                if(response.body()!=null) {
                    if (response.body().getEmail() != null) {
                        editTextEmail.setError("Format Email Salah/Email Sudah Terdaftar");
                        editTextEmail.requestFocus();

                    } else {
                        ApiResponse list = response.body();
                        Login login = list.getLogin();
                        if (login != null) {
                            loginViewModel.insertData(login);
                            Intent intent = new Intent(RegistrasiActivity.this, MainActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();

                        }
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(), "Gagal Terhubung Ke Server", Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

}