package id.difanesia.www.UI.Adapter.Varian;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import id.difanesia.www.Model.Varian;
import id.difanesia.www.R;

public class AdapterVarian extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Varian> varians;
    List<String> jenis= new ArrayList<>();
    OnBtnClick onBtnClick;
    int VIEWTYPE_BARU = 1;
    public AdapterVarian(Context context,List<Varian> varians, OnBtnClick onBtnClick){
        this.context=context;
        this.varians=varians;
        this.onBtnClick=onBtnClick;

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==VIEWTYPE_BARU) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_varian, parent, false);
            return new VarianViewHolder(view);
        }else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            return new NullViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof VarianViewHolder) {
            final VarianViewHolder varianViewHolder = (VarianViewHolder) holder;
                this.jenis.add(varians.get(position).getVarian());
                varianViewHolder.namaVarian.setText(varians.get(position).getVarian());
                Picasso.get().load(context.getString(R.string.path) +"/uploads/products/"+ varians.get(position).getPathimage()).into(varianViewHolder.gambarVarian);

        }
    }

    @Override
    public int getItemCount()  {
        return varians==null ? 0:varians.size();
    }

    @Override
    public int getItemViewType(int position) {

       return Tambah(position);
    }

    public interface OnBtnClick{
        void OnClickVarian(int posisi);
    }

    private static class VarianViewHolder extends RecyclerView.ViewHolder implements OnBtnClick {
        ImageView gambarVarian;
        TextView namaVarian;
        LinearLayout layoutVarian;
        public VarianViewHolder(View view) {
            super(view);
            gambarVarian=view.findViewById(R.id.GambarVarian);
            namaVarian=view.findViewById(R.id.NamaVarian);
            layoutVarian=view.findViewById(R.id.LayoutVarian);

        }

        @Override
        public void OnClickVarian(int posisi) {

        }
    }

    private static class NullViewHolder extends RecyclerView.ViewHolder {
        public NullViewHolder(View view) {
            super(view);
        }
    }
    public int Tambah(int position){
        int jen;
        jen=jenis.indexOf(varians.get(position).getVarian());
        if (jen>0){
            jenis.add(varians.get(position).getVarian());
            return VIEWTYPE_BARU;
        }
        else{
            return VIEWTYPE_BARU;
        }

    }
}
