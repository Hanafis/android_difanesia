package id.difanesia.www.UI.Activity.Product.Rating;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.List;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.UserRating;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterRating;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RatingFragment extends Fragment {

    View view;
    ApiRequest apiinterface;
    LoginViewModel loginViewModel;
    AdapterRating adapterRating;
    String Token;
    Integer id_users,id_product=0,bintang,Limit=12,Start=0;
    List<UserRating> userRatings;
    NestedScrollView infinityScroll;
    Boolean loading=false;
    ScrollView scrollView;
    ShimmerFrameLayout loadingShimer;
    LinearLayout layoutNot;

    public RatingFragment(Integer bintang, List<UserRating> userRating,Integer id_product) {
        this.bintang=bintang;
        this.userRatings=userRating;
        this.id_product=id_product;

    }
    RecyclerView listRating;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_rating, container, false);
        apiinterface = ApiConfig.getClient(getContext()).create(ApiRequest.class);
        listRating=view.findViewById(R.id.ListRating);
        infinityScroll=view.findViewById(R.id.RatingInfinity);
        scrollView=view.findViewById(R.id.Scroll);
        layoutNot=view.findViewById(R.id.LayoutNot);
        loadingShimer=view.findViewById(R.id.Loading);
        loadingShimer.startShimmer();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        listRating.setLayoutManager(layoutManager);
        listRating.setHasFixedSize(true);
        listRating.setNestedScrollingEnabled(false);
        SharedPreferences sharedPref = view.getContext().getSharedPreferences("DataProduct", Context.MODE_PRIVATE);
        Token();
        Log.e("Bintang",bintang.toString());
        Log.e("DATA", String.valueOf(userRatings.size()));
        if (userRatings!=null) {
            if (userRatings.size() > 0) {
                adapterRating = new AdapterRating(getContext(), userRatings);
                listRating.setAdapter(adapterRating);
                adapterRating.notifyDataSetChanged();
                if (infinityScroll != null) {
                    infinityScroll.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {


                        if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                            if (!loading) {
                                loading = true;
                                if (id_product != null) {

                                }
                                tambahdata();

                            }
                        }
                    });
                    scrollView.setVisibility(View.GONE);
                    loadingShimer.stopShimmer();
                }
            }else{
                scrollView.setVisibility(View.GONE);
                layoutNot.setVisibility(View.VISIBLE);
                loadingShimer.stopShimmer();
            }
        }else{
            scrollView.setVisibility(View.GONE);
            layoutNot.setVisibility(View.VISIBLE);
            loadingShimer.stopShimmer();
        }



        return view;
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(getViewLifecycleOwner(), login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();


                }
                else {
                    Toast.makeText(getContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    //if Scroll Call Api
    public void ApiRating(){

        Call<ApiResponse> call = apiinterface.RatingData(Token,id_product,this.bintang,Start,Limit);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getUserRating().size()>0){
                        adapterRating.add(response.body().getUserRating());
                        adapterRating.notifyDataSetChanged();

                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public  void tambahdata(){

            Start = Start + Limit;
            ApiRating();
            adapterRating.notifyDataSetChanged();
            loading = false;


    }
}