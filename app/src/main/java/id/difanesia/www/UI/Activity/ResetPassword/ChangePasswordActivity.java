package id.difanesia.www.UI.Activity.ResetPassword;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Welcome.LoginActivity;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    LinearLayout btnBack,layoutPasswordLama;
    EditText passwordLama,passwordBaru,konfirmasiPassword;
    Button btnUpdate;
    ApiRequest apiinterface;
    LoginViewModel loginViewModel;
    String Token;
    Integer id_users;
    String email,kode;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        btnBack=findViewById(R.id.BtnBack);
        layoutPasswordLama=findViewById(R.id.LayoutPasswordLama);
        passwordLama=findViewById(R.id.PasswordLama);
        passwordBaru=findViewById(R.id.PasswordBaru);
        konfirmasiPassword=findViewById(R.id.KonfirmasiPassword);
        btnUpdate=findViewById(R.id.BtnUpdate);
        view=findViewById(R.id.view);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                email= null;
                kode=null;
            } else {
                email= (String) extras.getString("EMAIL");
                kode=(String) extras.getString("KODE");
            }
        }
        else {
            email= (String) savedInstanceState.getSerializable("EMAIL");
            kode= (String) savedInstanceState.getSerializable("KODE");
        }
        Token();
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                Token="Bearer "+login.getToken();
                id_users=login.getId_user();
                btnBack.setVisibility(View.VISIBLE);
                layoutPasswordLama.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);
                btnBack.setOnClickListener(view -> onBackPressed());
                btnUpdate.setOnClickListener(view -> {
                    if (validate()){
                        ApiUpdate();
                    }

                });

            }
            else {
                btnUpdate.setOnClickListener(view -> {
                    if (validate()){
                        ApiReset();
                    }

                });
            }
        });

    }
    private boolean validate() {
        boolean temp=true;
        String pass=passwordBaru.getText().toString();
        String cpass=konfirmasiPassword.getText().toString();
        if(!pass.equals(cpass)){
            konfirmasiPassword.setError("Password Tidak Sama");
            konfirmasiPassword.requestFocus();
            temp=false;
        }
        return temp;
    }
    public void ApiUpdate(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Ganti Password...");
        progressDialog.show();
        String old=passwordLama.getText().toString().trim();
        String newp=passwordBaru.getText().toString().trim();
        Call<ApiResponse> call = apiinterface.UpdatePassword(Token,id_users,old,newp);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if(response.body()!=null){
                    if(response.body().getMessage()!=null){
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung ke Server",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
            }
        });
    }
    public void ApiReset(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Ganti Password...");
        progressDialog.show();
        String newp=passwordBaru.getText().toString().trim();
        Call<ApiResponse> call = apiinterface.resetPassword(email,Integer.parseInt(kode),newp);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if(response.body()!=null){
                    if(response.body().getMessage()!=null){
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Berhasil DI Update",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        progressDialog.dismiss();

                        Toast.makeText(getApplicationContext(),"Gagal Terhubung ke Server",Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung ke Server",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
            }
        });
    }
}