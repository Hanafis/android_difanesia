package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


import id.difanesia.www.Model.Provinsi;
import id.difanesia.www.R;

public class AdapterFilterKota extends RecyclerView.Adapter<AdapterFilterKota.ViewHolder>{
    Context context;
    List<Provinsi> itemds;
    OnBtnClick monBtnClick;
    public AdapterFilterKota(Context context, List<Provinsi> itemds, AdapterFilterKota.OnBtnClick onBtnClick){
        super();
        this.monBtnClick=onBtnClick;
        this.context = context;
        this.itemds = itemds;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_kota, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder  holder, @SuppressLint("RecyclerView") int position) {

        holder.BtnUrutkan.setText(itemds.get(position).getKota().trim());
        int values=0;
        //aksi
        if (itemds.get(position).getPilih()!=null){
            if (itemds.get(position).getPilih()==1) {
                values = 1;
            }
        }
        if (values==1) {
            holder.BtnUrutkan.setBackground(context.getResources().getDrawable(R.drawable.button_rounded_hijau));
            holder.BtnUrutkan.setTextColor(context.getResources().getColor(R.color.white));
        }
        else{
            holder.BtnUrutkan.setBackground(context.getResources().getDrawable(R.drawable.edit_text));
            holder.BtnUrutkan.setTextColor(context.getResources().getColor(R.color.black));
        }
        holder.BtnUrutkan.setOnClickListener(view -> {
            int values1 =0;
            //aksi
            if (itemds.get(position).getPilih()!=null){
                if (itemds.get(position).getPilih()==1) {
                    values1 = 1;
                }
            }
            monBtnClick.OnClickFilter(holder.getAdapterPosition(), values1,itemds.get(position).getKota());
        });


    }

    @Override
    public int getItemCount() {
        return itemds==null ? 0:itemds.size();
    }

    public void clear() {
        int size = itemds.size();
        itemds.clear();
        notifyItemRangeRemoved(0, size);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements OnBtnClick{
        Button BtnUrutkan;
        public ViewHolder(@NonNull  View itemView) {
            super(itemView);
            BtnUrutkan=itemView.findViewById(R.id.BtnPilihan);




        }

        @Override
        public void OnClickFilter(int posisi,int value,String kota) {

        }
    }
    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Provinsi> slider) {
        this.itemds = slider;
        this.itemds.add(0, null);
        notifyDataSetChanged();
    }
    @SuppressLint("NotifyDataSetChanged")
    public void updateDataKota(Integer posisis, Integer value) {
        Provinsi p = new Provinsi(itemds.get(posisis).getKota(),value);
        if (value==1) {

            int newPosition = posisis;
            this.itemds.remove(newPosition);
            notifyItemRemoved(newPosition);
            notifyItemRangeChanged(newPosition, this.itemds.size());
            notifyItemRangeChanged(posisis, itemds.size());
            this.itemds.add(0,p);
            notifyDataSetChanged();
        }else{
            int newposisi=posisis;
            this.itemds.set(newposisi,p);
            notifyItemChanged(newposisi);
        }

    }

    public  interface OnBtnClick{

        void OnClickFilter(int posisi,int value,String kota);

    }


}
