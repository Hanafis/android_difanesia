package id.difanesia.www.UI.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import id.difanesia.www.UI.Activity.Chat.ChatList;
import id.difanesia.www.UI.Activity.Chat.ChatNotRead;

public class AdapterTabChat extends FragmentStateAdapter {
    public AdapterTabChat(@NonNull FragmentActivity fragmentActivity)
    {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        if (position == 1) {
            return new ChatNotRead();
        }
        return new ChatList();
    }
    @Override
    public int getItemCount() {return 2; }
}
