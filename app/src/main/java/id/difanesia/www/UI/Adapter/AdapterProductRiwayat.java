package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;

public class AdapterProductRiwayat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Produk> produks;
    Context context;
    FormatCurrency formatCurrency = new FormatCurrency();
    OnClickButton onclickButton;
    int VIEWTYPE_GROUP = 0;
    int VIEWTYPE_PRODUK = 1;

    public AdapterProductRiwayat(Context context, List<Produk> produks, OnClickButton onclickButton){
        this.produks=produks;
        this.context=context;
        this.onclickButton=onclickButton;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==VIEWTYPE_GROUP ){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_riwayat,parent,false);
            return new ProdukHeaderViewHolder(view,onclickButton);
        }
        else if (viewType == VIEWTYPE_PRODUK){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_riwayat,parent,false);
            return new ProdukProductViewHolder(view,onclickButton);
        }
        throw new RuntimeException("No match for " + viewType + ".");

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ProdukHeaderViewHolder){
            final ProdukHeaderViewHolder riwayatHeaderViewHolder = (ProdukHeaderViewHolder) holder;
            riwayatHeaderViewHolder.namaToko.setText(produks.get(position).getToko());
            riwayatHeaderViewHolder.statusTransaksi.setText(produks.get(position).getStatusRating());
            riwayatHeaderViewHolder.namaProduk.setText(produks.get(position).getNama());
            if (produks.get(position).getVarian()!=null) {
                //Varian
                riwayatHeaderViewHolder.varianProduk.setVisibility(View.VISIBLE);
            }
            riwayatHeaderViewHolder.jumlahProduk.setText(produks.get(position).getJumlah().toString());
            riwayatHeaderViewHolder.hargaProduk.setText(formatCurrency.Rupiah(Double.parseDouble(produks.get(position).getHarga())));
            Picasso.get().load(context.getResources().getString(R.string.path)+"/uploads/products/"+produks.get(position).getImage()).into(riwayatHeaderViewHolder.gambarProduct);
            riwayatHeaderViewHolder.btnAksi.setVisibility(View.GONE);
            riwayatHeaderViewHolder.layoutProduk.setOnClickListener(view -> onclickButton.OnClickDetail(produks.get(position)));
            riwayatHeaderViewHolder.layoutPesan.setVisibility(View.GONE);

        } else if (holder instanceof ProdukProductViewHolder){

            final ProdukProductViewHolder riwayatProductViewHolder = (ProdukProductViewHolder) holder;
            riwayatProductViewHolder.layoutToko.setVisibility(View.GONE);
            riwayatProductViewHolder.namaProduk.setText(produks.get(position).getNama());
            if (produks.get(position).getVarian()!=null) {
                //Varian
                riwayatProductViewHolder.varianProduk.setVisibility(View.VISIBLE);
            }
            riwayatProductViewHolder.jumlahProduk.setText(produks.get(position).getJumlah().toString());
            riwayatProductViewHolder.hargaProduk.setText(formatCurrency.Rupiah(Double.parseDouble(produks.get(position).getHarga())));
            Picasso.get().load(context.getResources().getString(R.string.path)+"/uploads/products/"+produks.get(position).getImage()).into(riwayatProductViewHolder.gambarProduct);
            riwayatProductViewHolder.btnAksi.setVisibility(View.GONE);
            riwayatProductViewHolder.layoutProduk.setOnClickListener(view -> onclickButton.OnClickDetail(produks.get(position)));
            riwayatProductViewHolder.layoutPesan.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return produks==null?0:produks.size();
    }
    public interface OnClickButton{
        void OnClickDetail(Produk riwayat);
        void OnclickBtn(Produk riwayat);
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0 || !this.produks.get(position).getIdUser().equals(this.produks.get(position - 1).getIdUser())){
            return VIEWTYPE_GROUP;
        }
        else {
            return VIEWTYPE_PRODUK;
        }
    }

    private class ProdukHeaderViewHolder extends RecyclerView.ViewHolder implements OnClickButton {
        TextView namaToko,tanggalTransaksi,statusTransaksi,namaProduk,varianProduk,jumlahProduk,hargaProduk,pesan,lacakPaket;
        ImageView gambarProduct;
        Button btnAksi;
        LinearLayout layoutPesan,layoutKirim,layoutProduk,layoutToko;

        public ProdukHeaderViewHolder(View view, OnClickButton onclickButton) {
            super(view);
            namaToko=view.findViewById(R.id.NamaToko);
            tanggalTransaksi=view.findViewById(R.id.TanggalTransaksi);
            statusTransaksi=view.findViewById(R.id.StatusTransaksi);
            namaProduk=view.findViewById(R.id.NamaProduct);
            varianProduk=view.findViewById(R.id.VarianProduct);
            jumlahProduk=view.findViewById(R.id.JumlahProduct);
            hargaProduk=view.findViewById(R.id.HargaProduct);
            pesan=view.findViewById(R.id.Pesan);
            lacakPaket=view.findViewById(R.id.LacakPaket);
            gambarProduct=view.findViewById(R.id.GambarProduk);
            btnAksi=view.findViewById(R.id.BtnAksi);
            layoutKirim=view.findViewById(R.id.LayoutDikirim);
            layoutPesan=view.findViewById(R.id.LayoutPesan);
            layoutProduk=view.findViewById(R.id.LayoutRiwayat);
            layoutToko=view.findViewById(R.id.LayoutToko);

        }

        @Override
        public void OnClickDetail(Produk riwayat) {

        }

        @Override
        public void OnclickBtn(Produk riwayat) {

        }
    }

    private class ProdukProductViewHolder extends RecyclerView.ViewHolder {
        TextView namaToko,tanggalTransaksi,statusTransaksi,namaProduk,varianProduk,jumlahProduk,hargaProduk,pesan,lacakPaket;
        ImageView gambarProduct;
        Button btnAksi;
        LinearLayout layoutPesan,layoutKirim,layoutProduk,layoutToko;
        public ProdukProductViewHolder(View view, OnClickButton onclickButton) {
            super(view);
            namaToko=view.findViewById(R.id.NamaToko);
            tanggalTransaksi=view.findViewById(R.id.TanggalTransaksi);
            statusTransaksi=view.findViewById(R.id.StatusTransaksi);
            namaProduk=view.findViewById(R.id.NamaProduct);
            varianProduk=view.findViewById(R.id.VarianProduct);
            jumlahProduk=view.findViewById(R.id.JumlahProduct);
            hargaProduk=view.findViewById(R.id.HargaProduct);
            pesan=view.findViewById(R.id.Pesan);
            lacakPaket=view.findViewById(R.id.LacakPaket);
            gambarProduct=view.findViewById(R.id.GambarProduk);
            btnAksi=view.findViewById(R.id.BtnAksi);
            layoutKirim=view.findViewById(R.id.LayoutDikirim);
            layoutPesan=view.findViewById(R.id.LayoutPesan);
            layoutProduk=view.findViewById(R.id.LayoutRiwayat);
            layoutToko=view.findViewById(R.id.LayoutToko);
        }
    }
}
