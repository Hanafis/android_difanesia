package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Donasi;
import id.difanesia.www.R;

public class AdapterDonasi extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Donasi> donasis;
    OnClickButton onClickButton;
    FormatCurrency formatCurrency= new FormatCurrency();
    public AdapterDonasi(Context context,List<Donasi> donasis, OnClickButton onClickButton){
        this.context=context;
        this.donasis=donasis;
        this.onClickButton=onClickButton;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_donasi, parent, false);
        return new DonasiViewHolder(view, onClickButton);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof AdapterDonasi.DonasiViewHolder) {

            final AdapterDonasi.DonasiViewHolder donasiViewHolder = (AdapterDonasi.DonasiViewHolder) holder;
            if (donasis.get(position).getGambar()!=null){
                if (!donasis.get(position).getGambar().equals("")){
                Picasso.get().load(context.getString(R.string.path)+"/uploads/products/"+donasis.get(position).getGambar()).into(donasiViewHolder.gambarDonasi);
            }}
            donasiViewHolder.judulDonasi.setText(donasis.get(position).getJudulDonasi());

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = simpleDateFormat.parse(donasis.get(position).getTanggalselesai());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat convetDateFormat = new SimpleDateFormat("MMMM d,yyyy");
            assert date != null;
            String Htanggal=convetDateFormat.format(date);
            donasiViewHolder.tanggalDonasi.setText(Htanggal);
            donasiViewHolder.layoutDonasi.setOnClickListener(view -> onClickButton.OnclickDonasi(donasiViewHolder.getAdapterPosition()));


        }
    }

    @Override
    public int getItemCount() {
        return this.donasis==null?0:donasis.size();
    }
    public  interface OnClickButton{
        public void OnclickDonasi(int posisi);
    }

    private static class DonasiViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layoutDonasi;
        ImageView gambarDonasi;
        TextView judulDonasi,tanggalDonasi;
        public DonasiViewHolder(View view, OnClickButton onClickButton) {
            super(view);
                gambarDonasi=view.findViewById(R.id.GambarDonasi);
                judulDonasi=view.findViewById(R.id.JudulDonasi);
                tanggalDonasi=view.findViewById(R.id.TanggalDonasi);
                layoutDonasi=view.findViewById(R.id.LayoutDonasi);
        }
    }
}
