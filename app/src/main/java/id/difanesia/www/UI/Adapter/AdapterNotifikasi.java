package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import id.difanesia.www.Model.Notifikasi;
import id.difanesia.www.R;

public class AdapterNotifikasi extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Notifikasi> notifikasis;
    Context context;
    OnClickButton onClickButton;
    public AdapterNotifikasi(Context context,List<Notifikasi> notifikasis, OnClickButton onClickButton){
        this.context=context;
        this.notifikasis=notifikasis;
        this.onClickButton=onClickButton;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notif,parent,false);
        return new NotifikasiViewHolder(view,onClickButton);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NotifikasiViewHolder){
            final NotifikasiViewHolder notifikasiViewHolder = (NotifikasiViewHolder) holder;
            if (notifikasis.get(position).getDibaca().equals("1")){
                notifikasiViewHolder.layoutNotifikasi.setBackgroundColor(context.getResources().getColor(R.color.transAplikasi));
            }
            notifikasiViewHolder.judulNotifikasi.setText(notifikasis.get(position).getJudul());
            notifikasiViewHolder.notifikasi.setText(notifikasis.get(position).getPesan());
            if (notifikasis.get(position).getGambar()!=null){
                Picasso.get().load(context.getString(R.string.path)  +"/uploads/products/"+ notifikasis.get(position).getGambar()).into(notifikasiViewHolder.gambarNotifikasi);
            }
            notifikasiViewHolder.gambarNotifikasi.setScaleType(ImageView.ScaleType.CENTER);
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = simpleDateFormat.parse(notifikasis.get(position).getCreatedAt());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat convetDateFormat = new SimpleDateFormat("d, MMMM yyyy");
            assert date != null;
            String Htanggal=convetDateFormat.format(date);
            notifikasiViewHolder.tanggal.setText(Htanggal);
            notifikasiViewHolder.layoutNotifikasi.setOnClickListener(view -> onClickButton.OnClickBaca(notifikasiViewHolder.getAdapterPosition(),notifikasis.get(position).getDibaca()));
        }
    }

    @Override
    public int getItemCount() {
        return this.notifikasis==null?0:notifikasis.size();
    }

    private class NotifikasiViewHolder extends RecyclerView.ViewHolder implements OnClickButton {
        TextView notifikasi,tanggal,judulNotifikasi;
        ImageView gambarNotifikasi;
        LinearLayout layoutNotifikasi;
        public NotifikasiViewHolder(View view,OnClickButton onClickButton) {
            super(view);
                judulNotifikasi=view.findViewById(R.id.JudulNotifikasi);
                notifikasi=view.findViewById(R.id.DeskripsiNotifikasi);
                tanggal=view.findViewById(R.id.TanggalNotifikasi);
                gambarNotifikasi=view.findViewById(R.id.GambarNotifikasi);
                layoutNotifikasi=view.findViewById(R.id.LayoutNotifikasi);

        }

        @Override
        public void OnClickBaca(int posisi,String dibaca) {

        }
    }
    @SuppressLint("NotifyDataSetChanged")
    public void Dibaca(int posisi){
        notifikasis.get(posisi).setDibaca("0");
        notifyItemChanged(posisi);
    }
    public interface OnClickButton{
        void OnClickBaca(int posisi,String dibaca);
    }

}
