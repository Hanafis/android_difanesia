package id.difanesia.www.UI.Activity.Donate;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;


import com.facebook.shimmer.ShimmerFrameLayout;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Donate.Detail.DetailDonasiActivity;
import id.difanesia.www.UI.Adapter.AdapterDonasi;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListDonasi extends Fragment {
    View view;
    ApiRequest apiinterface;
    AdapterDonasi adapterDonasi;
    RecyclerView donasiList;
    LoginViewModel loginViewModel;
    ScrollView scrollView;
    ShimmerFrameLayout loading;
    String Token;
    Integer id_users,Limit=12,Start=0;
    LinearLayout layoutNot;

    public ListDonasi() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_tab_donasi, container, false);
        apiinterface = ApiConfig.getClient(getContext()).create(ApiRequest.class);
        donasiList =view.findViewById(R.id.ListDonasi);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        donasiList.setLayoutManager(layoutManager);
        donasiList.setHasFixedSize(true);
        donasiList.setNestedScrollingEnabled(false);
        scrollView=view.findViewById(R.id.ScrollDonasi);
        loading=view.findViewById(R.id.LoadingDonasi);
        layoutNot=view.findViewById(R.id.LayoutNot);
        loading.startShimmer();
        Token();

        return view;
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(getViewLifecycleOwner(), login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiDonasi();


                }
                else {
                    Toast.makeText(getContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void ApiDonasi(){
        Call<ApiResponse> call = apiinterface.DonasiList(Token, Start,Limit);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getDonasi().size()>0){
                        adapterDonasi = new AdapterDonasi(getContext(), response.body().getDonasi(), posisi -> {
                            Intent intent = new Intent(getActivity(), DetailDonasiActivity.class);
                            intent.putExtra("DETAILDONASI",response.body().getDonasi().get(posisi));
                            startActivity(intent);
                        });
                        donasiList.setAdapter(adapterDonasi);
                        adapterDonasi.notifyDataSetChanged();
                        scrollView.setVisibility(View.GONE);
                        donasiList.setVisibility(View.VISIBLE);
                        loading.stopShimmer();
                    }else{
                        scrollView.setVisibility(View.GONE);
                        loading.stopShimmer();
                        layoutNot.setVisibility(View.VISIBLE);
                    }
                }else{
                    scrollView.setVisibility(View.GONE);
                    loading.stopShimmer();
                    layoutNot.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}