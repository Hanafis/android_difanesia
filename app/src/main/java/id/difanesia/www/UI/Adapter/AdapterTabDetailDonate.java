package id.difanesia.www.UI.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;


import id.difanesia.www.Model.Donasi;
import id.difanesia.www.UI.Activity.Donate.Detail.Menu.TabDetailFragment;
import id.difanesia.www.UI.Activity.Donate.Detail.Menu.TabInfoFragment;

public class AdapterTabDetailDonate extends FragmentStateAdapter {
    Donasi donasi;
    public AdapterTabDetailDonate(@NonNull FragmentActivity fragmentActivity, Donasi donasi)
    {
        super(fragmentActivity);
        this.donasi=donasi;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        switch (position){
            case 1:
                return new TabInfoFragment(donasi);
            default:
                return new TabDetailFragment(donasi);
        }
    }
    @Override
    public int getItemCount() {return 3; }
}
