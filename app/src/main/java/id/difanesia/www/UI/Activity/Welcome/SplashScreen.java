package id.difanesia.www.UI.Activity.Welcome;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import id.difanesia.www.MainActivity;
import id.difanesia.www.R;
import id.difanesia.www.ViewModel.LoginViewModel;

@SuppressLint("CustomSplashScreen")
public class SplashScreen extends AppCompatActivity {
    LoginViewModel loginViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(this::cek, 2000);
    }
    public void cek (){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    finish();
                }
            }
            else {
                Intent intent = new Intent(SplashScreen.this, WelcomeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });

    }

}