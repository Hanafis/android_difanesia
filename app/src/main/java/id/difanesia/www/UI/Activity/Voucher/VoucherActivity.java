package id.difanesia.www.UI.Activity.Voucher;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.Voucher;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterVoucher;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VoucherActivity extends AppCompatActivity {
    AdapterVoucher adapterVoucher;
    RecyclerView listVoucher;
    ImageButton btnBack;
    LoginViewModel loginViewModel;
    String Token;
    Integer id_users,Start=0,Limit=2;
    ApiRequest apiinterface;
    EditText kode;
    Button btnSearch;
    Float TotalHarga= (float) 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        btnBack=findViewById(R.id.BtnBack);
        kode=findViewById(R.id.Kode);
        btnSearch=findViewById(R.id.BtnSearch);
        listVoucher =findViewById(R.id.ListVoucher);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        listVoucher.setLayoutManager(layoutManager);
        listVoucher.setHasFixedSize(true);
        listVoucher.setNestedScrollingEnabled(false);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                TotalHarga= null;
            } else {
                TotalHarga= extras.getFloat("JMLHARGA");

            }
        }
        Token();
        Aksi();

    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiVoucher();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void Aksi(){
        btnBack.setOnClickListener(view -> onBackPressed());
        btnSearch.setOnClickListener(view -> ApiSearch());
    }
    public void ApiSearch(){
        Call<ApiResponse> call = apiinterface.VoucherSearch(Token,id_users,kode.getText().toString().trim());
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getVoucher().size()>0){

                        adapterVoucher=new AdapterVoucher(getApplicationContext(), response.body().getVoucher(), new AdapterVoucher.OnClickButton() {
                            @Override
                            public void OnclickVoucher(int posisi) {
                                if (response.body().getVoucher().get(posisi).getIdUser()==null){
                                    ApiClaim(response.body().getVoucher().get(posisi),response.body().getVoucher().get(posisi).getIdKupon());
                                }
                                else{
                                    Intent intent = new Intent();
                                    intent.putExtra("VOUCHER",response.body().getVoucher().get(posisi));
                                    setResult(Activity.RESULT_OK,intent);
                                    finish();
                                }
                            }

                            @Override
                            public void OnClickDetail(int posisi) {
                                Intent intent = new Intent(VoucherActivity.this, DetailVoucherActivity.class);
                                intent.putExtra("DETAILVOUCHER",response.body().getVoucher().get(posisi));
                                startActivity(intent);
                            }
                        });
                        listVoucher.setAdapter(adapterVoucher);
                        adapterVoucher.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void ApiVoucher(){
        Call<ApiResponse> call = apiinterface.VoucherList(Token,id_users);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getVoucher().size()>0){

                        adapterVoucher=new AdapterVoucher(getApplicationContext(), response.body().getVoucher(), new AdapterVoucher.OnClickButton() {
                            @Override
                            public void OnclickVoucher(int posisi) {
                                if (response.body().getVoucher().get(posisi).getIdUser()==null){
                                    ApiClaim(response.body().getVoucher().get(posisi),response.body().getVoucher().get(posisi).getIdKupon());
                                }else{
                                    Intent intent = new Intent();
                                    intent.putExtra("VOUCHER",response.body().getVoucher().get(posisi));
                                    setResult(Activity.RESULT_OK,intent);
                                    finish();
                                }
                            }

                            @Override
                            public void OnClickDetail(int posisi) {
                                Intent intent = new Intent(VoucherActivity.this, DetailVoucherActivity.class);
                                intent.putExtra("DETAILVOUCHER",response.body().getVoucher().get(posisi));
                                startActivity(intent);
                            }
                        });
                        listVoucher.setAdapter(adapterVoucher);
                        adapterVoucher.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void ApiClaim(Voucher voucher,Integer id_kupon){
        Call<ApiResponse> call = apiinterface.VoucherPilih(Token,id_users,id_kupon);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()){
                        ApiSearch();
                    }else{
                        if (response.body().getMessage()!=null) {
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
}