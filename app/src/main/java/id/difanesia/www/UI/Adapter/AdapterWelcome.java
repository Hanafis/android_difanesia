package id.difanesia.www.UI.Adapter;





import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;



import com.squareup.picasso.Picasso;

import java.util.List;

import id.difanesia.www.Model.Welcome;
import id.difanesia.www.R;

public class AdapterWelcome extends RecyclerView.Adapter<AdapterWelcome.ViewHolder>{
    Context context;
    List<Welcome> itemds;
    ViewPager2 viewPager;

    public AdapterWelcome(Context context, List<Welcome> mySliderLists, ViewPager2 viewPager){
        super();
        this.context = context;
        this.itemds = mySliderLists;
        this.viewPager = viewPager;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_welcome, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder  holder, int position) {
        Picasso.get().load(itemds.get(position).getGambar()).into(holder.image);
//        holder.image.setScaleType(ImageView.ScaleType.FIT_XY);
        holder.judul.setText(itemds.get(position).getJudul());
        holder.isi.setText(itemds.get(position).getIsi());

    }

    @Override
    public int getItemCount() {
        return itemds.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView judul,isi;
        ImageView image;
        LinearLayout relativeLayout;
        public ViewHolder(@NonNull  View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.ImageWelcome);
            judul=itemView.findViewById(R.id.JudulWelcome);
            isi=itemView.findViewById(R.id.IsiWelcome);
            relativeLayout = itemView.findViewById(R.id.container);


        }
    }
    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Welcome> help, ViewPager2 viewPager) {
        this.itemds = help;
        this.viewPager=viewPager;
        this.itemds.add(0, null);
        notifyDataSetChanged();
    }

}

