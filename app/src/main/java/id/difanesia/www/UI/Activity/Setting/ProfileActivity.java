package id.difanesia.www.UI.Activity.Setting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import id.difanesia.www.ViewModel.LoginViewModel;
import id.difanesia.www.ViewModel.UserViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    CircleImageView gambarUser;
    Spinner jenisKelamin;
    SimpleDateFormat dateFormatter,dateKirim;
    DatePickerDialog datePickerDialog;
    EditText namaLengkap,tanggalLahir,noTlpn;
    UserViewModel userViewModel;
    String Token;
    String JenisKelamin,TanggalLahir;
    LoginViewModel loginViewModel;
    Button btnUpdate;
    Integer id_users;
    ApiRequest apiinterface;
    Uri imagepath;
    ImageButton btnBack;
    RequestBody bodyNamaLengkap;
    RequestBody bodyJenisKelamin;
    RequestBody bodyTanggalLahir;
    RequestBody bodyNoTlpn;
    ArrayList<String> jenisKelamins;
    Date date = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        gambarUser=findViewById(R.id.GambarUser);
        jenisKelamin=findViewById(R.id.JenisKelamin);
        namaLengkap=findViewById(R.id.NamaLengkap);
        tanggalLahir=findViewById(R.id.TanggalLahir);
        noTlpn=findViewById(R.id.NoTlpn);
        btnUpdate=findViewById(R.id.BtnUpdate);
        jenisKelamins = new ArrayList<>();
        Locale localeID = new Locale("in", "ID");
        dateFormatter = new SimpleDateFormat("d MMMM yyyy", localeID);
        dateKirim = new SimpleDateFormat("yyyy-MM-dd", localeID);
        btnBack=findViewById(R.id.BtnBack);
        jenisKelamins.add("Laki-Laki");
        jenisKelamins.add("Perempuan");
        ArrayAdapter adapter = ArrayAdapter.createFromResource(
                this,
                R.array.jenis_kelamin,
                R.layout.selected_jeniskelamin
        );
        adapter.setDropDownViewResource(R.layout.jeniskelamin_dropdown);
        jenisKelamin.setAdapter(adapter);
        Token();
        Aksi();

    }
    private void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    getUser();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private void getUser(){

        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        userViewModel.getApiResponseLiveData().observe(this, users -> {
            if(users!=null) {
                if (users.getName() != null) {
                    namaLengkap.setText(users.getName());
                    if (users.getJenisKelamin() != null) {
                        if (users.getJenisKelamin().equals("Perempuan")) {
                            jenisKelamin.setSelection(1);
                        }
                    }

                    TanggalLahir=users.getTanggalLahir();
                    noTlpn.setText(users.getNoTlpn());
                }
                if(users.getImage()!=null) {
                    Picasso.get().load(getString(R.string.path)  +"/uploads/products/"+ users.getImage()).into(gambarUser);
                }
                if (TanggalLahir!=null){
                    try {
                        date = dateKirim.parse(TanggalLahir);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (date!=null) {
                        tanggalLahir.setText(dateFormatter.format(date));
                    }
                }
            }
        });

    }
    private void Aksi(){
        btnBack.setOnClickListener(v -> onBackPressed());
        btnUpdate.setOnClickListener(v -> {

            if (namaLengkap.getText().toString().matches("")) {
                namaLengkap.setError("Kolom Nama Tidak Boleh Kosong");
                namaLengkap.requestFocus();
                return;
            }
            if (JenisKelamin.matches("")) {
                Toast.makeText(getApplicationContext(),"Kolom Jenis Kelamin Tidak Boleh Kosong",Toast.LENGTH_LONG).show();
                return;
            }
            if (tanggalLahir.getText().toString().matches("")) {
                tanggalLahir.setError("Kolom Tanggal Tidak Boleh Kosong");
                tanggalLahir.requestFocus();
                return;
            }
            if (noTlpn.getText().toString().matches("")) {
                noTlpn.setError("Kolom Nomor Handphone Tidak Boleh Kosong");
                noTlpn.requestFocus();
                return;
            }


            ApiUpdate();

        });
        tanggalLahir.setOnClickListener(v -> showDateDialog());
        jenisKelamin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                JenisKelamin = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        gambarUser.setOnClickListener(v -> ImagePicker.Companion.with(ProfileActivity.this)
                .crop()
                .compress(1024)
                .maxResultSize(1080,1080)
                .start());

    }
    private void ApiUpdate() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Ganti Profile...");
        progressDialog.show();
        try {
            date = dateFormatter.parse(tanggalLahir.getText().toString().trim());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        bodyNamaLengkap = RequestBody.create(MediaType.parse("text/plain"), namaLengkap.getText().toString().trim());
        bodyJenisKelamin = RequestBody.create(MediaType.parse("text/plain"), JenisKelamin);
        bodyTanggalLahir = RequestBody.create(MediaType.parse("text/plain"), dateKirim.format(date));
        bodyNoTlpn = RequestBody.create(MediaType.parse("text/plain"), noTlpn.getText().toString().trim());
        if (imagepath!=null){
            if (getRealPathFromURI(imagepath) != null) {
                File file = new File(getRealPathFromURI(imagepath)); //Get File
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);

                Call<ApiResponse> call = apiinterface.UpdateUser(Token, id_users, bodyNamaLengkap, bodyJenisKelamin, bodyTanggalLahir, bodyNoTlpn, body);
                call.enqueue(new Callback<ApiResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                        if (response.body() != null) {
                            if (response.body().getMessage() != null) {
                                ApiUser();
                                progressDialog.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();

                    }
                });
            }
        }
        else {
            Call<ApiResponse> call = apiinterface.UpdateUser(Token,id_users,bodyNamaLengkap,bodyJenisKelamin,bodyTanggalLahir,bodyNoTlpn);
            call.enqueue(new Callback<ApiResponse>() {
                @Override
                public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                    if(response.body()!=null){
                        if (response.body().getMessage()!=null){
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),"Berhasil Di Update",Toast.LENGTH_LONG).show();
                            userViewModel.updateNoImageData(
                                    namaLengkap.getText().toString().trim(),
                                    JenisKelamin,
                                    dateKirim.format(date),
                                    noTlpn.getText().toString().trim(),
                                    id_users);

                        }
                        else{
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Gagal terhubung Ke Server", Toast.LENGTH_LONG).show();
                        }
                    }
                    else{
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal terhubung Ke Server", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Gagal terhubung Ke Server", Toast.LENGTH_LONG).show();

                }
            });
        }
    }
    private void showDateDialog(){

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);

            tanggalLahir.setText(dateFormatter.format(newDate.getTime()));

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data.getData()!=null) {
            imagepath = data.getData();
            gambarUser.setImageURI(imagepath);
        }

    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    public void ApiUser(){

        Call<ApiResponse> call = apiinterface.userdetail(Token,id_users);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getUser()!=null){
                        String gambar=null;
                        if (response.body().getUser().getImage()!=null){
                            gambar=response.body().getUser().getImage();
                        }
                        Toast.makeText(getApplicationContext(), "Berhasil Di Update", Toast.LENGTH_LONG).show();
                        userViewModel.updateData(
                                namaLengkap.getText().toString().trim(),
                                JenisKelamin,
                                dateKirim.format(date),
                                noTlpn.getText().toString().trim(),
                                gambar,
                                id_users);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }

}