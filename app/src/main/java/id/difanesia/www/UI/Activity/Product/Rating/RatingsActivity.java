package id.difanesia.www.UI.Activity.Product.Rating;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterTabRating;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatingsActivity extends AppCompatActivity {
    AdapterTabRating adapter;
    ViewPager2 viewPager2;
    TabLayout tabLayout;
    LinearLayout btnBack;
    ApiRequest apiinterface;
    LoginViewModel loginViewModel;
    String Token;
    Integer id_users;
    Integer id_product=0;
    private final int[] tabIcons = {
            R.drawable.start05,
            R.drawable.star04,
            R.drawable.star03,
            R.drawable.star02,
            R.drawable.star01,
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);
        viewPager2=findViewById(R.id.pager);
        tabLayout=findViewById(R.id.LayoutTabs);
        SharedPreferences sharedPref = getSharedPreferences("DataProduct", Context.MODE_PRIVATE);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        if (sharedPref.contains("PRODUCTID")) {
            id_product = sharedPref.getInt("PRODUCTID", 0);
            adapter=new AdapterTabRating(this,id_product);
        }
        tabLayout.addTab(tabLayout.newTab().setText("(0)"));
        tabLayout.addTab(tabLayout.newTab().setText("(0)"));
        tabLayout.addTab(tabLayout.newTab().setText("(0)"));
        tabLayout.addTab(tabLayout.newTab().setText("(0)"));
        tabLayout.addTab(tabLayout.newTab().setText("(0)"));
        Objects.requireNonNull(tabLayout.getTabAt(0)).setIcon(tabIcons[0]);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setIcon(tabIcons[1]);
        Objects.requireNonNull(tabLayout.getTabAt(2)).setIcon(tabIcons[2]);
        Objects.requireNonNull(tabLayout.getTabAt(3)).setIcon(tabIcons[3]);
        Objects.requireNonNull(tabLayout.getTabAt(4)).setIcon(tabIcons[4]);


        Token();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });
        btnBack=findViewById(R.id.BtnBack);
        btnBack.setOnClickListener(view -> onBackPressed());
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiRating();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void ApiRating(){

        Call<ApiResponse> call = apiinterface.RatingData(Token,id_product);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getRatingBintang()!=null){

                        if (response.body().getRatingBintang().get(0).getLima()!=null){
                            Objects.requireNonNull(tabLayout.getTabAt(0)).setText("("+response.body().getRatingBintang().get(0).getLima().toString()+")");
                        }
                        if (response.body().getRatingBintang().get(0).getEmpat()!=null){
                            Objects.requireNonNull(tabLayout.getTabAt(1)).setText("("+response.body().getRatingBintang().get(0).getEmpat().toString()+")");
                        }
                        if (response.body().getRatingBintang().get(0).getTiga()!=null){
                            Objects.requireNonNull(tabLayout.getTabAt(2)).setText("("+response.body().getRatingBintang().get(0).getTiga().toString()+")");
                        }
                        if (response.body().getRatingBintang().get(0).getDua()!=null){
                            Objects.requireNonNull(tabLayout.getTabAt(3)).setText("("+response.body().getRatingBintang().get(0).getDua().toString()+")");
                        }
                        if (response.body().getRatingBintang().get(0).getSatu()!=null){
                            Objects.requireNonNull(tabLayout.getTabAt(4)).setText("("+response.body().getRatingBintang().get(0).getSatu().toString()+")");
                        }

                    }
                    adapter.addData(response.body().getB1(),response.body().getB2(),response.body().getB3(),response.body().getB4(),response.body().getB5());
                    adapter.notifyDataSetChanged();
                    viewPager2.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                Log.e("getFS ", t.toString());
            }
        });
    }
}