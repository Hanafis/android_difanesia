package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;


import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;


public class AdapterCheckout extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Produk> Produk;
    FormatCurrency formatCurrency = new FormatCurrency();
    int VIEWTYPE_GROUP = 0;
    int VIEWTYPE_PRODUK = 1;



    public AdapterCheckout(Context context, List<Produk> produk) {
        this.context = context;
        this.Produk = produk;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_header_cart, parent, false);
            return new GropViewHolder(view);
        }
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_cart, parent, false);
        return new ProdukViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if(holder instanceof GropViewHolder) {

            final AdapterCheckout.GropViewHolder gropViewHolder = (AdapterCheckout.GropViewHolder) holder;
            gropViewHolder.namaToko.setText(Produk.get(position).getToko());
            gropViewHolder.kotaToko.setVisibility(View.GONE);
            gropViewHolder.layoutJml.setVisibility(View.GONE);
            gropViewHolder.jmlBarang.setVisibility(View.VISIBLE);
            gropViewHolder.checkGroup.setVisibility(View.GONE);
            gropViewHolder.checkProduct.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0,0,0,0);
            gropViewHolder.namaToko.setLayoutParams(params);
            if (Produk.get(position).getIdVarian()!=null){
                if (Produk.get(position).getVarianStock()!=null) {
                    float harga=Float.parseFloat(Produk.get(position).getVarianHarga())*Float.parseFloat(Produk.get(position).getJml().toString());
                    gropViewHolder.namaProduct.setText(Produk.get(position).getNama());
                    gropViewHolder.hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(String.valueOf(harga))));
                    gropViewHolder.varianProduct.setText(Produk.get(position).getVarian());

                    if (Produk.get(position).getVarianImage() != null) {
                        Picasso.get().load(context.getString(R.string.path)+"/uploads/products/"+Produk.get(position).getVarianImage()).into(gropViewHolder.gambarProduct);
                    }
                }else{
                    gropViewHolder.layoutListProduct.setEnabled(false);
                    gropViewHolder.layoutListProduct.setClickable(false);
                }
            }
            else{
                if (Produk.get(position).getStock()!=null) {
                    float harga=Float.parseFloat(Produk.get(position).getHarga())*Float.parseFloat(Produk.get(position).getJml().toString());
                    gropViewHolder.namaProduct.setText(Produk.get(position).getNama());
                    gropViewHolder.hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(String.valueOf(harga))));
                    gropViewHolder.varianProduct.setVisibility(View.GONE);
                }
                else{
                    gropViewHolder.layoutListProduct.setEnabled(false);
                    gropViewHolder.layoutListProduct.setClickable(false);
                }
                Picasso.get().load(context.getString(R.string.path)+"/uploads/products/"+Produk.get(position).getImage()).into(gropViewHolder.gambarProduct);
            }
            gropViewHolder.jmlBarang.setText(Produk.get(position).getJml().toString()+" x");
            gropViewHolder.layoutBtn.setVisibility(View.GONE);


        }
        if (holder instanceof ProdukViewHolder){
            final AdapterCheckout.ProdukViewHolder produkViewHolder = (AdapterCheckout.ProdukViewHolder) holder;
            produkViewHolder.layoutJml.setVisibility(View.GONE);
            produkViewHolder.jmlBarang.setVisibility(View.VISIBLE);
            produkViewHolder.checkProduct.setVisibility(View.GONE);
            if (Produk.get(position).getIdVarian()!=null){
                if (Produk.get(position).getVarianStock()!=null) {
                    float harga=Float.parseFloat(Produk.get(position).getVarianHarga())*Float.parseFloat(Produk.get(position).getJml().toString());
                    produkViewHolder.namaProduct.setText(Produk.get(position).getNama());
                    produkViewHolder.hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(String.valueOf(harga))));
                    produkViewHolder.varianProduct.setText(Produk.get(position).getVarian());

                    if (Produk.get(position).getVarianImage() != null) {
                        Picasso.get().load(context.getString(R.string.path)+"/uploads/products/"+ Produk.get(position).getVarianImage()).into(produkViewHolder.gambarProduct);
                    }
                }else{
                    produkViewHolder.layoutListProduct.setEnabled(false);
                    produkViewHolder.layoutListProduct.setClickable(false);
                }
            }
            else{
                if (Produk.get(position).getStock()!=null) {
                    float harga=Float.parseFloat(Produk.get(position).getHarga())*Float.parseFloat(Produk.get(position).getJml().toString());
                    produkViewHolder.namaProduct.setText(Produk.get(position).getNama());
                    produkViewHolder.hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(String.valueOf(harga))));
                    produkViewHolder.varianProduct.setVisibility(View.GONE);
                }
                else{
                    produkViewHolder.layoutListProduct.setEnabled(false);
                    produkViewHolder.layoutListProduct.setClickable(false);
                }
                Picasso.get().load(context.getString(R.string.path)+"/uploads/products/"+"storage/upload/"+ Produk.get(position).getImage()).into(produkViewHolder.gambarProduct);
            }
            produkViewHolder.jmlBarang.setText(Produk.get(position).getJml().toString()+" x");
            produkViewHolder.layoutBtn.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0 || !this.Produk.get(position).getIdToko().equals(this.Produk.get(position - 1).getIdToko())){
            return VIEWTYPE_GROUP;
        }
        else {
            return VIEWTYPE_PRODUK;
        }


    }

    @Override
    public int getItemCount() {return Produk == null ? 0 : Produk.size();
    }

    private static class GropViewHolder extends RecyclerView.ViewHolder{

        TextView namaToko, kotaToko,hargaProduct,namaProduct,varianProduct,jmlBarang;
        CheckBox checkGroup,checkProduct;
        ImageView gambarProduct;
        View view;
        LinearLayout layoutListProduct,layoutJml,layoutBtn;

        public GropViewHolder(@NonNull View itemView) {
            super(itemView);
            namaToko = itemView.findViewById(R.id.NamaToko);
            kotaToko = itemView.findViewById(R.id.KotaToko);
            hargaProduct=itemView.findViewById(R.id.HargaProduct);
            namaProduct=itemView.findViewById(R.id.NamaProduct);
            gambarProduct=itemView.findViewById(R.id.GambarProduct);
            view=itemView.findViewById(R.id.BatasHeader);
            layoutListProduct=itemView.findViewById(R.id.LayoutListProduct);
            checkGroup=itemView.findViewById(R.id.CheckGroup);
            checkProduct=itemView.findViewById(R.id.CheckProduct);
            varianProduct=itemView.findViewById(R.id.VarianProduct);
            jmlBarang=itemView.findViewById(R.id.JmlBarang);
            layoutJml=itemView.findViewById(R.id.LayoutJml);
            layoutBtn=itemView.findViewById(R.id.LayoutBtn);

        }






    }

    private static class ProdukViewHolder extends RecyclerView.ViewHolder{
        TextView hargaProduct,namaProduct,varianProduct,jmlBarang;
        CheckBox checkProduct;
        ImageView gambarProduct;
        LinearLayout layoutListProduct,layoutJml,layoutBtn;

        public ProdukViewHolder(@NonNull View itemView) {
            super(itemView);
            checkProduct = itemView.findViewById(R.id.CheckProduct);
            namaProduct = itemView.findViewById(R.id.NamaProduct);
            varianProduct = itemView.findViewById(R.id.VarianProduct);
            hargaProduct = itemView.findViewById(R.id.HargaProduct);
            gambarProduct = itemView.findViewById(R.id.GambarProduct);
            layoutListProduct = itemView.findViewById(R.id.LayoutListProduct);
            jmlBarang = itemView.findViewById(R.id.JmlBarang);
            layoutJml = itemView.findViewById(R.id.LayoutJml);
            layoutBtn = itemView.findViewById(R.id.LayoutBtn);

        }
    }







}
