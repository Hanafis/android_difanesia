package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.difanesia.www.Model.ListAlamat;
import id.difanesia.www.R;

public class AdapterListAddress extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<ListAlamat> listAlamats;
    Context context;
    OnClickButton onClickButton;
    public AdapterListAddress(Context context, List<ListAlamat> listAlamats, OnClickButton onClickButton){
        this.listAlamats=listAlamats;
        this.context=context;
        this.onClickButton=onClickButton;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_address,parent,false);
        return new ViewHolderAddress(view,onClickButton);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof ViewHolderAddress){
                final ViewHolderAddress viewHolderAddress = (ViewHolderAddress) holder;
                if (listAlamats.get(position).getProvinsi()!=null) {
                    viewHolderAddress.alamat.setText(listAlamats.get(position).getProvinsi());
                }
                if (listAlamats.get(position).getKabupaten()!=null) {
                    viewHolderAddress.alamat.setText(listAlamats.get(position).getKabupaten());
                }
                if (listAlamats.get(position).getKecamatan()!=null) {
                    viewHolderAddress.alamat.setText(listAlamats.get(position).getKecamatan());
                }
                if (listAlamats.get(position).getKelurahan()!=null) {
                    viewHolderAddress.alamat.setText(listAlamats.get(position).getKelurahan());
                }
                if (listAlamats.get(position).getKodePos()!=null) {
                    viewHolderAddress.alamat.setText(listAlamats.get(position).getKodePos().toString());
                }
                viewHolderAddress.layoutAlamat.setOnClickListener(view -> onClickButton.OnClickPilih(position));

            }
    }

    @Override
    public int getItemCount() {
        return listAlamats==null?0:listAlamats.size();
    }
    public interface OnClickButton{
        void OnClickPilih(int posisi);
    }

    private class ViewHolderAddress extends RecyclerView.ViewHolder {
        TextView alamat;
        LinearLayout layoutAlamat;
        public ViewHolderAddress(View view, OnClickButton onClickButton) {
            super(view);
            alamat=view.findViewById(R.id.TextAlamat);
            layoutAlamat=view.findViewById(R.id.LayoutAlamat);
        }
    }
}
