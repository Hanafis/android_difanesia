package id.difanesia.www.UI.Adapter;





import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;





import java.util.List;

import id.difanesia.www.Model.Search;
import id.difanesia.www.R;


public class AdapterSearch extends RecyclerView.Adapter<AdapterSearch.ViewHolder>{
    Context context;
    List<Search> itemds;
    OnBtnClick monBtnClick;

    public AdapterSearch(Context context, List<Search> itemds, AdapterSearch.OnBtnClick onBtnClick){
        super();
        this.monBtnClick=onBtnClick;
        this.context = context;
        this.itemds = itemds;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder  holder, @SuppressLint("RecyclerView") int position) {

    //aksi
            holder.keyword.setText(itemds.get(position).getKeyword());

            holder.btnKeyword.setOnClickListener(view -> monBtnClick.OnClickKeyword(holder.getAdapterPosition()));


    }

    @Override
    public int getItemCount() {
        return itemds==null ? 0:itemds.size();
    }

    public void clear() {
        int size = itemds.size();
        itemds.clear();
        notifyItemRangeRemoved(0, size);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements OnBtnClick{
        LinearLayout btnKeyword;
        TextView keyword;
        public ViewHolder(@NonNull  View itemView) {
            super(itemView);
            btnKeyword=itemView.findViewById(R.id.BtnKeyword);
            keyword=itemView.findViewById(R.id.KeywordTextView);




        }

        @Override
        public void OnClickKeyword(int posisi) {

        }
    }
    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Search> slider) {
        this.itemds = slider;
        this.itemds.add(0, null);
        notifyDataSetChanged();
    }
    public  interface OnBtnClick{

        void OnClickKeyword(int posisi);

    }

}


