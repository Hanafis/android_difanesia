package id.difanesia.www.UI.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.difanesia.www.Model.Chat;
import id.difanesia.www.R;

public class AdapterChat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Chat> chats;
    final int INCOME=0;
    final int OUTCOME=1;
    public AdapterChat(Context context, List<Chat> chats){
        this.context=context;
        this.chats=chats;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType==INCOME){
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_income,parent,false);
            return new InComeViewHolder(view);
        }else{
            view=LayoutInflater.from(parent.getContext()).inflate(R.layout.row_outcom,parent,false);
            return new OutComeViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof InComeViewHolder){
                final InComeViewHolder inComeViewHolder= (InComeViewHolder) holder;
            }
            else if (holder instanceof OutComeViewHolder){
                final OutComeViewHolder outComeViewHolder= (OutComeViewHolder) holder;
            }
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    private static class InComeViewHolder extends RecyclerView.ViewHolder {
        public InComeViewHolder(View view) {
            super(view);
        }
    }

    private static class OutComeViewHolder extends RecyclerView.ViewHolder {
        public OutComeViewHolder(View view) {
            super(view);
        }
    }
}
