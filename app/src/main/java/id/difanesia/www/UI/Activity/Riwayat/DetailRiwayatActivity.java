package id.difanesia.www.UI.Activity.Riwayat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Alamat;
import id.difanesia.www.Model.DetailTransaksi;
import id.difanesia.www.Model.PaymentInfromation;
import id.difanesia.www.Model.Pengiriman;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.Model.Riwayat;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Payment.DetailPaymentActivity;
import id.difanesia.www.UI.Activity.Product.CartActivity;
import id.difanesia.www.UI.Activity.Product.DetailProductActivity;
import id.difanesia.www.UI.Activity.Product.Rating.NewRatingProduct;
import id.difanesia.www.UI.Adapter.AdapterProductRiwayat;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public   class DetailRiwayatActivity extends AppCompatActivity {
    TextView statusPesanan,jenisPengiriman,resi,gedung,namaUser,nomorUser,alamatUser,nomorPesanan,totalPesanan,tanggalPemesanan,namaBank,metode;
    Button btnAksi,btnPengembalian,btnOngkir;
    ImageButton btnBack,btnCopy;
    ImageView gmbrBank;
    RecyclerView listRiwayat;
    ApiRequest apiinterface;
    String Token;
    Integer id_users;
    LoginViewModel loginViewModel;
    AdapterProductRiwayat adapterProductRiwayat;
    FormatCurrency formatCurrency = new FormatCurrency();
    LinearLayout layoutResi;
    Riwayat riwayat;
    private ClipboardManager myClipboard;
    private ClipData myClip;
    List<Produk> produks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_riwayat);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        statusPesanan=findViewById(R.id.StatusPesanan);
        jenisPengiriman=findViewById(R.id.JenisPengiriman);
        resi=findViewById(R.id.NoResi);
        gedung=findViewById(R.id.Gedung);
        namaUser=findViewById(R.id.NamaUser);
        nomorUser=findViewById(R.id.NomorUser);
        alamatUser=findViewById(R.id.AlamatUser);
        nomorPesanan=findViewById(R.id.NoPesanan);
        totalPesanan=findViewById(R.id.TotalPesnan);
        btnCopy=findViewById(R.id.BtnCopyVa);
        tanggalPemesanan=findViewById(R.id.WaktuPembayaran);
        btnAksi=findViewById(R.id.BtnAksi);
        btnPengembalian=findViewById(R.id.BtnPengembalian);
        btnOngkir=findViewById(R.id.BtnOngkir);
        layoutResi=findViewById(R.id.LayoutResi);
        btnBack=findViewById(R.id.BtnBack);
        listRiwayat=findViewById(R.id.ListRiwayat);
        gmbrBank=findViewById(R.id.gmbrBank);
        namaBank=findViewById(R.id.NamaBank);
        metode=findViewById(R.id.MetodePembayaran);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false);
        listRiwayat.setLayoutManager(layoutManager);
        listRiwayat.setHasFixedSize(true);
        listRiwayat.setNestedScrollingEnabled(false);
        setup(savedInstanceState);
        Aksi();


    }
    public void Aksi(){
        btnBack.setOnClickListener(view->{onBackPressed();});
    }
    public void setup(Bundle savedInstanceState){
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                riwayat= null;
            } else {
                riwayat= (Riwayat) extras.getSerializable("DETAILRIWAYAT");
            }
        }
        else {
            riwayat= (Riwayat) savedInstanceState.getSerializable("DETAILRIWAYAT");
        }
        if (riwayat!=null) {
            Data();
            Token();
        }
    }
    @SuppressLint("SetTextI18n")
    public void Data(){
        statusPesanan.setText(riwayat.getStatus());
        nomorPesanan.setText(riwayat.getKodeTransaksi());
        totalPesanan.setText(formatCurrency.Rupiah(Double.parseDouble(riwayat.getJumlahBayar())));
        tanggalPemesanan.setText(riwayat.getCreatedAt());
        switch (riwayat.getStatus()){
            case "Belum Dibayar":
                statusPesanan.setText("Pesanan Berhasil Dibuat Pada Tanggal "+riwayat.getCreatedAt()+" Silahkan Melakukan Pembayaran Sebelum ");
                break;
            case "Sedang Dikemas":
                statusPesanan.setText("Produk Akan Dikirim Sebelum Tanggal "+riwayat.getCreatedAt());
                break;
            case "Sedang Dikirim":
                statusPesanan.setText("Konfirmasi Produk Sebelum Tanggal "+riwayat.getCreatedAt());
                break;
            case "Telah DIterima":
                statusPesanan.setText("Pesanan Telah Diterima Pada Tanggal "+riwayat.getUpdatedAt()+"Silahkan Melakukan Konfirmasi Produk Sebelum");
                break;
            case "Dibatalkan":
                statusPesanan.setText(riwayat.getPesan());
                break;
            case "Pengembalian":
                statusPesanan.setText(riwayat.getPesan());
                break;
        }
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    ApiDetail();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void ApiDetail(){
        Call<ApiResponse> call = apiinterface.RiwayatDetail(Token,riwayat.getIdTransaksi());
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if(response.body()!=null) {
                    Log.e("d",response.body().getDetailTransaksi().getProduk().size()+"");
                    DetailTransaksi detailTransaksi= response.body().getDetailTransaksi();
                    if ( detailTransaksi!= null) {

                        if (detailTransaksi.getProduk().size() > 0) {
                            produks=detailTransaksi.getProduk();
                            adapterProductRiwayat = new AdapterProductRiwayat(getApplicationContext(), detailTransaksi.getProduk(), new AdapterProductRiwayat.OnClickButton() {
                                @Override
                                public void OnClickDetail(Produk riwayat) {
                                    Intent intent = new Intent(DetailRiwayatActivity.this, DetailProductActivity.class);
                                    intent.putExtra("DETAILPRODUCT", riwayat);

                                    startActivity(intent);
                                }

                                @Override
                                public void OnclickBtn(Produk riwayat) {

                                }
                            });
                            listRiwayat.setAdapter(adapterProductRiwayat);
                            adapterProductRiwayat.notifyDataSetChanged();
                        }
                        if (detailTransaksi.getStatus().equals("Belum Dibayar")){
                            btnAksi.setText("Info Rek Bank");
                            btnAksi.setOnClickListener(view->{
                                Intent intent = new Intent(DetailRiwayatActivity.this, DetailPaymentActivity.class);
                                intent.putExtra("TAGIHAN",detailTransaksi.getSubTotalTransaksi());
                                intent.putExtra("PaymentInformation",detailTransaksi.getPaymentInformation());
                                startActivity(intent);
                            });
                        }else if (detailTransaksi.getStatus().equals("Sedang Dikemas") ){
                            btnPengembalian.setVisibility(View.GONE);
                            btnAksi.setVisibility(View.GONE);

                        }else if(detailTransaksi.getStatus().equals("Sedang Dikirim")){
                            btnAksi.setText("Pesanan Diterima");
                            btnAksi.setOnClickListener(view->{
                                ApiPesananDiterima();
                            });
                            btnPengembalian.setVisibility(View.VISIBLE);
                            btnPengembalian.setText("Ajukan Pengembalian");
                        } else if(detailTransaksi.getStatus().equals("Dibatalkan")){
                            btnAksi.setText("Prosess Pengembalian");
                            btnAksi.setOnClickListener(view->{
                                Intent intent = new Intent(DetailRiwayatActivity.this, ProgressReturn.class);
                                startActivity(intent);
                            });
                        }
                        else if(detailTransaksi.getStatus().equals("Telah Diterima")){
                            AksiPesananDiterima(false);
                        }

                        Alamat alamat =response.body().getDetailTransaksi().getAlamat();
                        if (alamat!=null) {
                            gedung.setText(alamat.getGedung());
                            namaUser.setText(alamat.getNamaLengkap());
                            nomorUser.setText(alamat.getNoTlpn());
                            alamatUser.setText(alamat.getJalan());

                        }
                        //Ongkir
                        Pengiriman pengiriman = response.body().getDetailTransaksi().getPengiriman();
                        if (pengiriman!=null) {

                            jenisPengiriman.setText(pengiriman.getNamaEkspedisi());
                            if (pengiriman.getResi()!=null){
                                if (!pengiriman.getResi().equals("")) {
                                    layoutResi.setVisibility(View.VISIBLE);
                                    resi.setText(pengiriman.getResi());
                                    btnCopy.setOnClickListener(view->{
                                        myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                        String text;
                                        text = resi.getText().toString();

                                        myClip = ClipData.newPlainText("text", text);
                                        myClipboard.setPrimaryClip(myClip);

                                        Toast.makeText(getApplicationContext(), "Resi Berhasil Di Copy",Toast.LENGTH_SHORT).show();
                                    });
                                }else{
                                    btnOngkir.setVisibility(View.GONE);
                                }
                            }
                        }

                        PaymentInfromation paymentInfromation = response.body().getDetailTransaksi().getPaymentInformation();
                        if (paymentInfromation!=null){
                            metode.setText("VA");
                            switch (paymentInfromation.getBankId()){
                                case "009":
                                    if (paymentInfromation.getManual()){
                                        metode.setText("Manual");
                                    }
                                    gmbrBank.setImageDrawable(getResources().getDrawable(R.drawable.bni));
                                    break;
                                case "002":
                                    gmbrBank.setImageDrawable(getResources().getDrawable(R.drawable.bri));
                                    break;
                                case "014":
                                    gmbrBank.setImageDrawable(getResources().getDrawable(R.drawable.bca));
                                    break;
                                case "008":
                                    gmbrBank.setImageDrawable(getResources().getDrawable(R.drawable.mandiri));
                                    break;
                            }
                            namaBank.setText(paymentInfromation.getBank());

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                Log.e("data",t.toString());
            }
        });
    }
    private void ApiPesananDiterima() {
        Call<ApiResponse> call = apiinterface.PesananDiterima(Token,riwayat.getIdTransaksi());
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()){
                        AksiPesananDiterima(false);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    private void AksiPesananDiterima(Boolean Batal) {
        btnAksi.setText("Beli Lagi");
        btnAksi.setOnClickListener(view->{
            for (int i=0; i<produks.size(); i++){
                Keranjang(produks.get(i).getIdProduct(),produks.get(i).getJumlah());
            }
            Intent i = new Intent(DetailRiwayatActivity.this, CartActivity.class);
            startActivity(i);
        });
        if (!Batal) {
            Boolean sudah = true;
            if (produks != null) {
                for (int i = 0; i < produks.size(); i++) {
                    if (!produks.get(i).getIsRating()) {
                        sudah = false;
                    }
                }
                if (!sudah) {
                    btnPengembalian.setVisibility(View.VISIBLE);
                    btnPengembalian.setText("Niali Produk");
                    btnPengembalian.setOnClickListener(v -> {
                        Intent intent = new Intent(DetailRiwayatActivity.this, NewRatingProduct.class);
                        intent.putExtra("PRODUCT", (Serializable) produks);
                        intent.putExtra("RIWAYAT", (Serializable) riwayat);
                        startActivityForResult(intent, 1);
                    });
                }
            }
        }
    }
    public void Keranjang(Integer id_produk,Integer jumlah){
        Call<ApiResponse> call = apiinterface.CartAdd(Token,id_users,id_produk,jumlah);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if(response.body()!=null){
                    if (response.body().getMessage()!=null){
                        Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {

            if(resultCode == RESULT_OK){
             ApiDetail();
            }

        }

    }
}