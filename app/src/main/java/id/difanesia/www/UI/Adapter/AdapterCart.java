package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;


import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Helper.MinMaxFilter;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;


public class AdapterCart extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Produk> Produk;
    OnBtnClick monBtnClick;
    FormatCurrency formatCurrency = new FormatCurrency();

    int VIEWTYPE_GROUP = 0;
    int VIEWTYPE_PRODUK = 1;



    public AdapterCart(Context context, List<Produk> produk, OnBtnClick onBtnClick) {
        this.context = context;
        this.Produk = produk;
        this.monBtnClick = onBtnClick;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEWTYPE_PRODUK) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_cart, parent, false);
            return new ProdukViewHolder(view, monBtnClick);

        } else  if (viewType == VIEWTYPE_GROUP){

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_header_cart, parent, false);
            return new GropViewHolder(view, monBtnClick);
        }
        throw new RuntimeException("No match for " + viewType + ".");
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
         if(holder instanceof GropViewHolder) {

             final AdapterCart.GropViewHolder gropViewHolder = (AdapterCart.GropViewHolder) holder;
             gropViewHolder.namaToko.setText(Produk.get(position).getToko());
             gropViewHolder.kotaToko.setText(Produk.get(position).getKota());
             if (Produk.get(position).getIdVarian()!=null){
                 if (Produk.get(position).getVarianStock()!=null) {
                     gropViewHolder.namaProduct.setText(Produk.get(position).getNama());
                     gropViewHolder.jmlProduct.setFilters( new InputFilter[]{ new MinMaxFilter( "1" , Produk.get(position).getVarianStock() )});
                     gropViewHolder.hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(Produk.get(position).getVarianHarga())));
                     gropViewHolder.varianProduct.setText(Produk.get(position).getVarian());

                     if (Produk.get(position).getVarianImage() != null) {
                         Picasso.get().load(context.getString(R.string.path) +"/uploads/products/"+ Produk.get(position).getVarianImage()).into(gropViewHolder.gambarProduct);
                     }
                 }else{
                     gropViewHolder.layoutListProduct.setEnabled(false);
                     gropViewHolder.layoutListProduct.setClickable(false);
                 }
             }
             else{
                 if (Produk.get(position).getStock()!=null) {
                     gropViewHolder.namaProduct.setText(Produk.get(position).getNama());
                     gropViewHolder.jmlProduct.setFilters( new InputFilter[]{ new MinMaxFilter( "1" , Produk.get(position).getStock() )});
                     gropViewHolder.hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(Produk.get(position).getHarga())));
                     gropViewHolder.varianProduct.setVisibility(View.GONE);
                 }
                 else{
                     gropViewHolder.layoutListProduct.setEnabled(false);
                     gropViewHolder.layoutListProduct.setClickable(false);
                 }
                 Picasso.get().load(context.getString(R.string.path)+"/uploads/products/"+Produk.get(position).getImage()).into(gropViewHolder.gambarProduct);
             }
             gropViewHolder.jmlProduct.setText(Produk.get(position).getJml().toString());
            if (Produk.get(position).getIdWish()!=null){
                gropViewHolder.btnWish.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_baseline_favorite_24));
            }
            gropViewHolder.btnWish.setOnClickListener(view -> monBtnClick.OnClickWish(position,Produk.get(position)));
            gropViewHolder.btnTambah.setOnClickListener(view -> monBtnClick.OnClickTambah(position,Produk.get(position)));
            gropViewHolder.btnKurang.setOnClickListener(view -> monBtnClick.OnClickKurang(position,Produk.get(position)));
            gropViewHolder.btnHapus.setOnClickListener(view -> monBtnClick.OnClickHapus(position,Produk.get(position)));
            gropViewHolder.checkGroup.setOnClickListener(view -> monBtnClick.OnClickCheckAll(Produk.get(position)));
            gropViewHolder.checkProduct.setOnClickListener(view -> monBtnClick.OnClickCheckProduk(position,Produk.get(position)));
            gropViewHolder.layoutListProduct.setOnClickListener(view -> monBtnClick.OnClickProduct(Produk.get(position)));
            if (Produk.get(position).getCheck()){
                gropViewHolder.checkProduct.setChecked(true);
            }

             if (position>0){
                 gropViewHolder.view.setVisibility(View.VISIBLE);
             }


         }
         if (holder instanceof ProdukViewHolder){
             final AdapterCart.ProdukViewHolder produkViewHolder = (AdapterCart.ProdukViewHolder) holder;
             if (Produk.get(position).getIdVarian()!=null){
                 if (Produk.get(position).getVarianStock()!=null) {
                     produkViewHolder.namaProduct.setText(Produk.get(position).getNama());
                     produkViewHolder.jmlProduct.setFilters( new InputFilter[]{ new MinMaxFilter( "1" , Produk.get(position).getVarianStock() )});
                     produkViewHolder.hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(Produk.get(position).getVarianHarga())));
                     produkViewHolder.varianProduct.setText(Produk.get(position).getVarian());

                     if (Produk.get(position).getVarianImage() != null) {
                         Picasso.get().load(context.getString(R.string.path) +"/uploads/products/"+ Produk.get(position).getVarianImage()).into(produkViewHolder.gambarProduct);
                     }
                 }else{
                     produkViewHolder.layoutListProduct.setEnabled(false);
                     produkViewHolder.layoutListProduct.setClickable(false);
                 }
             }
             else{
                 if (Produk.get(position).getStock()!=null) {
                     produkViewHolder.namaProduct.setText(Produk.get(position).getNama());
                     produkViewHolder.jmlProduct.setFilters( new InputFilter[]{ new MinMaxFilter( "1" , Produk.get(position).getStock() )});
                     produkViewHolder.hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(Produk.get(position).getHarga())));
                     produkViewHolder.varianProduct.setVisibility(View.GONE);
                 }
                 else{
                     produkViewHolder.layoutListProduct.setEnabled(false);
                     produkViewHolder.layoutListProduct.setClickable(false);
                 }
                 Picasso.get().load(context.getString(R.string.path) +"/uploads/products/"+ Produk.get(position).getImage()).into(produkViewHolder.gambarProduct);
             }
             produkViewHolder.jmlProduct.setText(Produk.get(position).getJml().toString());
             if (Produk.get(position).getIdWish()!=null){
                 produkViewHolder.btnWish.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_baseline_favorite_24));
             }
             produkViewHolder.btnWish.setOnClickListener(view -> monBtnClick.OnClickWish(position,Produk.get(position)));
             produkViewHolder.btnTambah.setOnClickListener(view -> monBtnClick.OnClickTambah(position,Produk.get(position)));
             produkViewHolder.btnKurang.setOnClickListener(view -> monBtnClick.OnClickKurang(position,Produk.get(position)));
             produkViewHolder.btnHapus.setOnClickListener(view -> monBtnClick.OnClickHapus(position,Produk.get(position)));
             produkViewHolder.checkProduct.setOnClickListener(view -> monBtnClick.OnClickCheckProduk(position,Produk.get(position)));
             produkViewHolder.layoutListProduct.setOnClickListener(view -> monBtnClick.OnClickProduct(Produk.get(position)));

             if (Produk.get(position).getCheck()){
                 produkViewHolder.checkProduct.setChecked(true);
             }
             
         }
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0 || !this.Produk.get(position).getIdToko().equals(this.Produk.get(position - 1).getIdToko())){
            return VIEWTYPE_GROUP;
        }
        else {
            return VIEWTYPE_PRODUK;
        }
    }

    @Override
    public int getItemCount() {
        return Produk == null ? 0 : Produk.size();
    }

    private static class GropViewHolder extends RecyclerView.ViewHolder implements OnBtnClick {

        TextView namaToko, kotaToko,hargaProduct,namaProduct,varianProduct;
        Button btnTambah,btnKurang;
        CheckBox checkGroup,checkProduct;
        EditText jmlProduct;
        ImageButton btnWish,btnHapus;
        ImageView gambarProduct;
        View view;
        LinearLayout layoutListProduct;

        public GropViewHolder(@NonNull View itemView, OnBtnClick monBtnClick) {
            super(itemView);
            namaToko = itemView.findViewById(R.id.NamaToko);
            kotaToko = itemView.findViewById(R.id.KotaToko);
            hargaProduct=itemView.findViewById(R.id.HargaProduct);
            namaProduct=itemView.findViewById(R.id.NamaProduct);
            gambarProduct=itemView.findViewById(R.id.GambarProduct);
            view=itemView.findViewById(R.id.BatasHeader);
            btnWish=itemView.findViewById(R.id.BtnWishList);
            btnHapus=itemView.findViewById(R.id.BtnHapus);
            layoutListProduct=itemView.findViewById(R.id.LayoutListProduct);
            btnTambah=itemView.findViewById(R.id.BtnTambah);
            btnKurang=itemView.findViewById(R.id.BtnKurang);
            jmlProduct=itemView.findViewById(R.id.JmlProduct);
            checkGroup=itemView.findViewById(R.id.CheckGroup);
            checkProduct=itemView.findViewById(R.id.CheckProduct);
            varianProduct=itemView.findViewById(R.id.VarianProduct);

        }

        @Override
        public void OnClickProduct(Produk produk) {

        }

        @Override
        public void OnClickCheckAll(Produk produk) {

        }

        @Override
        public void OnClickCheckProduk(int posisi,Produk produk) {

        }

        @Override
        public void OnClickWish(int posisi,Produk produk) {

        }

        @Override
        public void OnClickHapus(int posisi,Produk produk) {

        }

        @Override
        public void OnClickTambah(int posisi,Produk produk) {

        }

        @Override
        public void OnClickKurang(int posisi,Produk produk) {

        }


    }

    private static class ProdukViewHolder extends RecyclerView.ViewHolder implements OnBtnClick {
        TextView hargaProduct,namaProduct,varianProduct;
        Button btnTambah,btnKurang;
        CheckBox checkProduct;
        EditText jmlProduct;
        ImageButton btnWish,btnHapus;
        ImageView gambarProduct;
        LinearLayout layoutListProduct;

        public ProdukViewHolder(@NonNull View itemView, OnBtnClick monBtnClick) {
            super(itemView);
            checkProduct = itemView.findViewById(R.id.CheckProduct);
            namaProduct = itemView.findViewById(R.id.NamaProduct);
            varianProduct = itemView.findViewById(R.id.VarianProduct);
            hargaProduct = itemView.findViewById(R.id.HargaProduct);
            btnHapus = itemView.findViewById(R.id.BtnHapus);
            btnTambah = itemView.findViewById(R.id.BtnTambah);
            btnKurang = itemView.findViewById(R.id.BtnKurang);
            btnWish = itemView.findViewById(R.id.BtnWishList);
            gambarProduct = itemView.findViewById(R.id.GambarProduct);
            jmlProduct = itemView.findViewById(R.id.JmlProduct);
            layoutListProduct=itemView.findViewById(R.id.LayoutListProduct);

        }

        @Override
        public void OnClickProduct(Produk produk) {

        }

        @Override
        public void OnClickCheckAll(Produk produk) {

        }

        @Override
        public void OnClickCheckProduk(int posisi,Produk produk) {

        }

        @Override
        public void OnClickWish(int posisi,Produk produk) {

        }

        @Override
        public void OnClickHapus(int posisi,Produk produk) {

        }

        @Override
        public void OnClickTambah(int posisi,Produk produk) {

        }

        @Override
        public void OnClickKurang(int posisi,Produk produk) {

        }

    }

    public interface OnBtnClick {
                void OnClickProduct(Produk produk);
                void OnClickCheckAll(Produk produk);
                void OnClickCheckProduk(int posisi,Produk produk);
                void OnClickWish(int posisi,Produk produk);
                void OnClickHapus(int posisi,Produk produk);
                void OnClickTambah(int posisi,Produk produk);
                void OnClickKurang(int posisi,Produk produk);

    }


    @SuppressLint("NotifyDataSetChanged")
    public void UpdateData(int posisi, Boolean value){
        int updateIndex = posisi;
        Produk.get(updateIndex).setCheck(value);
        this.notifyItemChanged(updateIndex);
        this.notifyDataSetChanged();
    }
    public void updateDataFavorit(Integer posisis, Boolean value) {
        int newPosition = posisis;
        if (value) {
            this.Produk.get(newPosition).setIdWish(1);
            notifyItemChanged(newPosition);
        }else{
            this.Produk.get(newPosition).setIdWish(null);
            notifyItemChanged(newPosition);
        }

    }
    public void RemoveDataCart(Integer posisis){
        int updateIndex = posisis;
        this.Produk.remove(updateIndex);
        notifyItemRemoved(updateIndex);
        notifyItemRangeChanged(updateIndex, this.Produk.size());
        notifyItemRangeChanged(posisis, Produk.size());
    }
    public void ActionCart(Integer posisis,Boolean value){
        int newPosition = posisis;
        int JmlItem=this.Produk.get(newPosition).getJml();
        if (value) {
            this.Produk.get(newPosition).setJml(JmlItem+1);
            notifyItemChanged(newPosition);
        }else {
            this.Produk.get(newPosition).setJml(JmlItem - 1);
            notifyItemChanged(newPosition);

        }
    }

}
