package id.difanesia.www.UI.Activity.Menu;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.Timer;
import java.util.TimerTask;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.SpacesItemDecoration;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Berita.DetailBeritaActivity;
import id.difanesia.www.UI.Activity.Donate.Detail.DetailDonasiActivity;
import id.difanesia.www.UI.Activity.Product.CartActivity;
import id.difanesia.www.UI.Activity.Product.DetailProductActivity;
import id.difanesia.www.UI.Activity.Product.Search.SearchActivity;
import id.difanesia.www.UI.Activity.Product.WishlistActivity;
import id.difanesia.www.UI.Adapter.AdapterBerita;
import id.difanesia.www.UI.Adapter.AdapterDonasi;
import id.difanesia.www.UI.Adapter.AdapterProduct;
import id.difanesia.www.UI.Adapter.AdapterSlider;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {
    View view;
    ApiRequest apiinterface;
    LinearLayout headerAtas,kpria,kwanita,kpekerjaan,khandphone,kkomputer,khobi,kalattulis,kanak,promoLayout;
    ShimmerFrameLayout loadingSlider,loadingProductPopuler,loadingProductRating,loadingDonasi,loadingBerita,loadingPromo;
    EditText search;
    ImageButton btnWishlist;
    ImageView gambarPromo,bannerImage;
    FrameLayout sliderLayout,btnCart;
    ViewPager2 viewPager;
    DotsIndicator dotsIndicator;
    Timer timer;
    AdapterSlider adapterSlider;
    AdapterProduct adapterProduct;
    AdapterDonasi adapterDonasi;
    RecyclerView donasiList;
    AdapterBerita adapterBerita;
    RecyclerView beritaList;
    LoginViewModel loginViewModel;
    private static int currentPage = 0;
    private static final int NUM_PAGES = 3;
    String Token;
    Integer id_users,Limit=12,Start=0;
    RecyclerView recyclerView,ratingList;
    NestedScrollView infinityScroll;
    Boolean Scroll=true,Koneksi=false;
    CardView badageKeranjang;
    TextView badageKeranjangtext;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_home, container, false);
        apiinterface = ApiConfig.getClient(getContext()).create(ApiRequest.class);
        infinityScroll=view.findViewById(R.id.InfinityScroll);
        headerAtas=view.findViewById(R.id.HeaderAtas);
        bannerImage=view.findViewById(R.id.BannerPromo);
        kpria=view.findViewById(R.id.Kpria);
        kwanita=view.findViewById(R.id.Kwanita);
        kpekerjaan=view.findViewById(R.id.Kpekerjaan);
        khandphone=view.findViewById(R.id.Khandphone);
        kkomputer=view.findViewById(R.id.Klaptop);
        khobi=view.findViewById(R.id.Kolahraga);
        kalattulis=view.findViewById(R.id.Kalattulis);
        kanak=view.findViewById(R.id.kanak);
        promoLayout=view.findViewById(R.id.PromoLayout);
        gambarPromo=view.findViewById(R.id.GambarPromo);
        loadingSlider=view.findViewById(R.id.LoadingSlider);
        loadingPromo=view.findViewById(R.id.LoadingPromo);
        loadingProductPopuler=view.findViewById(R.id.LoadingProductPopuler);
        loadingProductRating=view.findViewById(R.id.LoadingProductRating);
        loadingBerita=view.findViewById(R.id.LoadingBerita);
        loadingDonasi=view.findViewById(R.id.LoadingDonasi);
        loadingSlider.startShimmer();
        loadingPromo.startShimmer();
        loadingProductPopuler.startShimmer();
        loadingProductRating.startShimmer();
        loadingBerita.startShimmer();
        loadingDonasi.startShimmer();
        sliderLayout=view.findViewById(R.id.SliderLayout);
        search=view.findViewById(R.id.SearchText);
        btnWishlist=view.findViewById(R.id.BtnWishList);
        btnCart=view.findViewById(R.id.BtnCart);
        badageKeranjang=view.findViewById(R.id.BadageKeranjang);
        badageKeranjangtext=view.findViewById(R.id.BadageKeranjangText);
        badageKeranjang.setVisibility(View.GONE);
        viewPager=view.findViewById(R.id.SliderList);
        dotsIndicator =  view.findViewById(R.id.dots_indicator);

        recyclerView =view.findViewById(R.id.ProductPopulerList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        ratingList =view.findViewById(R.id.ProductRatingList);
        ratingList.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        ratingList.setHasFixedSize(true);
        ratingList.setNestedScrollingEnabled(false);

        donasiList =view.findViewById(R.id.DonasiList);
        donasiList.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        donasiList.setHasFixedSize(true);
        donasiList.setNestedScrollingEnabled(false);

        beritaList =view.findViewById(R.id.BeritaList);
        beritaList.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        beritaList.setHasFixedSize(true);
        beritaList.setNestedScrollingEnabled(false);

        SpacesItemDecoration decoration = new SpacesItemDecoration(8,true);
        recyclerView.addItemDecoration(decoration);
        ratingList.addItemDecoration(decoration);
        donasiList.addItemDecoration(decoration);
        beritaList.addItemDecoration(decoration);
        Token();
        InfinityScroll();
        Aksi();

        return view;
    }
    public void InfinityScroll(){
        int colorFrom = getResources().getColor(R.color.transparant);
        int colorTo = getResources().getColor(R.color.Aplikasi);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(250); // milliseconds
        colorAnimation.addUpdateListener(animator -> headerAtas.setBackgroundColor((int) animator.getAnimatedValue()));
        if (infinityScroll != null) {
            infinityScroll.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                if (scrollY == 0) {
                    Scroll=true;
                    headerAtas.setBackgroundColor(getResources().getColor(R.color.transparant));
                }
                if (scrollY != 0) {
                    if (Scroll) {
                        Scroll = false;
                        colorAnimation.start();
                    }
                }
            });
        }
    }
    public void Aksi(){
        search.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), SearchActivity.class);
            startActivity(i);
        });
        btnWishlist.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), WishlistActivity.class);
            startActivity(i);
        });
        btnCart.setOnClickListener(view -> {
            Intent c = new Intent(getActivity(), CartActivity.class);
            startActivity(c);
        });
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(getViewLifecycleOwner(), login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    Home();
                    ProdukHome("populer");
                    ProdukHome("rating");
                    DonasiHome();
                    BeritaHome();

                }
                else {
                    Toast.makeText(getContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void Home(){
        Call<ApiResponse> call = apiinterface.Home(Token,Start,Limit,id_users);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){

                    //keranjang
                    if (response.body().getJmlKeranjang()>0){
                        badageKeranjang.setVisibility(View.VISIBLE);
                        badageKeranjangtext.setText(response.body().getJmlKeranjang().toString());
                    }
                    //promo
                    if (response.body().getPromo().size() > 0) {
                        loadingPromo.stopShimmer();
                        loadingPromo.setVisibility(View.GONE);
                        promoLayout.setVisibility(View.VISIBLE);
                        float density = getResources().getDisplayMetrics().density;
                        if (density >= 1.0 && density < 1.5) {
                            Picasso.get().load(getString(R.string.path) +"/uploads/products/"+ response.body().getPromo().get(0).getMdpi()).into(gambarPromo);
                        } else if (density >= 1.5 && density < 2) {

                            Picasso.get().load(getString(R.string.path)  +"/uploads/products/"+ response.body().getPromo().get(0).getHdpi()).into(gambarPromo);
                        } else if (density >= 2.0 && density < 3) {
                            Picasso.get().load(getString(R.string.path)  +"/uploads/products/"+ response.body().getPromo().get(0).getXhdpi()).into(gambarPromo);
                        } else if (density >= 3.0 && density < 4) {
                            Picasso.get().load(getString(R.string.path)  +"/uploads/products/"+ response.body().getPromo().get(0).getXxhdpi()).into(gambarPromo);
                        } else if (density >= 4.0) {
                            Picasso.get().load(getString(R.string.path)  +"/uploads/products/"+ response.body().getPromo().get(0).getXxhdpi()).into(gambarPromo);
                        }
                        gambarPromo.setScaleType(ImageView.ScaleType.FIT_XY);
                    }else{
                        loadingPromo.stopShimmer();
                        loadingPromo.setVisibility(View.GONE);
                        promoLayout.setVisibility(View.VISIBLE);
                    }
                    //slider
                    if (response.body().getSlider().size()>0) {
                        loadingSlider.stopShimmer();
                        loadingSlider.setVisibility(View.GONE);
                        sliderLayout.setVisibility(View.VISIBLE);
                        adapterSlider = new AdapterSlider(getContext(), response.body().getSlider(), viewPager);
                        viewPager.setAdapter(adapterSlider);
                        dotsIndicator.setViewPager2(viewPager);
                        getPagger();
                        /*After setting the adapter use the timer */
                        final Handler handler = new Handler();
                        final Runnable Update = () -> {
                            if (currentPage == NUM_PAGES) {
                                currentPage = 0;
                            }
                            viewPager.setCurrentItem(currentPage++, true);
                        };

                        timer = new Timer(); // This will create a new Thread
                        timer.schedule(new TimerTask() { // task to be scheduled
                            @Override
                            public void run() {
                                handler.post(Update);
                            }
                        }, 500, 5000);
                    }else{
                        loadingSlider.stopShimmer();
                        loadingSlider.setVisibility(View.GONE);
                        bannerImage.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                Log.e("getFS ", t.toString());
            }
        });
    }
    public void ProdukHome(String tipe){
        if (tipe.equals("populer")) {
            Call<ApiResponse> call = apiinterface.ProdukPopuler(Token);
                call.enqueue(new Callback<ApiResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                       if (response.body()!=null) {
                           if (response.body().getProduk().size() > 0) {
                               adapterProduct = new AdapterProduct(getContext(), response.body().getProduk(), id_users, new AdapterProduct.OnBtnClick() {
                                   @Override
                                   public void OnClickProduct(int posisi, Produk produk) {
                                       Intent intent = new Intent(getActivity(), DetailProductActivity.class);
                                       intent.putExtra("DETAILPRODUK", produk);
                                       startActivity(intent);


                                   }

                                   @Override
                                   public void OnClickWish(Integer posisi, Produk produk) {

                                       ApiWish(produk.getIdProduct(), posisi, produk.getIdWish());

                                   }
                               });
                               Koneksi = true;
                               recyclerView.setAdapter(adapterProduct);
                               adapterProduct.notifyDataSetChanged();
                               loadingProductPopuler.stopShimmer();
                               loadingProductPopuler.setVisibility(View.GONE);
                               recyclerView.setVisibility(View.VISIBLE);
                           }
                       }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                    }
                });
            }
        else {
            Call<ApiResponse> call = apiinterface.ProdukRating(Token);
                call.enqueue(new Callback<ApiResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                        if (response.body()!=null) {
                            if (response.body().getProduk().size() > 0) {
                                adapterProduct = new AdapterProduct(getContext(), response.body().getProduk(), id_users, new AdapterProduct.OnBtnClick() {
                                    @Override
                                    public void OnClickProduct(int posisi, Produk produk) {
                                        Intent intent = new Intent(getActivity(), DetailProductActivity.class);
                                        intent.putExtra("DETAILPRODUK", produk);
                                        startActivity(intent);


                                    }

                                    @Override
                                    public void OnClickWish(Integer posisi, Produk produk) {

                                        ApiWish(produk.getIdProduct(), posisi, produk.getIdWish());

                                    }
                                });
                                Koneksi = true;
                                ratingList.setAdapter(adapterProduct);
                                adapterProduct.notifyDataSetChanged();
                                loadingProductRating.stopShimmer();
                                loadingProductRating.setVisibility(View.GONE);
                                ratingList.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                    }
                });
            }
        }
    public void DonasiHome(){
        Call<ApiResponse> call = apiinterface.DonasiHome(Token);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getDonasi().size()>0){
                        adapterDonasi = new AdapterDonasi(getContext(), response.body().getDonasi(), posisi -> {
                            Intent intent = new Intent(getActivity(), DetailDonasiActivity.class);
                            intent.putExtra("DETAILDONASI",response.body().getDonasi().get(posisi));
                            startActivity(intent);
                        });
                        donasiList.setAdapter(adapterDonasi);
                        adapterDonasi.notifyDataSetChanged();
                        loadingDonasi.stopShimmer();
                        loadingDonasi.setVisibility(View.GONE);
                        donasiList.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void BeritaHome(){
        Call<ApiResponse> call = apiinterface.BeritaList(Token,0,12);
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getBerita().size()>0){
                        adapterBerita = new AdapterBerita(getContext(), response.body().getBerita(), posisi -> {
                            Intent intent = new Intent(getActivity(), DetailBeritaActivity.class);
                            intent.putExtra("DETAILBERITA",response.body().getBerita().get(posisi));
                            startActivity(intent);
                        });
                        beritaList.setAdapter(adapterBerita);
                        adapterBerita.notifyDataSetChanged();
                        loadingBerita.stopShimmer();
                        loadingBerita.setVisibility(View.GONE);
                        beritaList.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    private void getPagger(){
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);


            }


        });
    }
    public void ApiWish(int id_product,int posisi,int id_wish){
        Call<ApiResponse> call = apiinterface.WishAdd(Token,id_users,id_product);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()!=null){
                        adapterProduct.updateDataFavorit(posisi,response.body().getSuccess(),id_wish);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }


}