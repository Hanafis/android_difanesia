package id.difanesia.www.UI.Activity.Menu;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.ResetPassword.ChangePasswordActivity;
import id.difanesia.www.UI.Activity.Setting.HelpActivity;
import id.difanesia.www.UI.Activity.Welcome.LoginActivity;
import id.difanesia.www.UI.Activity.Setting.ProfileActivity;
import id.difanesia.www.UI.Activity.Riwayat.RiwayatActivity;
import id.difanesia.www.UI.Activity.Welcome.WelcomeActivity;
import id.difanesia.www.UI.Activity.Product.WishlistActivity;
import id.difanesia.www.ViewModel.LoginViewModel;
import id.difanesia.www.ViewModel.SearchViewModel;
import id.difanesia.www.ViewModel.UserViewModel;


public class SettingFragment extends Fragment {

    View view;
    TextView namaUser,ubahProfile,ubahPassword,riwayatTransaksi,whistList,penilaianAplikasi,bantuan,tentang,keluar;
    CircleImageView gambarUser;
    LoginViewModel loginViewModel;
    SearchViewModel searchViewModel;
    UserViewModel userViewModel;
    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_setting, container, false);
        namaUser=view.findViewById(R.id.NamaUser);
        ubahProfile=view.findViewById(R.id.UbahProfile);
        ubahPassword=view.findViewById(R.id.UbahPassword);
        riwayatTransaksi=view.findViewById(R.id.RIwayatTransaksi);
        whistList=view.findViewById(R.id.WhishList);
        penilaianAplikasi=view.findViewById(R.id.Ulasan);
        bantuan=view.findViewById(R.id.Bantuan);
        tentang=view.findViewById(R.id.TentangKami);
        keluar=view.findViewById(R.id.Logout);
        gambarUser=view.findViewById(R.id.GambarUser);
        searchViewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        Token();
        Aksi();

        return view;
    }
    public void Token(){
        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        searchViewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        userViewModel.getApiResponseLiveData().observe(getViewLifecycleOwner(), login -> {
            if (login!=null){
                if (login.getName()!=null){
                    namaUser.setText(login.getName());
                    Picasso.get().load(getString(R.string.path)  +"/uploads/products/"+ login.getImage()).into(gambarUser);
                }
                else {
                    Toast.makeText(getContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void Aksi(){
        ubahProfile.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), ProfileActivity.class);
            startActivity(intent);
        });
        ubahPassword.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
            startActivity(intent);
        });
        riwayatTransaksi.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), RiwayatActivity.class);
            startActivity(intent);
        });
        whistList.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), WishlistActivity.class);
            startActivity(intent);
        });
        bantuan.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), HelpActivity.class);
            startActivity(intent);
        });
        tentang.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), WelcomeActivity.class);
            startActivity(intent);
        });
        keluar.setOnClickListener(view -> {

            loginViewModel.DeleteALL();
            searchViewModel.DeleteALL();
            userViewModel.DeleteALL();
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();

        });
    }
}