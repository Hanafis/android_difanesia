package id.difanesia.www.UI.Activity.Product;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Helper.SpacesItemDecoration;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Product.Rating.RatingsActivity;
import id.difanesia.www.UI.Adapter.AdapterImage;
import id.difanesia.www.UI.Adapter.AdapterProduct;
import id.difanesia.www.UI.Adapter.Varian.AdapterVarian;
import id.difanesia.www.UI.Adapter.Varian.AdapterVarian2;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProductActivity extends AppCompatActivity {
    String Token,sampul;
    Integer id_users;
    TextView hargaProduct,namaProduct,deskripsiProduct,ratingProduct,namaToko,ratingToko,chatToko,prosesToko,kotaToko,headerProduct,ratingUser,namaRatingUser,deskripsiRatingUser,ratingTotalUser,typeVarian,typevarian2,hargaVarian,stockvarian;
    ImageButton btnFavorit,btnShare,btnClose,btnCart;
    RatingBar ratingBarProduct,ratingBarUser,ratingBarTotalUser;
    Button btnLihatsemua,btnKeranjang,btnBeli,btnKurang,btnTambah,btnVarianKeranjang;
    ImageView gambarVarian,gambarRating;
    EditText jmlProduct;
    LinearLayout btnBackHeader,layoutrating;
    CircleImageView tokoimage,userImage;
    ViewPager2 viewPager;
    DotsIndicator dotsIndicator;
    RecyclerView productOther,listVarian,listVarian2;
    LoginViewModel loginViewModel;
    ApiRequest apiinterface;
    AdapterImage adapterSlider;
    AdapterProduct adapterProduct;
    AdapterVarian adapterVarian;
    AdapterVarian2 adapterVarian2;
    FormatCurrency formatCurrency=new FormatCurrency();
    BottomSheetDialog varianDialog;
    Produk produk;
    View varian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        //product
        btnBackHeader=findViewById(R.id.HeaderAtas);
        headerProduct=findViewById(R.id.ProductHeader);
        namaProduct=findViewById(R.id.NamaProduct);
        hargaProduct=findViewById(R.id.HargaProduct);
        deskripsiProduct=findViewById(R.id.DeskripsiProduct);
        ratingProduct=(TextView) findViewById(R.id.RatingProducts);
        ratingBarProduct=findViewById(R.id.RatingBarProduct);
        btnFavorit=findViewById(R.id.BtnWishList);
        btnShare=findViewById(R.id.BtnShare);
        viewPager=findViewById(R.id.ImageDetailProduct);
        dotsIndicator=findViewById(R.id.dots_indicator);
        //toko
        namaToko=findViewById(R.id.NamaToko);
        kotaToko=findViewById(R.id.KotaToko);
        ratingToko=findViewById(R.id.RatingToko);
        chatToko=findViewById(R.id.ChatToko);
        prosesToko=findViewById(R.id.ProsesToko);
        tokoimage=findViewById(R.id.TokoImage);
        //user
        ratingBarTotalUser=findViewById(R.id.RatingBarTotalUser);
        ratingUser=findViewById(R.id.RatingUser);
        ratingTotalUser=findViewById(R.id.TotalRatingUser);
        ratingBarUser=findViewById(R.id.RatingBarUser);
        namaRatingUser=findViewById(R.id.NamaRatingUser);
        deskripsiRatingUser=findViewById(R.id.DeskripsiRatingUser);
        userImage=findViewById(R.id.GambarUser);
        layoutrating=findViewById(R.id.LayoutRating);
        gambarRating=findViewById(R.id.GambarRating);

        //btn
        btnLihatsemua=findViewById(R.id.BtnLihatSemua);
        btnBeli=findViewById(R.id.BtnBeli);
        btnKeranjang=findViewById(R.id.BtnKeranjang);

        //other
        btnCart=findViewById(R.id.BtnCart);
        productOther=findViewById(R.id.OtherProductList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        productOther.setLayoutManager(layoutManager);
        productOther.setHasFixedSize(true);
        SpacesItemDecoration decoration = new SpacesItemDecoration(8,true);
        productOther.addItemDecoration(decoration);

        Shared(savedInstanceState);
        if (produk!=null) {
            Token();
            SetProduct();
            SetAksi();
        }


    }
    public void Shared(Bundle savedInstanceState){
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                produk= null;
            } else {
                produk= (Produk) extras.getSerializable("DETAILPRODUK");
            }
        }
        else {
            produk= (Produk) savedInstanceState.getSerializable("DETAILPRODUK");
        }
    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    DetailProduct();
                    VarianLayout();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    public void SetProduct(){
        //product
        headerProduct.setText(produk.getNama());
        namaProduct.setText(produk.getNama());
        hargaProduct.setText(formatCurrency.Rupiah(Double.parseDouble(produk.getHarga())));
        deskripsiProduct.setText(produk.getDeskripsi());
        if (produk.getRating()!=null) {
            ratingProduct.setText(produk.getRating().toString());
            ratingBarProduct.setRating(produk.getRating());
        }
        if (produk.getIdWish()!=null){
            btnFavorit.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_24));
            Log.e("ID_WISH",produk.getIdWish().toString());
        }



         //toko
        namaToko.setText(produk.getToko());
        kotaToko.setText(produk.getKota());
//        chatToko.setText();
//        prosesToko.setText();
        if (produk.getImageToko()!=null) {
            Picasso.get().load(getString(R.string.path)+"/uploads/products/"+produk.getImageToko()).into(tokoimage);
        }

    }
    public void SetAksi(){
        btnBackHeader.setOnClickListener(view -> onBackPressed());
        btnFavorit.setOnClickListener(view -> ApiWish());
        btnShare.setOnClickListener(view -> {

        });
        btnBeli.setOnClickListener(view -> {
            produk.setJml(1);
            List<Produk> keranjang = new ArrayList<>();
            keranjang.add(produk);

            Intent intent = new Intent(DetailProductActivity.this, CheckoutActivity.class);
            intent.putExtra("PRODUCT",(Serializable) keranjang);
            intent.putExtra("JMLBARANG",Float.parseFloat(produk.getJml().toString()));
            intent.putExtra("JMLHARGA",Float.parseFloat(produk.getHarga()));
            startActivity(intent);
        });
        btnKeranjang.setOnClickListener(view -> {
//                varianDialog.setContentView(varian);
//                varianDialog.show();
            Keranjang();
        });
        btnLihatsemua.setOnClickListener(view -> {
            SharedPreferences sharedPref = getSharedPreferences("DataProduct", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("PRODUCTID", produk.getIdProduct());
            editor.apply();
                Intent intent = new Intent(DetailProductActivity.this, RatingsActivity.class);
                startActivity(intent);

        });
        btnCart.setOnClickListener(view -> {
            Intent intent = new Intent(DetailProductActivity.this, CartActivity.class);
            startActivity(intent);
        });

    }
    public void VarianLayout(){
        varianDialog = new BottomSheetDialog(DetailProductActivity.this, R.style.BottomSheetDialogTheme);
        varian= LayoutInflater.from(getApplicationContext()).inflate(R.layout.varian_layout,findViewById(R.id.VarianContainer));
        btnClose=varian.findViewById(R.id.BtnCloseVarian);
        gambarVarian=varian.findViewById(R.id.GambarVarian);
        typeVarian=varian.findViewById(R.id.TypeVarian);
        typevarian2=varian.findViewById(R.id.TypeVarian2);
        listVarian=varian.findViewById(R.id.ListVarian);
        listVarian2=varian.findViewById(R.id.ListVarian2);
        hargaVarian=varian.findViewById(R.id.HargaVarian);
        stockvarian=varian.findViewById(R.id.StcokVarian);
        btnKurang=varian.findViewById(R.id.BtnKurang);
        btnTambah=varian.findViewById(R.id.BtnTambah);
        jmlProduct=varian.findViewById(R.id.JmlProduct);
        btnVarianKeranjang=varian.findViewById(R.id.BtnVarianKeranjang);
        listVarian.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        listVarian.setHasFixedSize(true);

        listVarian2.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        listVarian2.setHasFixedSize(true);

            Picasso.get().load(varian.getContext().getString(R.string.path) +"/uploads/products/"+ "sepatu.png" ).into(gambarVarian);
        hargaVarian.setText(formatCurrency.Rupiah(Double.parseDouble(produk.getHarga())));

//        ApiVarian();

    }
    public void DetailProduct() {
        Call<ApiResponse> call = apiinterface.DetailProduct(Token, produk.getIdProduct(),produk.getIdUser());
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint({"SetTextI18n", "NotifyDataSetChanged"})
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body() != null) {
                    //gambar
                    if (response.body().getImageProduct() != null) {
                        if (response.body().getImageProduct().size() > 0) {
                            adapterSlider = new AdapterImage(getApplicationContext(), response.body().getImageProduct(), viewPager);
                            viewPager.setAdapter(adapterSlider);
                            dotsIndicator.setViewPager2(viewPager);
                            sampul=response.body().getImageProduct().get(0).getPathimage();
                        }

                    }
                    //otherproduct
                    if (response.body().getProduk() != null) {
                        if (response.body().getProduk().size() > 0) {
                            adapterProduct = new AdapterProduct(getApplicationContext(), response.body().getProduk(), id_users, new AdapterProduct.OnBtnClick() {
                                @Override
                                public void OnClickProduct(int posisi,Produk produk) {
                                    Intent intent = new Intent(getApplicationContext(), DetailProductActivity.class);
                                    intent.putExtra("DETAILPRODUK",produk);
                                    startActivity(intent);


                                }

                                @Override
                                public void OnClickWish(Integer posisi,Produk produk) {
                                    ApiWish(produk.getIdProduct(),posisi,produk.getIdWish());
                                }
                            });
                            adapterProduct.notifyDataSetChanged();
                            productOther.setAdapter(adapterProduct);
//                            loadingOtherproduct.stopShimmer();
//                            loadingOtherproduct.setVisibility(View.GONE);
//                            productOther.setVisibility(View.VISIBLE);

                        }

                    }
                    //rating
                    assert response.body() != null;
                    if (response.body().getRating() != null) {
                        if(response.body().getRating().getRating()!=null) {
                            ratingBarTotalUser.setRating(Float.parseFloat(response.body().getRating().getRating().replaceAll("\\.?0*$", "")));
                            ratingUser.setText(response.body().getRating().getRating().replaceAll("\\.?0*$", ""));
                            ratingTotalUser.setText("( " + response.body().getRating().getTotal().toString() + " Review )");
                            ratingToko.setText(response.body().getRating().getRating().replaceAll("\\.?0*$", ""));

                        }


                    }
                    //userRating
                    if (response.body().getUserRating() != null) {
                        if (response.body().getUserRating().size() > 0) {
                            layoutrating.setVisibility(View.VISIBLE);
                            btnLihatsemua.setVisibility(View.VISIBLE);
                            ratingBarUser.setRating(response.body().getUserRating().get(0).getRating());
                            namaRatingUser.setText(response.body().getUserRating().get(0).getName());
                            if (response.body().getUserRating().get(0).getDeskripsi()!=null) {
                                deskripsiRatingUser.setText(response.body().getUserRating().get(0).getDeskripsi() + "");


                            }
                            if (response.body().getUserRating().get(0).getImguser()!=null){
                                Picasso.get().load(getString(R.string.path)+"/"+response.body().getUserRating().get(0).getImguser()).into(userImage);
                            }
                            if (response.body().getUserRating().get(0).getImage()!=null){
                                gambarRating.setVisibility(View.VISIBLE);
                                Picasso.get().load(getString(R.string.path) +"/"+response.body().getUserRating().get(0).getImage()).into(gambarRating);
                            }
                        }

                    }


                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });

    }
    public void ApiWish(){
        Call<ApiResponse> call = apiinterface.WishAdd(Token,id_users,produk.getIdProduct());
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()!=null){
                        if (response.body().getSuccess()){
                            btnFavorit.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_24));
                        }
                        else{
                            btnFavorit.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void ApiWish(int id_product,int posisi,int id_wish){
        Call<ApiResponse> call = apiinterface.WishAdd(Token,id_users,id_product);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getSuccess()!=null){
                        adapterProduct.updateDataFavorit(posisi,response.body().getSuccess(),id_wish);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void ApiVarian(){
        Call<ApiResponse> call = apiinterface.Varian(Token,produk.getIdProduct());
        call.enqueue(new Callback<ApiResponse>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if (response.body()!=null){
                    if (response.body().getVarian()!=null){
                        adapterVarian = new AdapterVarian(getApplicationContext(), response.body().getVarian(), posisi -> {

                        });
                        adapterVarian2 = new AdapterVarian2(getApplicationContext(), response.body().getVarian(), posisi -> {

                        });
                        listVarian.setAdapter(adapterVarian);
                        adapterVarian.notifyDataSetChanged();
                        listVarian2.setAdapter(adapterVarian2);
                        adapterVarian2.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }
    public void Keranjang(){
        Call<ApiResponse> call = apiinterface.CartAdd(Token,id_users,produk.getIdProduct(),1);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                if(response.body()!=null){
                    if (response.body().getMessage()!=null){
                        Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

            }
        });
    }

}