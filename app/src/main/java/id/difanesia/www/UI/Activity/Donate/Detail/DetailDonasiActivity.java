package id.difanesia.www.UI.Activity.Donate.Detail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Berita;
import id.difanesia.www.Model.Donasi;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterTabDetailDonate;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailDonasiActivity extends AppCompatActivity {
    AdapterTabDetailDonate adapter;
    ViewPager2 viewPager2;
    TabLayout tabLayout;
    Donasi donasi;
    TextView targetDonasi,jumlahDonasi,totalHari;
    FormatCurrency formatCurrency= new FormatCurrency();
    ImageView gambarDonasi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donasi);
        viewPager2=findViewById(R.id.pager);
        tabLayout=findViewById(R.id.LayoutTabs);
        targetDonasi=findViewById(R.id.TargetDonasi);
        jumlahDonasi=findViewById(R.id.JumlahDonasi);
        totalHari=findViewById(R.id.TotalHari);
        gambarDonasi=findViewById(R.id.GambarDonasi);
        Setup(savedInstanceState);
        Menu();




    }
    @SuppressLint("SetTextI18n")
    public void Setup(Bundle savedInstanceState){
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                donasi= null;
            } else {
                donasi= (Donasi) extras.getSerializable("DETAILDONASI");
            }
        }
        else {
            donasi= (Donasi) savedInstanceState.getSerializable("DETAILDONASI");
        }
        if (donasi!=null){

            targetDonasi.setText(formatCurrency.Rupiah(Double.parseDouble(donasi.getTarget())));
//            jumlahDonasi.setText(""+donasi.getTotalDonatur().toString());
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = simpleDateFormat.parse(donasi.getTanggalselesai());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat convetDateFormat = new SimpleDateFormat("MMMM d,yyyy");
            assert date != null;
            String Htanggal=convetDateFormat.format(date);
            totalHari.setText(Htanggal);
            Picasso.get().load(getResources().getString(R.string.path)+"/uploads/products/"+donasi.getGambar()).into(gambarDonasi);
        }
    }
    public void Menu(){
        adapter=new AdapterTabDetailDonate(DetailDonasiActivity.this,donasi);
        viewPager2.setAdapter(adapter);
        tabLayout.addTab(tabLayout.newTab().setText("Detail"));
        tabLayout.addTab(tabLayout.newTab().setText("Info Donasi"));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });

    }



}