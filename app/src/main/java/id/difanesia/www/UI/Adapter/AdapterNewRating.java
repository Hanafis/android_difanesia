package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



import com.squareup.picasso.Picasso;


import java.util.List;


import id.difanesia.www.Model.Produk;
import id.difanesia.www.R;

public class AdapterNewRating extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    OnBtnClick monBtnClick;
    List<Produk> produks;
    public AdapterNewRating(Context context, List<Produk> produkList,OnBtnClick onBtnClick){
        this.produks=produkList;
        this.monBtnClick=onBtnClick;
        context=context;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_new_rating,parent,false);
        return new NewRatingViewHolder(view,monBtnClick);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NewRatingViewHolder){
            final NewRatingViewHolder newRatingViewHolder = (NewRatingViewHolder) holder;
                newRatingViewHolder.namaProduct.setText(produks.get(position).getNama()+"");
                newRatingViewHolder.namaToko.setText(produks.get(position).getToko()+"");
                Picasso.get().load(produks.get(position).getImage()).into(newRatingViewHolder.gambarRating);
            newRatingViewHolder.layoutRating.setOnClickListener(v -> monBtnClick.OnClickProduct(produks.get(position)));
            }
        }

    @Override
    public int getItemCount() {
        return produks==null?0:produks.size();
    }


    private class NewRatingViewHolder extends RecyclerView.ViewHolder implements AdapterProduct.OnBtnClick {
        LinearLayout layoutRating;
        ImageView gambarRating;
        TextView namaProduct,namaToko;

        public NewRatingViewHolder(View view, OnBtnClick monBtnClick) {
            super(view);
            layoutRating=view.findViewById(R.id.LayoutRating);
            namaProduct=view.findViewById(R.id.NamaBarang);
            namaToko=view.findViewById(R.id.NamaToko);
            gambarRating=view.findViewById(R.id.GambarBarang);



        }

        @Override
        public void OnClickProduct(int posisi, Produk produk) {

        }

        @Override
        public void OnClickWish(Integer posisi, Produk produk) {

        }
    }
    public  interface OnBtnClick{

        void OnClickProduct(Produk produk);

    }
}
