package id.difanesia.www.UI.Adapter;





import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;


import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;



import com.squareup.picasso.Picasso;

import java.util.List;

import id.difanesia.www.Model.ImageProduct;
import id.difanesia.www.R;


public class AdapterImage extends RecyclerView.Adapter<AdapterImage.ViewHolder>{
    Context context;
    List<ImageProduct> itemds;
    ViewPager2 viewPager;

    public AdapterImage(Context context, List<ImageProduct> mySliderLists, ViewPager2 viewPager){
        super();
        this.context = context;
        this.itemds = mySliderLists;
        this.viewPager = viewPager;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_slider, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder  holder, int position) {

            Picasso.get().load(context.getString(R.string.path) + itemds.get(position).getPathimage()).into(holder.image);

//        holder.image.setScaleType(ImageView.ScaleType.FIT_XY);


    }

    @Override
    public int getItemCount() {
        return itemds.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        LinearLayout button;
        LinearLayout relativeLayout;
        public ViewHolder(@NonNull  View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.sliderimage);
            button=itemView.findViewById(R.id.sliderlayout);
            relativeLayout = itemView.findViewById(R.id.container);



        }
    }
    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<ImageProduct> slider, ViewPager2 viewPager) {
        this.itemds = slider;
        this.viewPager=viewPager;
        this.itemds.add(0, null);
        notifyDataSetChanged();
    }

}


