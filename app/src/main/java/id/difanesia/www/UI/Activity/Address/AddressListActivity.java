package id.difanesia.www.UI.Activity.Address;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Model.Alamat;
import id.difanesia.www.Model.Voucher;
import id.difanesia.www.R;
import id.difanesia.www.UI.Adapter.AdapterListAddress;
import id.difanesia.www.ViewModel.LoginViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressListActivity extends AppCompatActivity {
    ApiRequest apiinterface;
    LoginViewModel loginViewModel;
    String Token;
    Integer id_users;
    TextView btnProvinsi,btnKabupaten,btnKecamatan,btnKelurahan,btnKodePos,pilihan;
    Button btnSimpan;
    RecyclerView listAlamat;
    Integer id_provinsi,id_kabupaten,id_kecamatan,id_kelurahan;
    AdapterListAddress adapterListAddress;
    String Provinsi,Kabupaten,Kecamatan,Kelurahan,KodePos;
    ImageButton btnBack;
    Alamat alamat ;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        btnProvinsi=findViewById(R.id.Provinsi);
        btnKabupaten=findViewById(R.id.Kabupaten);
        btnKecamatan=findViewById(R.id.Kecamatan);
        btnKelurahan=findViewById(R.id.Kelurahan);
        btnKodePos=findViewById(R.id.KodePos);
        pilihan=findViewById(R.id.Pilihan);
        btnSimpan=findViewById(R.id.BtnSimpan);
        btnBack=findViewById(R.id.BtnBack);
        listAlamat =findViewById(R.id.ListAlamat);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        listAlamat.setLayoutManager(layoutManager);
        listAlamat.setHasFixedSize(true);
        listAlamat.setNestedScrollingEnabled(false);
        pilihan.setText("Pilih Provinsi");
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                alamat= null;
            } else {
                alamat= (Alamat) extras.getSerializable("ALAMATS");

            }
        }
        else {
            alamat=(Alamat) savedInstanceState.getSerializable("ALAMATS");
        }
        if (alamat!=null){
            id_provinsi=alamat.getIdProvinsi();
            id_kabupaten=alamat.getIdKabupaten();
            id_kecamatan=alamat.getIdKecamatan();
            id_kelurahan=alamat.getIdKelurahan();
            Provinsi=alamat.getProvinsi();
            Kabupaten=alamat.getKabupaten();
            Kecamatan=alamat.getKecamatan();
            Kelurahan=alamat.getKelurahan();
            KodePos=alamat.getKodepos();
            btnProvinsi.setText(alamat.getProvinsi());
            btnKabupaten.setText(alamat.getKabupaten());
            btnKecamatan.setText(alamat.getKecamatan());
            btnKelurahan.setText(alamat.getKelurahan());
            btnKodePos.setText(alamat.getKodepos());
        }
        Token();

    }
    public void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();
                    Aksi();
                    ApiAlamat(null,"Provinsi");

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    @SuppressLint("SetTextI18n")
    public void Aksi(){
        btnProvinsi.setOnClickListener(view -> {
            ApiAlamat(null,"Provinsi");
            btnKabupaten.setText("Kabupaten");
            btnKecamatan.setText("Kecamatan");
            btnKelurahan.setText("Kelurahan");
            btnKodePos.setText("KodePos");

        });
        btnKabupaten.setOnClickListener(view -> {
            ApiAlamat(id_provinsi,"Kabupaten");
            btnKecamatan.setText("Kecamatan");
            btnKelurahan.setText("Kelurahan");
            btnKodePos.setText("KodePos");

        });
        btnKecamatan.setOnClickListener(view -> {
            ApiAlamat(id_kabupaten,"Kecamatan");
            btnKelurahan.setText("Kelurahan");
            btnKodePos.setText("KodePos");
        });
        btnKelurahan.setOnClickListener(view -> {
            ApiAlamat(id_kecamatan,"Kelurahan");
            btnKodePos.setText("KodePos");
        });
        btnKodePos.setOnClickListener(view -> ApiAlamat(id_kelurahan,"KodePos"));
        btnBack.setOnClickListener(view -> onBackPressed());
        btnSimpan.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.putExtra("ALAMATS", alamat);
            setResult(Activity.RESULT_OK,intent);
            finish();
        });

    }
    public void ApiAlamat(Integer id,String Tipe){
        switch (Tipe) {
            case "Provinsi": {
                Call<ApiResponse> call = apiinterface.Alamats(Token, id_users);
                call.enqueue(new Callback<ApiResponse>() {
                    @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                    @Override
                    public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                        if (response.body() != null) {
                            if (response.body().getListAlamat().size()>0) {
                                adapterListAddress = new AdapterListAddress(getApplicationContext(), response.body().getListAlamat(), posisi -> {
                                    Provinsi=response.body().getListAlamat().get(posisi).getProvinsi();
                                    id_provinsi=Integer.parseInt(response.body().getListAlamat().get(posisi).getIdProvinsi());
                                    btnProvinsi.setText(response.body().getListAlamat().get(posisi).getProvinsi());
                                    if (alamat==null) {
                                        alamat = new Alamat(null, null, null, null, null, null, null, null);
                                    }
                                    alamat.setIdProvinsi(id_provinsi);
                                    alamat.setProvinsi(Provinsi);
                                    ApiAlamat(id_provinsi, "Kabupaten");
                                });
                                pilihan.setText("Pilih Provinsi");
                                listAlamat.setAdapter(adapterListAddress);
                                adapterListAddress.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                    }
                });
                break;
            }
            case "Kabupaten": {
                if (Provinsi==null){
                    Toast.makeText(getApplicationContext(),"Pilih Provinsi Terlebih Dahulu",Toast.LENGTH_LONG).show();
                }
                else {
                    Call<ApiResponse> call = apiinterface.Kabupaten(Token, id_users, id);
                    call.enqueue(new Callback<ApiResponse>() {
                        @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                        @Override
                        public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                            if (response.body() != null) {
                                if (response.body().getListAlamat() != null) {
                                    adapterListAddress = new AdapterListAddress(getApplicationContext(), response.body().getListAlamat(), posisi -> {
                                        Kabupaten=response.body().getListAlamat().get(posisi).getKabupaten();
                                        id_kabupaten=Integer.parseInt(response.body().getListAlamat().get(posisi).getIdKabupaten());
                                        btnKabupaten.setText(response.body().getListAlamat().get(posisi).getKabupaten());
                                        alamat.setIdKabupaten(id_kabupaten);
                                        alamat.setKabupaten(Kabupaten);
                                        ApiAlamat(id_kabupaten, "Kecamatan");
                                    });
                                    pilihan.setText("Pilih Kabupaten");
                                    listAlamat.setAdapter(adapterListAddress);
                                    adapterListAddress.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                        }
                    });
                }
                break;
            }
            case "Kecamatan": {
                if (Kabupaten==null){
                    Toast.makeText(getApplicationContext(),"Pilih Provinsi Kabupaten Dahulu",Toast.LENGTH_LONG).show();
                }
                else {


                    Call<ApiResponse> call = apiinterface.Kecamatan(Token, id_users, id);
                    call.enqueue(new Callback<ApiResponse>() {
                        @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                        @Override
                        public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                            if (response.body() != null) {
                                if (response.body().getListAlamat() != null) {
                                    adapterListAddress = new AdapterListAddress(getApplicationContext(), response.body().getListAlamat(), posisi -> {
                                        Kecamatan=response.body().getListAlamat().get(posisi).getKecamatan();
                                        id_kecamatan=Integer.parseInt(response.body().getListAlamat().get(posisi).getIdKecamatan());
                                        btnKecamatan.setText(response.body().getListAlamat().get(posisi).getKecamatan());
                                        alamat.setIdKecamatan(id_kecamatan);
                                        alamat.setKecamatan(Kecamatan);
                                        ApiAlamat(id_kecamatan, "Kelurahan");
                                    });
                                    pilihan.setText("Pilih Kecamatan");
                                    listAlamat.setAdapter(adapterListAddress);
                                    adapterListAddress.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                        }
                    });
                }
                break;
            }
            case "Kelurahan": {
                if (Kecamatan==null){
                    Toast.makeText(getApplicationContext(),"Pilih Kecamatan Terlebih Dahulu",Toast.LENGTH_LONG).show();
                }
                else {
                    Call<ApiResponse> call = apiinterface.Kalurahan(Token, id_users, id);
                    call.enqueue(new Callback<ApiResponse>() {
                        @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                        @Override
                        public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                            if (response.body() != null) {
                                if (response.body().getListAlamat() != null) {
                                    adapterListAddress = new AdapterListAddress(getApplicationContext(), response.body().getListAlamat(), posisi -> {
                                        Kelurahan=response.body().getListAlamat().get(posisi).getKelurahan();
                                        id_kelurahan=Integer.parseInt(response.body().getListAlamat().get(posisi).getIdKelurahan());
                                        btnKelurahan.setText(response.body().getListAlamat().get(posisi).getKelurahan());
                                        alamat.setIdKelurahan(id_kelurahan);
                                        alamat.setKelurahan(Kelurahan);
                                        ApiAlamat(id_kelurahan, "KodePos");

                                    });

                                    pilihan.setText("Pilih Kelurahan");
                                    listAlamat.setAdapter(adapterListAddress);
                                    adapterListAddress.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                        }
                    });
                }
                break;
            }
            case "KodePos": {
                if (Kelurahan==null){
                    Toast.makeText(getApplicationContext(),"Pilih Kelurahan Terlebih Dahulu",Toast.LENGTH_LONG).show();
                }
                else {
                    Call<ApiResponse> call = apiinterface.KodePos(Token, id_users, id);
                    call.enqueue(new Callback<ApiResponse>() {
                        @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                        @Override
                        public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                            if (response.body() != null) {
                                if (response.body().getListAlamat() != null) {
                                    adapterListAddress = new AdapterListAddress(getApplicationContext(), response.body().getListAlamat(), posisi -> {
                                        KodePos=response.body().getListAlamat().get(posisi).getKodePos().toString();
                                        btnKodePos.setText(response.body().getListAlamat().get(posisi).getKodePos().toString());
                                        alamat.setKodepos(KodePos);

                                    });
                                    pilihan.setText("Pilih KodePos");
                                    listAlamat.setAdapter(adapterListAddress);
                                    adapterListAddress.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {

                        }
                    });
                }
                break;
            }
        }

    }

}