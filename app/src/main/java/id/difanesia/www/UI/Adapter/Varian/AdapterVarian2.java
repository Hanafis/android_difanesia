package id.difanesia.www.UI.Adapter.Varian;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import id.difanesia.www.Model.Varian;
import id.difanesia.www.R;

public class AdapterVarian2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Varian> varians;
    List<String> jenis= new ArrayList<>();
    OnBtnClick onBtnClick;
    int VIEWTYPE_SAMA = 0;
    int VIEWTYPE_BARU = 1;
    public AdapterVarian2(Context context,List<Varian> varians, OnBtnClick onBtnClick){
        this.context=context;
        this.varians=varians;
        this.onBtnClick=onBtnClick;

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==VIEWTYPE_BARU) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_varian2, parent, false);
        return new VarianViewHolder(view);
        }else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_varian2, parent, false);
            return new NullViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof VarianViewHolder) {
            final VarianViewHolder varianViewHolder = (VarianViewHolder) holder;

                this.jenis.add(varians.get(position).getVarian2());
                varianViewHolder.namaVarian.setText(varians.get(position).getVarian2());
            }

    }

    @Override
    public int getItemCount()  {
        return varians==null ? 0:varians.size();
    }
    @Override
    public int getItemViewType(int position) {

        if (position==0){
            return VIEWTYPE_BARU;
        }
        else if (varians.get(position).getVarian2().equals(varians.get(position-1).getVarian2())){
            return VIEWTYPE_SAMA;
        }
        else{
            return VIEWTYPE_BARU;
        }
    }


    public interface OnBtnClick{
        void OnClickVarian(int posisi);
    }

    private static class VarianViewHolder extends RecyclerView.ViewHolder implements OnBtnClick {
        Button namaVarian;
        LinearLayout layoutVarian;
        public VarianViewHolder(View view) {
            super(view);
            namaVarian=view.findViewById(R.id.NamaVarian);
            layoutVarian=view.findViewById(R.id.LayoutVarian2);

        }

        @Override
        public void OnClickVarian(int posisi) {

        }
    }

    private static class NullViewHolder extends RecyclerView.ViewHolder {
        public NullViewHolder(View view) {
            super(view);
        }
    }
}

