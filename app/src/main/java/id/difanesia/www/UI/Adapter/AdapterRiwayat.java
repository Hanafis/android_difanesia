package id.difanesia.www.UI.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.Model.Riwayat;
import id.difanesia.www.R;

public class AdapterRiwayat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Riwayat> riwayats;
    Context context;
    FormatCurrency formatCurrency = new FormatCurrency();
    OnClickButton onclickButton;
    public AdapterRiwayat(Context context,List<Riwayat> riwayats,OnClickButton onclickButton){
        this.riwayats=riwayats;
        this.context=context;
        this.onclickButton=onclickButton;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_riwayat,parent,false);
            return new RiwayatViewHolder(view,onclickButton);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RiwayatViewHolder){

            final RiwayatViewHolder riwayatViewHolder = (RiwayatViewHolder) holder;
            if (riwayats.get(position).getRounded()){
                riwayatViewHolder.layoutRiwayat.setBackground(context.getResources().getDrawable(R.drawable.rounded_product));
            }
            riwayatViewHolder.namaToko.setText(riwayats.get(position).getProduk().getToko());
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = simpleDateFormat.parse(riwayats.get(position).getCreatedAt());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat convetDateFormat = new SimpleDateFormat("MMMM d,yyyy");
            assert date != null;
            String Htanggal=convetDateFormat.format(date);
            riwayatViewHolder.tanggalTransaksi.setText(Htanggal);
            riwayatViewHolder.statusTransaksi.setText(riwayats.get(position).getStatus());
            riwayatViewHolder.namaProduk.setText(riwayats.get(position).getProduk().getNama());
            if (riwayats.get(position).getProduk().getVarian()!=null) {
                //Varian
                riwayatViewHolder.varianProduk.setVisibility(View.VISIBLE);
            }
            riwayatViewHolder.jumlahProduk.setText(riwayats.get(position).getProduk().getJumlah().toString());
            riwayatViewHolder.hargaProduk.setText(formatCurrency.Rupiah(Double.parseDouble(riwayats.get(position).getProduk().getHarga())));
            riwayatViewHolder.pesan.setText(riwayats.get(position).getPesan());
            if (riwayats.get(position).getLacakPaket()!=null && !riwayats.get(position).getLacakPaket().equals(" ")){
                riwayatViewHolder.layoutKirim.setVisibility(View.VISIBLE);
                riwayatViewHolder.lacakPaket.setText(riwayats.get(position).getLacakPaket());
            }
            Picasso.get().load(context.getResources().getString(R.string.path)+"/uploads/products/"+riwayats.get(position).getProduk().getImage()).into(riwayatViewHolder.gambarProduct);
            riwayatViewHolder.btnAksi.setOnClickListener(view -> onclickButton.OnclickBtn(riwayats.get(position)));
            riwayatViewHolder.layoutRiwayat.setOnClickListener(view -> onclickButton.OnClickDetail(riwayats.get(position)));
            if(riwayats.get(position).getStatus().equals("Belum Dibayar")){
                riwayatViewHolder.btnAksi.setVisibility(View.GONE);
            }
            else if (riwayats.get(position).getStatus().equals("Sedang Dikemas") || riwayats.get(position).getStatus().equals("Sedang Dikirim")  ){
                riwayatViewHolder.btnAksi.setText("Hubungi Penjual");
            }
            else if(riwayats.get(position).getStatus().equals("Telah Diterima") || riwayats.get(position).getStatus().equals("Dibatalkan")){
                riwayatViewHolder.btnAksi.setText("Beli Lagi");
            }else{
                riwayatViewHolder.btnAksi.setText("Rincian Pengembalian");
            }
        }

    }

    @Override
    public int getItemCount() {
        return riwayats==null?0:riwayats.size();
    }
    public interface OnClickButton{
        void OnClickDetail(Riwayat riwayat);
        void OnclickBtn(Riwayat riwayat);
    }

    private class RiwayatViewHolder extends RecyclerView.ViewHolder implements OnClickButton {
        TextView namaToko,tanggalTransaksi,statusTransaksi,namaProduk,varianProduk,jumlahProduk,hargaProduk,pesan,lacakPaket;
        ImageView gambarProduct;
        Button btnAksi;
        LinearLayout layoutPesan,layoutKirim,layoutRiwayat,layoutToko;

        public RiwayatViewHolder(View view, OnClickButton onclickButton) {
            super(view);
            namaToko=view.findViewById(R.id.NamaToko);
            tanggalTransaksi=view.findViewById(R.id.TanggalTransaksi);
            statusTransaksi=view.findViewById(R.id.StatusTransaksi);
            namaProduk=view.findViewById(R.id.NamaProduct);
            varianProduk=view.findViewById(R.id.VarianProduct);
            jumlahProduk=view.findViewById(R.id.JumlahProduct);
            hargaProduk=view.findViewById(R.id.HargaProduct);
            pesan=view.findViewById(R.id.Pesan);
            lacakPaket=view.findViewById(R.id.LacakPaket);
            gambarProduct=view.findViewById(R.id.GambarProduk);
            btnAksi=view.findViewById(R.id.BtnAksi);
            layoutKirim=view.findViewById(R.id.LayoutDikirim);
            layoutPesan=view.findViewById(R.id.LayoutPesan);
            layoutRiwayat=view.findViewById(R.id.LayoutRiwayat);
            layoutToko=view.findViewById(R.id.LayoutToko);

        }

        @Override
        public void OnClickDetail(Riwayat riwayat) {

        }

        @Override
        public void OnclickBtn(Riwayat riwayat) {

        }
    }


}
