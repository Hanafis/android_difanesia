package id.difanesia.www.UI.Activity.Payment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsuryo.androidcountdown.Counter;
import com.tsuryo.androidcountdown.TimeUnits;

import java.io.File;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import id.difanesia.www.Api.ApiConfig;
import id.difanesia.www.Api.ApiRequest;
import id.difanesia.www.Api.ApiResponse;
import id.difanesia.www.Helper.FormatCurrency;
import id.difanesia.www.MainActivity;
import id.difanesia.www.Model.Payment;
import id.difanesia.www.Model.PaymentInfromation;
import id.difanesia.www.R;
import id.difanesia.www.UI.Activity.Setting.ProfileActivity;
import id.difanesia.www.ViewModel.LoginViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPaymentActivity extends AppCompatActivity {
    LinearLayout bcaLayout,briLayout,bniLayout,mandiriLayout,bniLayoutManual,layoutBukti;
    TextView va,totalTagihan,tanggalTagihan,nomorText,banktext;
    ImageView bankImage;
    Counter waktuTagihan;
    String Tagihan;
    ImageButton btnBack,btnCopy,btnCopytagihan;
    PaymentInfromation paymentInfromation;
    Button btnpilih,btnUpload;

    ImageView buktiTransfer;
    Payment payment;
    private ClipboardManager myClipboard;
    private ClipData myClip;
    ApiRequest apiinterface;

    String Token;

    Uri imagepath;
    LoginViewModel loginViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_payment);
        apiinterface = ApiConfig.getClient(this).create(ApiRequest.class);
        btnpilih=findViewById(R.id.BtnPilih);
        bcaLayout=findViewById(R.id.BCALayout);
        briLayout=findViewById(R.id.BRILayout);
        bniLayout=findViewById(R.id.BNILayout);
        bniLayoutManual=findViewById(R.id.BNILayoutManual);
        mandiriLayout=findViewById(R.id.MandiriLayout);
        va=findViewById(R.id.VirtualAccount);
        tanggalTagihan=findViewById(R.id.TanggalPembayaran);
        waktuTagihan=findViewById(R.id.WaktuTagihan);
        btnBack=findViewById(R.id.BtnBack);
        btnCopy=findViewById(R.id.BtnCopyVa);
        btnCopytagihan=findViewById(R.id.BtnCopyTagihan);
        btnBack.setOnClickListener(view-> onBackPressed());
        totalTagihan=findViewById(R.id.Tagihan);
        bankImage=findViewById(R.id.GambarBank);
        nomorText=findViewById(R.id.TextNomor);
        banktext=findViewById(R.id.BankText);
        layoutBukti=findViewById(R.id.LayoutBuktiPembayaran);
        buktiTransfer=findViewById(R.id.GambarBukti);
        btnUpload=findViewById(R.id.BtnUpload);
        Token();
        btnUpload.setOnClickListener(view -> ImagePicker.Companion.with(DetailPaymentActivity.this)
                .crop()
                .compress(1024)
                .maxResultSize(1080,1080)
                .start());
        Bundle extras = getIntent().getExtras();
        FormatCurrency formatCurrency = new FormatCurrency();
        btnCopy.setOnClickListener(v -> {
            myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            String text;
            text = va.getText().toString();

            myClip = ClipData.newPlainText("text", text);
            myClipboard.setPrimaryClip(myClip);

            Toast.makeText(getApplicationContext(), "Virtual Account Berhasil Di Copy",Toast.LENGTH_SHORT).show();
        });
        btnCopytagihan.setOnClickListener(v -> {
            myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            String text;
            text = totalTagihan.getText().toString();

            myClip = ClipData.newPlainText("text", text);
            myClipboard.setPrimaryClip(myClip);

            Toast.makeText(getApplicationContext(), "Tagihan Berhasil Di Copy",Toast.LENGTH_SHORT).show();
        });
        if(extras == null) {
            paymentInfromation= null;
            Tagihan=null;
            payment=null;
        } else {
            Tagihan=extras.getString("TAGIHAN");
            paymentInfromation= (PaymentInfromation) extras.getSerializable("PaymentInformation");
            payment=(Payment) extras.getSerializable("DETAILPAYMENT");
        }
        if (payment!=null){
            Gson gson = new Gson();
            paymentInfromation = gson.fromJson(payment.getVaNumberList(),PaymentInfromation.class);
            Log.e("id_transaksi_wrap",paymentInfromation.getIdTransaksi()+"");
//            paymentInfromation.setValidity(payment.getValidity());

        }
        if (paymentInfromation!=null ){
            btnpilih.setVisibility(View.VISIBLE);
            btnpilih.setText("Kirim Bukti Bayar");
            btnpilih.setOnClickListener(view -> apiSendBukti());
            if (paymentInfromation.getManual()){
                banktext.setText("BNI Manual Check");
                nomorText.setText("Nomor Rekening BNI");
                bankImage.setImageResource(R.drawable.bni);
            }
            tanggalTagihan.setText(paymentInfromation.getValidity());
            Log.e("Tanggal",paymentInfromation.getValidity());
            va.setText(paymentInfromation.getVa());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date count = null;
            try {
                count = sdf.parse(paymentInfromation.getValidity());
            } catch (ParseException e) {
                e.printStackTrace();

            }
            if (count==null){
                Calendar c = Calendar.getInstance(TimeZone.getDefault());
                c.add( Calendar.DATE, 1 );
                try {
                    count=sdf.parse(sdf.format(c.getTime()));
                    Log.e("Tanggal",sdf.format(c.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            waktuTagihan.setDate(count); //countdown starts

            waktuTagihan.setIsShowingTextDesc(true);
            waktuTagihan.setMaxTimeUnit(TimeUnits.DAY);
            if (!Tagihan.isEmpty()) {
                totalTagihan.setText(formatCurrency.Rupiah(Double.parseDouble(Tagihan)));
            }
            switch (paymentInfromation.getBankId()){
                case "009":
                    if (paymentInfromation!=null){
                        if (paymentInfromation.getManual()){
                            bniLayoutManual.setVisibility(View.VISIBLE);
                        }else{
                            bniLayout.setVisibility(View.VISIBLE);
                        }
                    }else {
                        bniLayout.setVisibility(View.VISIBLE);
                    }
                    break;
                case "002":
                    briLayout.setVisibility(View.VISIBLE);
                    break;
                case "014":
                    bcaLayout.setVisibility(View.VISIBLE);
                    break;
                case "008":
                    mandiriLayout.setVisibility(View.VISIBLE);
                    break;
            }
        }

    }
    private void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();

                }
                else {
                    Toast.makeText(getApplicationContext(),"Gagal Terhubung Ke server",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private void apiSendBukti(){
        if (imagepath!=null) {
            if (getRealPathFromURI(imagepath) != null) {
                File file = new File(getRealPathFromURI(imagepath)); //Get File
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
                RequestBody id = RequestBody.create(MediaType.parse("text/plain"), paymentInfromation.getIdTransaksi().toString());
                Call<ApiResponse> call = apiinterface.sendBukti(Token,id,body);
                call.enqueue(new Callback<ApiResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                        if (response.body()!=null){
                            if (response.body().getSuccess()!=null){
                                Toast.makeText(DetailPaymentActivity.this, "Bukti Transfer Berhasil Dikirim", Toast.LENGTH_SHORT).show();
                                new Handler().postDelayed(() -> {
                                    startActivity(new Intent(DetailPaymentActivity.this, MainActivity.class));
                                    finish();
                                }, 5000);
                            }else {
                                Toast.makeText(DetailPaymentActivity.this, "Bukti Transfer Gagal Dikirim", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(DetailPaymentActivity.this, "Bukti Transfer Gagal Dikirim", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ApiResponse> call, Throwable t) {
                        Toast.makeText(DetailPaymentActivity.this, "Bukti Transfer Gagal Dikirim", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }else{
            Toast.makeText(this, "Silahkan Pilih Gambar Terlebih Dahulu", Toast.LENGTH_SHORT).show();
        }
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data.getData()!=null) {
            imagepath = data.getData();
            buktiTransfer.setVisibility(View.VISIBLE);
            buktiTransfer.setImageURI(imagepath);
        }

    }


}