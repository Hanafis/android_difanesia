package id.difanesia.www;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import com.etebarian.meowbottomnavigation.MeowBottomNavigation;


import id.difanesia.www.UI.Activity.Welcome.LoginActivity;
import id.difanesia.www.UI.Activity.Menu.NotifFragment;
import id.difanesia.www.UI.Activity.Menu.ChatFragment;
import id.difanesia.www.UI.Activity.Menu.DonateFragment;
import id.difanesia.www.UI.Activity.Menu.HomeFragment;
import id.difanesia.www.UI.Activity.Menu.SettingFragment;
import id.difanesia.www.ViewModel.LoginViewModel;



public class MainActivity extends AppCompatActivity {
    MeowBottomNavigation bottomNavigationView;
    LoginViewModel loginViewModel;
    String Token;
    Integer id_users,Level;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView= findViewById(R.id.navview);
//        bottomNavigationView.setOnNavigationItemSelectedListener(bottomNavMethod);

        bottomNavigationView.add(new MeowBottomNavigation.Model(1,R.drawable.ic_outline_home_24));
        bottomNavigationView.add(new MeowBottomNavigation.Model(2,R.drawable.ic_outline_chat_24));
        bottomNavigationView.add(new MeowBottomNavigation.Model(3,R.drawable.ic_donasi));
        bottomNavigationView.add(new MeowBottomNavigation.Model(4,R.drawable.ic_baseline_notifications_none_24));
        bottomNavigationView.add(new MeowBottomNavigation.Model(5,R.drawable.ic_outline_account_circle_24));
        bottomNavigationView.show(1,true);
        bottomNavigationView.setOnClickMenuListener(model -> {
            Fragment fragment=null;
            String TAG = null;
            switch (model.getId()){
                case 1:
                    TAG="FHOME";
                    fragment = new HomeFragment();
                    break;
                case 2:
                    TAG="FCHAT";
                    fragment = new ChatFragment();
                    break;
                case 4:
                    TAG="FCART";
                    fragment = new NotifFragment();
                    break;
                case 3:
                    TAG="FDONASI";
                    fragment = new DonateFragment();
                    break;
                case 5:
                    TAG="FSETTING";
                    fragment = new SettingFragment();
                    break;
            }
            assert fragment != null;
            getSupportFragmentManager().beginTransaction().replace(R.id.container_frangemnt,fragment,TAG).commit();
            return null;
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.container_frangemnt,new HomeFragment(),"FHOME").commit();
        Token();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Keluar Dari Aplikasi")
                .setMessage("Anda Yakin Inggin Keluar?")
                .setPositiveButton("Ya", (dialog, which) -> finish())
                .setNegativeButton("Tidak", null)
                .show();
    }
    private void Token(){
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getApiResponseLiveData().observe(this, login -> {
            if (login!=null){
                if (login.getToken()!=null){
                    Token="Bearer "+login.getToken();
                    id_users=login.getId_user();

                }
                else {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }

}