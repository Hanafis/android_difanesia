package id.difanesia.www.Api;




import android.content.Context;

import id.difanesia.www.R;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiConfig {
    private static Retrofit retrofit = null;
    public static Retrofit getClient(Context context) {
        //safe
        //Logging
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        //end loging
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(context.getResources().getString(R.string.path))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

}
