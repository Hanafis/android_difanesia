package id.difanesia.www.Api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.difanesia.www.Model.Alamat;
import id.difanesia.www.Model.Berita;
import id.difanesia.www.Model.DetailTransaksi;
import id.difanesia.www.Model.Donasi;
import id.difanesia.www.Model.ImageProduct;
import id.difanesia.www.Model.ListAlamat;
import id.difanesia.www.Model.Login;
import id.difanesia.www.Model.Notifikasi;
import id.difanesia.www.Model.Donatur;
import id.difanesia.www.Model.Payment;
import id.difanesia.www.Model.Produk;
import id.difanesia.www.Model.Provinsi;
import id.difanesia.www.Model.Rating;
import id.difanesia.www.Model.RatingBintang;
import id.difanesia.www.Model.Riwayat;
import id.difanesia.www.Model.Sliders;
import id.difanesia.www.Model.User;
import id.difanesia.www.Model.UserRating;
import id.difanesia.www.Model.Varian;
import id.difanesia.www.Model.Voucher;

public class ApiResponse {

    @SerializedName("email")
    @Expose
    private List<String> email = null;

    @SerializedName("success")
    @Expose
    private Boolean success=false;

    @SerializedName("message")
    @Expose
    private String message=null;

    @SerializedName("keranjang")
    @Expose
    private String keranjang=null;

    @SerializedName("Login")
    @Expose
    private Login login=null;

    @SerializedName("User")
    @Expose
    private User user=null;

    @SerializedName("Slider")
    @Expose
    private List<Sliders> slider = null;

    @SerializedName("Promo")
    @Expose
    private List<Sliders> promo = null;

    @SerializedName("Produk")
    @Expose
    private List<Produk> produk = null;

    @SerializedName("Provinsi")
    @Expose
    private List<Provinsi> provinsi = null;
    @SerializedName("Rating")
    @Expose
    private Rating rating;
    @SerializedName("UserRating")
    @Expose
    private List<UserRating> userRating = null;

    @SerializedName("ImageProduct")
    @Expose
    private List<ImageProduct> imageProduct = null;

    @SerializedName("JmlKeranjang")
    @Expose
    private Integer jmlKeranjang=null;

    @SerializedName("Varian")
    @Expose
    private List<Varian> varian = null;

    @SerializedName("RatingBintang")
    @Expose
    private List<RatingBintang> ratingBintang = null;

    @SerializedName("Alamat")
    @Expose
    private List<Alamat> alamat=null;

    @SerializedName("Donasi")
    @Expose
    private List<Donasi> donasi = null;

    @SerializedName("Berita")
    @Expose
    private List<Berita> berita = null;

    @SerializedName("Voucher")
    @Expose
    private List<Voucher> voucher = null;

    @SerializedName("Notifikasi")
    @Expose
    private List<Notifikasi> notifikasi = null;

    @SerializedName("Dibaca")
    @Expose
    private String dibaca=null;

    @SerializedName("Bayar")
    @Expose
    private Integer bayar=null;
    @SerializedName("Kemas")
    @Expose
    private Integer kemas=null;
    @SerializedName("Kirim")
    @Expose
    private Integer kirim=null;
    @SerializedName("Selesai")
    @Expose
    private Integer selesai=null;

    @SerializedName("ListAlamat")
    @Expose
    private List<ListAlamat> listAlamat = null;

    @SerializedName("Donatur")
    @Expose
    private List<Donatur> donatur = null;

    @SerializedName("Penerima")
    @Expose
    private List<Donatur> penerima = null;

    @SerializedName("LBayar")
    @Expose
    private List<Riwayat> lbayar = null;
    @SerializedName("LKemas")
    @Expose
    private List<Riwayat> lkemas = null;
    @SerializedName("LKirim")
    @Expose
    private List<Riwayat> lkirim = null;
    @SerializedName("LSelesai")
    @Expose
    private List<Riwayat> lselesai = null;
    @SerializedName("LBatal")
    @Expose
    private List<Riwayat> lbatal = null;
    @SerializedName("LPengembalian")
    @Expose
    private List<Riwayat> lpengembalian = null;

    @SerializedName("B1")
    @Expose
    private List<UserRating> b1 = null;

    @SerializedName("B2")
    @Expose
    private List<UserRating> b2 = null;

    @SerializedName("B3")
    @Expose
    private List<UserRating> b3 = null;

    @SerializedName("B4")
    @Expose
    private List<UserRating> b4 = null;

    @SerializedName("B5")
    @Expose
    private List<UserRating> b5 = null;

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("detail_transaksi")
    @Expose
    private DetailTransaksi detailTransaksi;

    @SerializedName("payment")
    @Expose
    private Payment payment;

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailTransaksi getDetailTransaksi() {
        return detailTransaksi;
    }

    public void setDetailTransaksi(DetailTransaksi detailTransaksi) {
        this.detailTransaksi = detailTransaksi;
    }


    public List<UserRating> getB1() {
        return b1;
    }

    public void setB1(List<UserRating> b1) {
        this.b1 = b1;
    }

    public List<UserRating> getB2() {
        return b2;
    }

    public void setB2(List<UserRating> b2) {
        this.b2 = b2;
    }

    public List<UserRating> getB3() {
        return b3;
    }

    public void setB3(List<UserRating> b3) {
        this.b3 = b3;
    }

    public List<UserRating> getB4() {
        return b4;
    }

    public void setB4(List<UserRating> b4) {
        this.b4 = b4;
    }

    public List<UserRating> getB5() {
        return b5;
    }

    public void setB5(List<UserRating> b5) {
        this.b5 = b5;
    }

    public List<Riwayat> getLBayar() {
        return lbayar;
    }

    public void setLBayar(List<Riwayat> lbayar) {
        this.lbayar = lbayar;
    }

    public List<Riwayat> getLKemas() {
        return lkemas;
    }

    public void setLKemas(List<Riwayat> lkemas) {
        this.lkemas = lkemas;
    }

    public List<Riwayat> getLKirim() {
        return lkirim;
    }

    public void setLKirim(List<Riwayat> lkirim) {
        this.lkirim = lkirim;
    }

    public List<Riwayat> getLSelesai() {
        return lselesai;
    }

    public void setLSelesai(List<Riwayat> lselesai) {
        this.lselesai = lselesai;
    }

    public void setLBatal(List<Riwayat> lbatal) {
        this.lbatal = lbatal;
    }

    public List<Riwayat> getLBatal() {
        return lbatal;
    }

    public void setLPengembalian(List<Riwayat> lpengembalian) {
        this.lpengembalian = lpengembalian;
    }

    public List<Riwayat> getLPengembalian() {
        return lpengembalian;
    }


    public List<Donatur> getDonatur() {
        return donatur;
    }

    public void setDonatur(List<Donatur> donatur) {
        this.donatur = donatur;
    }

    public List<Donatur> getPenerima() {
        return penerima;
    }

    public void setPenerima(List<Donatur> penerima) {
        this.penerima = penerima;
    }


    public List<ListAlamat> getListAlamat() {
        return listAlamat;
    }

    public void setListAlamat(List<ListAlamat> listAlamat) {
        this.listAlamat = listAlamat;
    }

    public Integer getBayar() {
        return bayar;
    }

    public void setBayar(Integer bayar) {
        this.bayar = bayar;
    }

    public Integer getKemas() {
        return kemas;
    }

    public void setKemas(Integer kemas) {
        this.kemas = kemas;
    }

    public Integer getKirim() {
        return kirim;
    }

    public void setKirim(Integer kirim) {
        this.kirim = kirim;
    }

    public Integer getSelesai() {
        return selesai;
    }

    public void setSelesai(Integer selesai) {
        this.selesai = selesai;
    }

    public String getDibaca() {
        return dibaca;
    }

    public void setDibaca(String dibaca) {
        this.dibaca = dibaca;
    }

    public List<Notifikasi> getNotifikasi() {
        return notifikasi;
    }

    public void setNotifikasi(List<Notifikasi> notifikasi) {
        this.notifikasi = notifikasi;
    }

    public List<Voucher> getVoucher() {
        return voucher;
    }

    public void setVoucher(List<Voucher> voucher) {
        this.voucher = voucher;
    }

    public List<Berita> getBerita() {
        return berita;
    }

    public void setBerita(List<Berita> berita) {
        this.berita = berita;
    }

    public List<Donasi> getDonasi() {
        return donasi;
    }

    public void setDonasi(List<Donasi> donasi) {
        this.donasi = donasi;
    }

    public List<Alamat> getAlamat() {
        return alamat;
    }

    public void setAlamat(List<Alamat> alamat) {
        this.alamat = alamat;
    }

    public List<RatingBintang> getRatingBintang() {
        return ratingBintang;
    }

    public void setRatingBintang(List<RatingBintang> ratingBintang) {
        this.ratingBintang = ratingBintang;
    }

    public List<Varian> getVarian() {
        return varian;
    }

    public void setVarian(List<Varian> varian) {
        this.varian = varian;
    }

    public Integer getJmlKeranjang() {
        return jmlKeranjang;
    }

    public void setJmlKeranjang(Integer jmlKeranjang) {
        this.jmlKeranjang = jmlKeranjang;
    }

    public List<ImageProduct> getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(List<ImageProduct> imageProduct) {
        this.imageProduct = imageProduct;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public List<UserRating> getUserRating() {
        return userRating;
    }

    public void setUserRating(List<UserRating> userRating) {
        this.userRating = userRating;
    }

    public List<Provinsi> getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(List<Provinsi> provinsi) {
        this.provinsi = provinsi;
    }

    public List<Produk> getProduk() {
        return produk;
    }

    public void setProduk(List<Produk> produk) {
        this.produk = produk;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() { return message; }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKeranjang() { return keranjang; }

    public void setKeranjang(String keranjang) {
        this.keranjang = keranjang;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<Sliders> getSlider() { return slider; }

    public void setSlider(List<Sliders> slider) { this.slider = slider; }

    public List<Sliders> getPromo() { return promo; }

    public void setPromo(List<Sliders> promo) { this.promo = promo; }





}
