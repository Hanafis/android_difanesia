package id.difanesia.www.Api;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiRequest {




    @GET("api/product/penjualan")
    Call<ApiResponse> ProdukPopuler(
            @Header("Authorization") String token
    );
    @GET("api/product/toprating")
    Call<ApiResponse> ProdukRating(
            @Header("Authorization") String token
    );
    @GET("api/home/donasi")
    Call<ApiResponse> DonasiHome(
            @Header("Authorization") String token
    );
    @FormUrlEncoded
    @POST("api/product/top")
    Call<ApiResponse> Top(@Header("Authorization") String token,
                            @Field("start") Integer Start,
                            @Field("limit") Integer limit
    );
    @FormUrlEncoded
    @POST("api/home")
    Call<ApiResponse> Home(
            @Header("Authorization") String token,
            @Field("start") Integer Start,
            @Field("limit") Integer limit,
            @Field("id_user") Integer id_user
    );
    @FormUrlEncoded
    @POST("api/product/search")
    Call<ApiResponse> Search(
            @Header("Authorization") String token,
            @Field("keyword") String Keyword,
            @Field("start") Integer Start,
            @Field("limit") Integer limit,
            @Field("kota[]") List<String> kota,
            @Field("urutan") String urutkan,
            @Field("min") String min,
            @Field("max") String max
    );
    @FormUrlEncoded
    @POST("api/product/varian")
    Call<ApiResponse> Varian(
            @Header("Authorization") String token,
            @Field("id_product") Integer id_product

    );
    @FormUrlEncoded
    @POST("api/keranjang/tambah")
    Call<ApiResponse> CartAdd(
            @Header("Authorization") String token,
            @Field("id_user") Integer id_user,
            @Field("id_product") Integer id_product,
            @Field("jml") Integer jml

    );
    @FormUrlEncoded
    @POST("api/keranjang/hapus")
    Call<ApiResponse> CartDelete(
            @Header("Authorization") String token,
            @Field("id_keranjang") Integer id_keranjang

    );
    @FormUrlEncoded
    @POST("api/keranjang/kurang")
    Call<ApiResponse> CartMinus(
            @Header("Authorization") String token,
            @Field("id_keranjang") Integer id_keranjang

    );
    @FormUrlEncoded
    @POST("api/keranjang/list")
    Call<ApiResponse> CartList(
            @Header("Authorization") String token,
            @Field("id_user") Integer id_user

    );

    @FormUrlEncoded
    @POST("api/product/rating/detail")
    Call<ApiResponse> RatingData(
            @Header("Authorization") String token,
            @Field("id_product") Integer id_product,
            @Field("bintang") Integer bintang,
            @Field("start") Integer Start,
            @Field("limit") Integer Limit

    );
    @FormUrlEncoded
    @POST("api/product/rating")
    Call<ApiResponse> RatingData(
            @Header("Authorization") String token,
            @Field("id_product") Integer id_product

    );
    @FormUrlEncoded
    @POST("api/user/alamat/utama")
    Call<ApiResponse> ALamatUtama(
            @Header("Authorization") String token,
            @Field("id_user") Integer id_user

    );
    @FormUrlEncoded
    @POST("api/user/alamat/list")
    Call<ApiResponse> ALamatList(
            @Header("Authorization") String token,
            @Field("id_user") Integer id_user

    );
    @FormUrlEncoded
    @POST("api/user/alamat/tambah")
    Call<ApiResponse> ALamats(
            @Header("Authorization") String token,
            @Field("id_user") Integer id_user,
            @Field("nama_lengkap") String nama_lengkap,
            @Field("no_tlpn") String no_tlpn,
            @Field("provinsi") String provinsi,
            @Field("kabupaten") String kabupaten,
            @Field("kecamatan") String kecamatan,
            @Field("kelurahan") String kelurahan,
            @Field("kodepos") String kodepos,
            @Field("jalan") String jalan,
            @Field("lainya") String lainya,
            @Field("gedung") String gedung,
            @Field("utama") String utama


    );
    @FormUrlEncoded
    @POST("api/user/alamat/ubah")
    Call<ApiResponse> ALamats(
            @Header("Authorization") String token,
            @Field("id_alamat") Integer id_alamat,
            @Field("id_user") Integer id_user,
            @Field("nama_lengkap") String nama_lengkap,
            @Field("no_tlpn") String no_tlpn,
            @Field("id_provinsi") Integer id_provinsi,
            @Field("id_kabupaten") Integer id_kabupaten,
            @Field("id_kecamatan") Integer id_kecamatan,
            @Field("id_kelurahan") Integer id_kelurahan,
            @Field("provinsi") String provinsi,
            @Field("kabupaten") String kabupaten,
            @Field("kecamatan") String kecamatan,
            @Field("kelurahan") String kelurahan,
            @Field("kodepos") String kodepos,
            @Field("jalan") String jalan,
            @Field("lainya") String lainya,
            @Field("gedung") String gedung,
            @Field("utama") String utama


    );
    @FormUrlEncoded
    @POST("api/user/alamat/hapus")
    Call<ApiResponse> AlamatHapus(
            @Header("Authorization") String token,
            @Field("id_user") Integer id_user,
            @Field("id_alamat") Integer id_alamat,
            @Field("utama") String utama


    );
    @FormUrlEncoded
    @POST("api/wish/list")
    Call<ApiResponse> WishList(
            @Header("Authorization") String token,
            @Field("id_user") Integer id_user,
            @Field("keyword") String Keyword,
            @Field("start") Integer Start,
            @Field("limit") Integer limit

    );
    @FormUrlEncoded
    @POST("api/wish/tambah")
    Call<ApiResponse> WishAdd(
            @Header("Authorization") String token,
            @Field("id_user") Integer id_user,
            @Field("id_product") Integer id_product

    );
    @FormUrlEncoded
    @POST("api/wish/hapus")
    Call<ApiResponse> WishDelete(
            @Header("Authorization") String token,
            @Field("id_wish") Integer id_wish

    );

    @FormUrlEncoded
    @POST("api/password/update")
    Call<ApiResponse> UpdatePassword(@Header("Authorization") String token,
                                     @Field("id_user") Integer id_users,
                                     @Field("old_password") String old_password,
                                     @Field("newpassword") String new_password
    );
    @FormUrlEncoded
    @POST("api/product/detail")
    Call<ApiResponse> DetailProduct(@Header("Authorization") String token,
                                     @Field("id_product") Integer id_product,
                                    @Field("id_user") Integer id_user
    );

    @FormUrlEncoded
    @POST("api/reset")
    Call<ApiResponse> reset(@Field("email") String email
    );

    @FormUrlEncoded
    @POST("api/kode")
    Call<ApiResponse> kode(@Field("email") String email,
                           @Field("kode") Integer kode
    );

    @FormUrlEncoded
    @POST("api/password")
    Call<ApiResponse> resetPassword(@Field("email") String email,
                                    @Field("kode") Integer kode,
                                    @Field("password") String password
    );


    @FormUrlEncoded
    @POST("api/login")
    Call<ApiResponse> login(@Field("email") String email,
                            @Field("password") String password);
    @FormUrlEncoded
    @POST("api/register")
    Call<ApiResponse> register(@Field("name") String nama,
                               @Field("no_tlpn") String telepon,
                               @Field("email") String email,
                               @Field("password") String password,
                               @Field("c_password") String c_password);

    @FormUrlEncoded
    @POST("api/user")
    Call<ApiResponse> userdetail(@Header("Authorization") String token,
                                 @Field("id_user") Integer id_user);
    @FormUrlEncoded
    @POST("api/kupon/list")
    Call<ApiResponse> VoucherList(@Header("Authorization") String token,
                                 @Field("id_user") Integer id_user);
    @FormUrlEncoded
    @POST("api/kupon/search")
    Call<ApiResponse> VoucherSearch(@Header("Authorization") String token,
                                    @Field("id_user") Integer id_user,
                                    @Field("kode") String kode);

    @FormUrlEncoded
    @POST("api/kupon/pilih")
    Call<ApiResponse> VoucherPilih(@Header("Authorization") String token,
                                    @Field("id_user") Integer id_user,
                                    @Field("id_kupon") Integer id_voucher);
    @FormUrlEncoded
    @POST("api/notifikasi/list")
    Call<ApiResponse> Notifikasi(@Header("Authorization") String token,
                                   @Field("id_user") Integer id_user,
                                   @Field("jenis") String jenis);
    @FormUrlEncoded
    @POST("api/notifikasi/dibaca")
    Call<ApiResponse> NotifikasiDibaca(@Header("Authorization") String token,
                                       @Field("id_user") Integer id_user,
                                       @Field("id_notifikasi") Integer id_notifikasi
                                       );
    @FormUrlEncoded
    @POST("api/notifikasi/dibaca/semua")
    Call<ApiResponse> NotifikasiSemuaDibaca(@Header("Authorization") String token,
                                            @Field("id_user") Integer id_user,
                                            @Field("jenis") String jenis);
    @FormUrlEncoded
    @POST("api/riwayat/notifikasi")
    Call<ApiResponse> RiwayatJumlah(@Header("Authorization") String token,
                                            @Field("id_user") Integer id_user);
    @FormUrlEncoded
    @POST("api/riwayat/list")
    Call<ApiResponse> RiwayatList(@Header("Authorization") String token,
                                    @Field("id_user") Integer id_user);

    @FormUrlEncoded
    @POST("api/riwayat/detail")
    Call<ApiResponse> RiwayatDetail(@Header("Authorization") String token,
                                    @Field("id_transaksi") Integer id_transaksi);

    @FormUrlEncoded
    @POST("api/alamat")
    Call<ApiResponse> Alamats(@Header("Authorization") String token,
                                    @Field("id_user") Integer id_user);
    @FormUrlEncoded
    @POST("api/alamat")
    Call<ApiResponse> Kabupaten(@Header("Authorization") String token,
                              @Field("id_user") Integer id_user,
                              @Field("id_provinsi") Integer id_provinsi
                              );
    @FormUrlEncoded
    @POST("api/alamat")
    Call<ApiResponse> Kecamatan(@Header("Authorization") String token,
                                @Field("id_user") Integer id_user,
                                @Field("id_kabupaten") Integer id_kabupaten
    );
    @FormUrlEncoded
    @POST("api/alamat")
    Call<ApiResponse> Kalurahan(@Header("Authorization") String token,
                                @Field("id_user") Integer id_user,
                                @Field("id_kecamatan") Integer id_kecamatan
    );
    @FormUrlEncoded
    @POST("api/alamat")
    Call<ApiResponse> KodePos(@Header("Authorization") String token,
                                @Field("id_user") Integer id_user,
                                @Field("id_kelurahan") Integer id_kelurahan
    );
    @Multipart
    @POST("api/user/update/{id_user}")
    Call<ApiResponse> UpdateUser(@Header("Authorization") String token,
                               @Path("id_user") Integer id_user,
                               @Part("name") RequestBody nama,
                               @Part("jenis_kelamin") RequestBody jenis,
                               @Part("tanggal_lahir") RequestBody tanggal,
                               @Part("no_tlpn") RequestBody noTlpn,
                               @Part MultipartBody.Part image);
    @Multipart
    @POST("api/user/update/{id_user}")
    Call<ApiResponse> UpdateUser(@Header("Authorization") String token,
                               @Path("id_user") Integer id_user,
                               @Part("name") RequestBody nama,
                               @Part("jenis_kelamin") RequestBody jenis,
                               @Part("tanggal_lahir") RequestBody tanggal,
                               @Part("no_tlpn") RequestBody noTlpn);

    @FormUrlEncoded
    @POST("api/donasi/list")
    Call<ApiResponse> DonasiList(
            @Header("Authorization") String token,
            @Field("start") Integer Start,
            @Field("limit") Integer limit
    );
    @FormUrlEncoded
    @POST("api/berita/list")
    Call<ApiResponse> BeritaList(
            @Header("Authorization") String token,
            @Field("start") Integer Start,
            @Field("limit") Integer limit
    );
    @FormUrlEncoded
    @POST("api/donasi/penerima")
    Call<ApiResponse> PenerimaDonasi(
            @Header("Authorization") String token,
            @Field("id_donasi") Integer id_donasi,
            @Field("start") Integer Start,
            @Field("limit") Integer limit
    );
    @FormUrlEncoded
    @POST("api/donasi/donatur")
    Call<ApiResponse> DonaturDonasi(
            @Header("Authorization") String token,
            @Field("id_donasi") Integer id_donasi,
            @Field("start") Integer Start,
            @Field("limit") Integer limit
    );
    @POST("api/checkout")
    Call<ApiResponse> Checkout(
            @Header("Authorization") String token,
            @Body RequestBody params
            );
    @FormUrlEncoded
    @POST("api/riwayat/diterima")
    Call<ApiResponse> PesananDiterima(
            @Header("Authorization") String token,
            @Field("id_transaksi") Integer id_donasi
    );
    @Multipart
    @POST("/api/add-rating")
    Call<ApiResponse> TambahRating(@Header("Authorization") String token,
                                 @Part("id_transaksi") RequestBody id_transaksi,
                                 @Part("id_transaksi_list") RequestBody id_transaksi_list,
                                 @Part("id_product") RequestBody id_product,
                                   @Part("rating") RequestBody rating,
                                   @Part("komentar") RequestBody komentar,
                                   @Part MultipartBody.Part image);

    @Multipart
    @POST("/api/transactions/kirim-bukti")
    Call<ApiResponse> sendBukti(
            @Header("Authorization") String token,
            @Part("id_transaksi_wrap") RequestBody jenis,
            @Part MultipartBody.Part image);

}
